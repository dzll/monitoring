<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaiDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_dosen', function (Blueprint $table) {
            $table->id();
            $table->integer('id_penguji')->unsigned();
            $table->foreign('id_penguji')->references('id')->on('penguji');
            $table->integer('id_item_nilai')->unsigned();
            $table->foreign('id_item_nilai')->references('id')->on('item_nilai');
            $table->float('nilai');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_dosen');
    }
}
