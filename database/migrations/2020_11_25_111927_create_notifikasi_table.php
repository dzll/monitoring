<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotifikasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifikasi', function (Blueprint $table) {
            $table->id();
            $table->integer('id_dosen')->unsigned();
            $table->foreign('id_dosen')->references('id')->on('dosen');
            $table->integer('id_pengajuan')->unsigned();
            $table->foreign('id_pengajuan')->references('id')->on('pengajuan');
            $table->integer('status');
            $table->integer('judul_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifikasi');
    }
}
