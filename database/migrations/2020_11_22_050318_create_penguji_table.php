<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengujiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penguji', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal_revisi')->nullable();
            $table->date('deadline_revisi')->nullable();
            $table->integer('id_pendaftaran')->unsigned();
            $table->foreign('id_pendaftaran')->references('id')->on('pendaftaran_proposal');
            $table->integer('id_dosen')->unsigned();
            $table->foreign('id_dosen')->references('id')->on('dosen');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penguji');
    }
}
