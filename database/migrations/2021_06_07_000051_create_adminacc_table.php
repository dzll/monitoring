<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminaccTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adminacc', function (Blueprint $table) {
            $table->id();
            $table->integer('id_pengajuan')->unsigned();
            $table->foreign('id_pengajuan')->references('id')->on('pengajuan');
            $table->integer('id_admin')->unsigned();
            $table->foreign('id_admin')->references('id')->on('admin');
            $table->integer('status');
            $table->integer('keterangan');
            $table->integer('aksi_dosen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adminacc');
    }
}
