<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendaftaranProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftaran_proposal', function (Blueprint $table) {
            $table->id();
            $table->string('file_dhs');
            $table->string('file_laporan');
            $table->string('file_krs');
            $table->string('file_bukti_pembayaran');
            $table->integer('id_pengajuan')->unsigned();
            $table->foreign('id_pengajuan')->references('id')->on('pengajuan');
            $table->date('tanggal');
            $table->string('waktu');
            $table->string('tempat');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftaran_proposal');
    }
}
