<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Admin::create([
            'username' => 'admin1',
            'password' => Hash::make('admin'),
            'nama' => 'Nama Admin 1',
            'email' => 'emailadmin1@gmail.com',
            'role' => 1,
        ]);

        Admin::create([
            'username' => 'admin2',
            'password' => Hash::make('admin'),
            'nama' => 'Nama Admin 2',
            'email' => 'emailadmin2@gmail.com',
            'role' => 1,
        ]);
    }
}
