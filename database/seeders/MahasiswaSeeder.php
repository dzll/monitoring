<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Mahasiswa;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Mahasiswa::create([
            'username' => 'mhs',
            'password' => Hash::make('mhs'),
            'npm' => '06.2017.1.00999',
            'nama' => 'Nama Mahasiswa 1',
            'semester' => 7,
            'email' => 'emailmhs@gmail.com',
            'role' => 3,
        ]);

        Mahasiswa::create([
            'username' => 'mhs2',
            'password' => Hash::make('mhs'),
            'npm' => '06.2017.1.00739',
            'nama' => 'Nama Mahasiswa 2',
            'semester' => 7,
            'email' => 'emailmhs2@gmail.com',
            'role' => 3,
        ]);

        Mahasiswa::create([
            'username' => 'mhs3',
            'password' => Hash::make('mhs'),
            'npm' => '06.2017.1.00711',
            'nama' => 'Nama Mahasiswa 3',
            'semester' => 7,
            'email' => 'emailmhs3@gmail.com',
            'role' => 3,
        ]);
    }
}
