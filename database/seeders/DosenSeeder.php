<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Dosen;

class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Dosen::create([
            'username'=>'dosen1',
            'password'=> Hash::make('dosen'),
            'nama'=>'Dosen 1 S.Kom M.Kom',
            'spesialis' => 'AI',
            'email'=> 'emaildosen1@gmail.com',
            'role'=> 2,
        ]);

        Dosen::create([
            'username'=>'dosen2',
            'password'=> Hash::make('dosen'),
            'nama'=>'Dosen 2 S.Kom M.Kom',
            'spesialis' => 'RPL',
            'email'=> 'emaildosen2@gmail.com',
            'role'=> 2,
        ]);

        Dosen::create([
            'username'=>'dosen3',
            'password'=> Hash::make('dosen'),
            'nama'=>'Dosen 3 S.Kom M.Kom',
            'spesialis' => 'Jaringan',
            'email'=> 'emaildosen3@gmail.com',
            'role'=> 2,
        ]);

        Dosen::create([
            'username'=>'dosen4',
            'password'=> Hash::make('dosen'),
            'nama'=>'Dosen 4 S.Kom M.Kom',
            'spesialis' => 'Jaringan',
            'email'=> 'emaildosen4@gmail.com',
            'role'=> 2,
        ]);

        Dosen::create([
            'username'=>'dosen5',
            'password'=> Hash::make('dosen'),
            'nama'=>'Dosen 5 S.Kom M.Kom',
            'spesialis' => 'AI',
            'email'=> 'emaildosen5@gmail.com',
            'role'=> 2,
        ]);
    }
}
