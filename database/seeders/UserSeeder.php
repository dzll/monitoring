<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'username'=>'admin',
            'password'=> Hash::make('admin'),
            'id_role'=> 1,
        ]);

        User::create([
            'username'=>'dosen1',
            'password'=> Hash::make('dosen'),
            'id_role'=> 2,
        ]);

        User::create([
            'username'=>'dosen2',
            'password'=> Hash::make('dosen'),
            'id_role'=> 2,
        ]);

        User::create([
            'username'=>'dosen3',
            'password'=> Hash::make('dosen'),
            'id_role'=> 2,
        ]);

        User::create([
            'username'=>'mhs',
            'password'=> Hash::make('mhs'),
            'id_role'=> 3,
        ]);
    }
}
