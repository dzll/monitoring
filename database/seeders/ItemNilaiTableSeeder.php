<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ItemNilai;

class ItemNilaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ItemNilai::create([
            'nama'=>'Perumusan Masalah Penelitian Latar Belakang Penelitian Kebaruan Penelitian',
            'persentase'=>'30',
            'deskripsi'=>'-',
        ]);

        ItemNilai::create([
            'nama'=>'Relevansi, Kebaruan Pustaka, dan Penyusunan Pustaka',
            'persentase'=>'10',
            'deskripsi'=>'-',
        ]);

        ItemNilai::create([
            'nama'=>'Perancangan Sistem dan Metode Yang Digunakan',
            'persentase'=>'30',
            'deskripsi'=>'-',
        ]);

        ItemNilai::create([
            'nama'=>'Penyampaian Materi dan Tanya Jawab',
            'persentase'=>'30',
            'deskripsi'=>'-',
        ]);
    }
}
