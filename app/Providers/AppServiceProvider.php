<?php

namespace App\Providers;

use App\Models\Adminacc;
use App\Models\Notifikasi;
use App\Models\PendaftaranProposal;
use App\Models\Pengajuan;
use App\Models\Penguji;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        // Schema::defaultStringLength(191);
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');

        view()->composer(
            ['components.header'],
            function ($view) {
                if (Auth::user()->role === 2) {
                    $dosen = Auth::user()->id;
                    $pengajuan = Notifikasi::where('id_dosen', $dosen)->where('status', 0)->orderBy('created_at', 'DESC')->get();
                    $ids = Penguji::where('id_dosen', $dosen)->pluck('id_pendaftaran')->toArray();
                    // dd($ids);
                    if ($ids == false) {
                        // $view->with('pengajuan', $pengajuan);
                        $jadwal = [];
                        $view->with('pengajuan', $pengajuan)->with('jadwal', $jadwal);
                    } else {
                        // $jadwal = PendaftaranProposal::whereIn('id', $ids)->where('status', 1)->orderBy('created_at', 'DESC')->get();
                        //jadwal diganti email
                        $jadwal = [];
                        $view->with('pengajuan', $pengajuan)->with('jadwal', $jadwal);
                    }
                } elseif (Auth::user()->role === 3) {
                    // $mahasiswaNotif = Pengajuan::where('id', Auth::user()->id)->where('status', '!=', 0)->get();
                    $mahasiswaNotif = [];
                    $view->with('notifmhs', $mahasiswaNotif);
                } elseif (Auth::user()->role === 1) {

                    $adminNotif = Adminacc::where('id_admin', Auth::user()->id)->where('status', 0)->get();
                    $view->with('adminnotif', $adminNotif);
                }
            }
        );
    }
}
