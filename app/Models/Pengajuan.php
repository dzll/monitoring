<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Notifikasi;
use App\Models\Mahasiswa;
use App\Models\PendaftaranProposal;
use App\Models\Adminacc;

class Pengajuan extends Model
{
    use HasFactory;

    protected $table = 'pengajuan';

    protected $fillable = [
        'judul_proposal', 'status', 'id_mahasiswa',
    ];

    public function notifikasi()
    {
        return $this->hasMany(Notifikasi::class, 'id_pengajuan');
    }

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'id_mahasiswa');
    }

    public function pendaftaran()
    {
        return $this->hasMany(PendaftaranProposal::class);
    }

    public function adminacc()
    {
        return $this->hasMany(Adminacc::class);
    }
}
