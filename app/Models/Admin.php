<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Adminacc;

class Admin extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'admin';

    protected $guard = 'admin';

    protected $fillable = [
        'nama', 'email', 'role', 'username', 'password',
    ];

    protected $hidden = ['password'];

    public function adminacc()
    {
        return $this->hasMany(Adminacc::class);
    }
}
