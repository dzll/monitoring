<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ItemNilai;
use App\Models\Penguji;

class NilaiDosen extends Model
{
    use HasFactory;

    protected $table = 'nilai_dosen';

    protected $fillable = [
        'nilai', 'id_penguji', 'id_item_nilai'
    ];

    public function itemNilai()
    {
        return $this->belongsTo(ItemNilai::class, 'id_item_nilai');
    }

    public function penguji()
    {
        return $this->belongsTo(Penguji::class, 'id_penguji');
    }

}
