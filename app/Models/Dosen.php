<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Notifikasi;

class Dosen extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'dosen';

    protected $guard = 'dosen';

    protected $fillable = [
        'nama', 'spesialis', 'email', 'role', 'username', 'password',
    ];

    protected $hidden = ['password'];

    public function notifikasi()
    {
        return $this->hasMany(Notifikasi::class);
    }

    public function penguji()
    {
        return $this->hasMany(Penguji::class);
    }
}
