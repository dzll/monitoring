<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pengajuan;
use App\Models\Penguji;

class PendaftaranProposal extends Model
{
    use HasFactory;

    protected $table = 'pendaftaran_proposal';

    protected $fillable = [
        'file_dhs', 'file_laporan', 'file_krs', 'file_bukti_pembayaran', 'id_pengajuan', 'tanggal', 'waktu', 'tempat', 'status' //, 'tittle', 'tanggal_mulai', 'tanggal_akhir'
    ];

    public function pengajuan()
    {
        return $this->belongsTo(Pengajuan::class, 'id_pengajuan');
    }

    public function penguji()
    {
        return $this->hasMany(Penguji::class);
    }
}
