<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\NilaiDosen;

class ItemNilai extends Model
{
    use HasFactory;

    protected $table = 'item_nilai';

    protected $fillable = [
        'nama', 'persentase', 'deskripsi'
    ];

    public function nilaiDosen()
    {
        return $this->hasMany(NilaiDosen::class);
    }
}
