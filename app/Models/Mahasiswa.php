<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Pengajuan;

class Mahasiswa extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'mahasiswa';

    protected $guard = 'mahasiswa';

    protected $fillable = [
        'nama', 'npm', 'semester', 'email', 'role', 'username', 'password',
    ];

    protected $hidden = ['password'];

    public function pengajuan()
    {
        return $this->hasMany(Pengajuan::class);
    }
}
