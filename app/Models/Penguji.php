<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PendaftaranProposal;
use App\Models\Dosen;
use App\Models\NilaiDosen;

class Penguji extends Model
{
    use HasFactory;

    protected $table = 'penguji';

    protected $fillable = [
        'tanggal_revisi', 'deadline_revisi', 'id_pendaftaran', 'id_dosen', 'status'
    ];

    public function pendaftaran()
    {
        return $this->belongsTo(PendaftaranProposal::class, 'id_pendaftaran');
    }

    public function dosen()
    {
        return $this->belongsTo(Dosen::class, 'id_dosen');
    }

    public function nilaiDosen()
    {
        return $this->hasMany(NilaiDosen::class);
    }
}
