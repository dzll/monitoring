<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pengajuan;
use App\Models\Admin;

class Adminacc extends Model
{
    use HasFactory;

    protected $table = 'adminacc';

    protected $fillable = [
        'id_pengajuan', 'id_admin', 'status', 'keterangan', 'aksi_dosen',
    ];

    public function pengajuan()
    {
        return $this->hasMany(Pengajuan::class, 'id_pengajuan');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'id_admin');
    }
}
