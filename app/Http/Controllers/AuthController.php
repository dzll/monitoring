<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
// use App\Models\Admin;

class AuthController extends Controller
{
    //
    use AuthenticatesUsers;

    protected $maxAttempts = 3;
    protected $decayMinutes = 2;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        if (auth()->guard('admin')->attempt($request->only('username', 'password'))) {
            return redirect('/user/admin');
        } else if (auth()->guard('dosen')->attempt($request->only('username', 'password'))) {
            return redirect('/user/dosen');
        } else if (auth()->guard('mahasiswa')->attempt($request->only('username', 'password'))) {
            return redirect('/user/mahasiswa');
        }

        return redirect()->back()->with('username', $request->username)->with('message', 'Username atau Password salah !!');
    }

    public function logout()
    {

        if (auth()->guard('admin')) {
            auth()->guard('admin')->logout();
        } else if (auth()->guard('dosen')) {
            auth()->guard('dosen')->logout();
        } else if (auth()->guard('mahasiswa')) {
            auth()->guard('mahasiswa')->logout();
        }
        session()->flush();

        return redirect()->route('login');
    }
}
