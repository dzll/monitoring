<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\PendaftaranProposal;
use App\Models\Pengajuan;

class PendaftaranProposalController extends Controller
{
    //

    public function view()
    {
        $pengajuan = Pengajuan::where('id_mahasiswa', Auth::user()->id)->where('status', 1)->first();
        $cek_status = Pengajuan::where('id_mahasiswa', Auth::user()->id)->where('status', 1)->first();
        return view('mahasiswa.pendaftaran')->with('pengajuan', $pengajuan)->with('cek_status', $cek_status);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'file_dhs' => 'required|file|mimes:doc,pdf,docx',
            'file_laporan' => 'required|file|mimes:doc,pdf,docx',
            'file_krs' => 'required|file|mimes:doc,pdf,docx',
            'file_bukti_pembayaran' => 'required|file|mimes:jpeg,jpg,png,pdf',
        ]);

        $file_dhs = $request->file('file_dhs');
        $file_laporan = $request->file('file_laporan');
        $file_krs = $request->file('file_krs');
        $file_bukti_pembayaran = $request->file('file_bukti_pembayaran');

        // $nama_file_dhs = time() . "_dhs_" . $file_dhs->getClientOriginalName();
        $nama_file_dhs = time() . "_dhs_" . Auth::user()->npm .".". $file_dhs->extension();
        $nama_file_laporan = time() . "_laporan_" . Auth::user()->npm .".". $file_laporan->extension();
        $nama_file_krs = time() . "_krs_" . Auth::user()->npm .".". $file_krs->extension();
        $nama_file_bukti_pembayaran = time() . "_bukti_pembayaran_" . Auth::user()->npm .".". $file_bukti_pembayaran->extension();

        $tujuan_upload = 'data_file';
        $file_dhs->move($tujuan_upload, $nama_file_dhs);
        $file_laporan->move($tujuan_upload, $nama_file_laporan);
        $file_krs->move($tujuan_upload, $nama_file_krs);
        $file_bukti_pembayaran->move($tujuan_upload, $nama_file_bukti_pembayaran);

        $pengajuan = Pengajuan::find($request->id_pengajuan);
        $pendaftaran = PendaftaranProposal::create([
            'file_dhs' => $nama_file_dhs,
            'file_laporan' => $nama_file_laporan,
            'file_krs' => $nama_file_krs,
            'file_bukti_pembayaran' => $nama_file_bukti_pembayaran,
            'id_pengajuan' => $request->id_pengajuan,

            'tanggal' => date("Y-m-d"),
            'waktu' => '-',
            'tempat' => '-',
            'status' => '0',
        ]);

        return redirect()->back()->with('success', 'Pendaftaran Proposal berhasil');
    }
}
