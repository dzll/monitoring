<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Adminacc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Dosen;
use App\Models\Notifikasi;
use App\Models\Pengajuan;
use App\Models\PendaftaranProposal;
use App\Models\Revisi;
use App\Models\Penguji;
use App\Models\ItemNilai;
use App\Models\NilaiDosen;


class DosenController extends Controller
{
    //
    // Profile Edit
    public function pengajuan()
    {
        $pengajuan = Notifikasi::where('id_dosen', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view('dosen.pengajuan')->with('pengajuan', $pengajuan);
    }

    public function jadwal()
    {
        // $jadwal
        return view('dosen.jadwal');
    }

    public function detailJadwal(Request $request)
    {
        return view('dosen.detail')->with('detail', PendaftaranProposal::find($request->id_pendaftaran));
    }

    public function fileLaporanDownload($id)
    {
        $file = PendaftaranProposal::find($id);

        return response()->download(public_path("data_file/" . $file->file_laporan));
    }

    public function revisi(Request $request)
    {
        $penguji = Penguji::where('id_pendaftaran', $request->id_pendaftaran)->where('id_dosen', Auth::id())->first();
        $nilai = DB::select("SELECT 
                            item_nilai.nama,
                            item_nilai.persentase,
                            nilai_dosen.id as id_nilai_dosen,
                            nilai_dosen.nilai,
                            ((item_nilai.persentase / 100) * nilai_dosen.nilai) as hasil_nilai_persentase 
                            FROM nilai_dosen
                            JOIN item_nilai
                            ON nilai_dosen.id_item_nilai = item_nilai.id
                            JOIN penguji
                            ON nilai_dosen.id_penguji = penguji.id
                            WHERE id_penguji = ".$penguji->id." ORDER BY id_nilai_dosen ASC;
        ");
        $nilai_akhir = 0;
        foreach ($nilai as $value) {
            $nilai_akhir += $value->hasil_nilai_persentase;
        }
        // return $nilai_akhir;
        $item_nilai = ItemNilai::orderBy('id')->get();
        return view('dosen.revisi')->with('detail', PendaftaranProposal::find($request->id_pendaftaran))->with('item_nilai', $item_nilai)->with('nilai_akhir', $nilai_akhir);
    }

    public function tambahRevisi(Request $request)
    {
        $this->validate($request, [
            'isi_revisi' => 'required',
        ]);

        $revisi = Revisi::create([
            'id_penguji' => $request->id_penguji,
            'bab_revisi' => $request->bab_revisi,
            'isi_revisi' => $request->isi_revisi,
            'status_revisi' => 0,
        ]);

        $rev = Revisi::where('id_penguji', $request->id_penguji)->get();
        if (count($rev) <= 1) {
            Penguji::find($request->id_penguji)->update([
                'tanggal_revisi' => date('Y-m-d'),
                'deadline_revisi' => date('Y-m-d', strtotime($revisi->created_at . ' + 14 days')),
            ]);
        }

        return response()->json([
            'status' => 201,
            'message'  => 'Berhasil menambah revisi',
        ]);
    }

    public function statusRevisiUpdate(Request $request)
    {
        Revisi::find($request->id_revisi)->update(['status_revisi' => 1]);
        return response()->json([
            'status' => 201,
            'message'  => 'Berhasil',
        ]);
    }

    public function hapusRevisi(Request $request)
    {
        Revisi::find($request->id_revisi)->delete();

        return response()->json([
            'status' => 201,
            'message'  => 'Berhasil hapus revisi',
        ]);
    }

    public function revisiPrint(Request $request)
    {
        $revisi = Revisi::where('id_penguji', $request->id_penguji)->orderBy('id', 'asc')->get();

        $hari = date("D", strtotime($request->tanggal));

        switch ($hari) {
            case 'Sun':
                $hari_nya = "Minggu";
                break;

            case 'Mon':
                $hari_nya = "Senin";
                break;

            case 'Tue':
                $hari_nya = "Selasa";
                break;

            case 'Wed':
                $hari_nya = "Rabu";
                break;

            case 'Thu':
                $hari_nya = "Kamis";
                break;

            case 'Fri':
                $hari_nya = "Jumat";
                break;

            case 'Sat':
                $hari_nya = "Sabtu";
                break;

            default:
                $hari_nya = "-";
                break;
        }

        $pdf = \PDF::loadView('dosen.print', [
            'data' => $revisi,
            'nama' => $request->nama,
            'npm' => $request->npm,
            'tanggal' => $request->tanggal,
            'hari' => $hari_nya,
            'penguji' => $request->penguji
        ])->setPaper('a4');
        return $pdf->stream();
    }

    public function tambahNilai(Request $request)
    {

        $this->validate($request, [
            'nilai' => 'required',
            'id_item_nilai' => 'required',
        ]);

        $jml = 0;
        foreach ($request->id_item_nilai as $key => $value) {
            if ($request->nilai[$key] == null) {
                $jml += 1;
            } 
        }
        // return $jml;

        $insert = [];
        if ($jml == 0) {
            foreach ($request->id_item_nilai as $key => $value) {
                $insert['id_penguji'] = $request->id_penguji[$key];
                $insert['id_item_nilai'] = $request->id_item_nilai[$key];
                $insert['nilai'] = $request->nilai[$key];
                NilaiDosen::create($insert);  
            } 
            
            return response()->json([
                'status' => 201,
                'message'  => 'Berhasil menambah nilai',
            ]);
        }

    }

    public function getNilai(Request $request)
    {
        $nilai = DB::select("SELECT 
                            item_nilai.id as id_item_nilai,
                            item_nilai.nama,
                            item_nilai.persentase,
                            nilai_dosen.id as id_nilai_dosen,
                            nilai_dosen.nilai,
                            ((item_nilai.persentase / 100) * nilai_dosen.nilai) as hasil_nilai_persentase 
                            FROM nilai_dosen
                            JOIN item_nilai
                            ON nilai_dosen.id_item_nilai = item_nilai.id
                            JOIN penguji
                            ON nilai_dosen.id_penguji = penguji.id
                            WHERE id_penguji = ".$request->id." ORDER BY id_nilai_dosen ASC;
        ");

        $jsonString = json_encode($nilai);
        return json_decode($jsonString);
    }

    public function editNilai(Request $request)
    {
        $this->validate($request, [
            'nilai' => 'required',
            'id_item_nilai' => 'required',
            'id_nilai_dosen' => 'required',
        ]);

        $update = [];
        foreach ($request->id_item_nilai as $key => $value) {
            if ($request->nilai[$key] == null) {
                $update['nilai'] = NilaiDosen::find($request->id_nilai_dosen[$key])->nilai;
            }else{
                $update['nilai'] = $request->nilai[$key];
            }
            NilaiDosen::find($request->id_nilai_dosen[$key])->update($update);  
        }
        
        return response()->json([
            'status' => 201,
            'message'  => 'Berhasil menambah nilai',
        ]);

    }

    public function profile($id)
    {
        $user = Dosen::find($id);
        return view('manage.profile.profile')->with('user', $user);
    }

    public function notification()
    {
        $req = Notifikasi::where('id_dosen', Auth::user()->id)->orderBy('id', 'DESC')->get();
        return view('notifikasi.dosen')->with('req', $req);
    }

    public function singleNotif()
    {
        $req = Notifikasi::where('id_dosen', Auth::user()->id)->where('status', 0)->orderBy('created_at', 'DESC')->get();
        return view('notifikasi.single.bimdos')->with('req', $req);
    }

    public function statusNo(Request $request)
    {
        Pengajuan::where(['id' => $request->id_pengajuan])->first()->update(['status' => '2']);
        Notifikasi::where(['id_pengajuan' => $request->id_pengajuan])->where('id_dosen', Auth::user()->id)->update(['status' => '1']);
        Notifikasi::where(['id_pengajuan' => $request->id_pengajuan])->where('id_dosen', Auth::user()->id)->update(['judul_status' => '2']);
        return redirect()->back()->with('success', 'Mahasiswa ' . $request->nama_mahasiswa . ' ditolak');
    }

    public function statusYes(Request $request)
    {
        // Pengajuan::where(['id' => $request->id_pengajuan])->first()->update(['status' => '1']);
        Notifikasi::where(['id_pengajuan' => $request->id_pengajuan])->where('id_dosen', Auth::user()->id)->update(['status' => '1']);
        Notifikasi::where(['id_pengajuan' => $request->id_pengajuan])->where('id_dosen', Auth::user()->id)->update(['judul_status' => '1']);
        $getID = Notifikasi::select('id_dosen')->where('id_pengajuan', $request->id_pengajuan)->get();

        $count_adm = Admin::all();
        if (count($getID) == 2) {
            $getACC = Notifikasi::where('id_pengajuan', $request->id_pengajuan)->where('judul_status', 1)->where('judul_status', '!=', 0)->get();
            if (count($getACC) == 2) {
                for ($i = 0; $i < count($count_adm); $i++) {
                    Adminacc::create([
                        'id_pengajuan' => $request->id_pengajuan,
                        'id_admin' => $count_adm[$i]->id,
                        'status' => 0,
                        'keterangan' => 0,
                        'aksi_dosen' => 1,
                    ]);
                }
            }
        } elseif (count($getID) == 1) {
            if (Notifikasi::where('id_pengajuan', $request->id_pengajuan)->where('judul_status', 1)->where('judul_status', '!=', 0)) {

                Adminacc::create([
                    'id_pengajuan' => $request->id_pengajuan,
                    'id_admin' => 1,
                    'status' => 0,
                    'keterangan' => 0,
                    'aksi_dosen' => 1,
                ]);
            }
        }

        return redirect()->back()->with('success', 'Mahasiswa ' . $request->nama_mahasiswa . ' di Approve');
    }


    public function profileUpdate(Request $request, $id)
    {

        if ($request->username & $request->email) {
            Dosen::find(Auth::user()->id)->update([
                'username' => $request->username,
                'email' => $request->email,
            ]);
        }
        if ($request->password_lama & $request->password_baru & $request->ulangi_password) {
            if (Hash::check($request->password_lama, Auth::user()->password)) {
                if ($request->password_baru === $request->ulangi_password) {
                    Dosen::find(Auth::user()->id)->update([
                        'password' => Hash::make($request->password_baru),
                    ]);
                    return redirect()->back()->with('success', 'Berhasil mengubah password');
                } else {
                    return redirect()->back()->with('error', 'Konfirmasi password tidak sesuai');
                }
            } else {
                return redirect()->back()->with('error', 'Password salah !!');
            }
        }

        return redirect()->back()->with('success', 'Berhasil mengubah data profil');
    }
}
