<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Penguji;
use App\Models\PendaftaranProposal;
use Redirect, Response;
use DataTables;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class PengujiController extends Controller
{
    //

    public function penjadwalan()
    {
        return view('admin.penjadwalan.index');
    }

    public function getDosen(Request $request)
    {
        $getDosen = PendaftaranProposal::where('id', $request->id)->first();

        $count_dosen = count($getDosen->pengajuan->notifikasi);

        $d = $getDosen->pengajuan->notifikasi->sortBy('id');

        echo json_encode(array("count_dosen"=>$count_dosen, 
                                "dosen_1"=>$d->first()->dosen->nama, 
                                "id_dosen_1"=>$d->first()->dosen->id, 
                                "dosen_2"=>$d->last()->dosen->nama,
                                "id_dosen_2"=>$d->last()->dosen->id ?? ''));
 
        // echo $getDosen->pengajuan->notifikasi->first()->dosen->nama;

        // echo $getDosen->pengajuan->notifikasi->dosen->nama;
    }

    public function tambahPenjadwalan(Request $request)
    {
        return view('admin.penjadwalan.tambah')->with('tanggal', $request->tanggal)->with('waktu', $request->waktu)->with('tempat', $request->tempat);
    }

    public function store(Request $request)
    {

        // dd($request);
        $validate = $this->validate($request, [
            'id_pendaftaran' => 'required',
            'tanggal' => 'required',
            'waktu' => 'required',
            'tempat' => 'required',
            // 'id_dosen' => 'required',
            // 'id_dosen_1' => 'required',
            // 'id_dosen_2' => 'required',
        ]);

        try {

            $penguji_1 = Penguji::create([
                'id_pendaftaran' => $request->id_pendaftaran,
                'id_dosen' => $request->id_dosen_1,
                'status' => 0,
            ]);

            $penguji_2 = Penguji::create([
                'id_pendaftaran' => $request->id_pendaftaran,
                'id_dosen' => $request->id_dosen_2,
                'status' => 0,
            ]);

            $getIdDosenPembimbing = PendaftaranProposal::find($request->id_pendaftaran);
            $pembimbing = Penguji::create([
                'id_pendaftaran' => $request->id_pendaftaran,
                'id_dosen' => $request->select_kehadiran_pembimbing,
                'status' => 0,
            ]);
            
            // foreach ($getIdDosenPembimbing->pengajuan->notifikasi as $value) {
            //     $pembimbing = Penguji::create([
            //         'id_pendaftaran' => $request->id_pendaftaran,
            //         'id_dosen' => $value->dosen->id,
            //         'status' => 0,
            //     ]);
            // }
            // xxxxxxxxxxxxxxxxxxxx
            // $pembimbing = Penguji::create([
            //     'id_pendaftaran' => $request->id_pendaftaran,
            //     'id_dosen' => $getIdDosenPembimbing->pengajuan->notifikasi->first()->dosen->id,
            //     'status' => 0,
            // ]);
            // xxxxxxxxxxxxxxxxxxxx

            $pendaftaran = PendaftaranProposal::find($request->id_pendaftaran)->update([
                'tanggal' => $request->tanggal,
                'waktu' => $request->waktu,
                'tempat' => $request->tempat,
                'status' => 1,
            ]);

            $pendaftaran = PendaftaranProposal::where('id', $request->id_pendaftaran)->get();
            foreach ($pendaftaran as $val) {
                $mhs_data = $val->pengajuan->mahasiswa;
            }
            $dosen_pembimbing = array();
            foreach ($getIdDosenPembimbing->pengajuan->notifikasi->sortBy('id') as &$value) {
                $dospem = $value->dosen->nama;
                $dosen_pembimbing[] = $dospem . '<br>';
            }
            $dosen_penguji_1 = DB::table('dosen')->where('id', $request->id_dosen_1)->first();
            $dosen_penguji_2 = DB::table('dosen')->where('id', $request->id_dosen_2)->first();
            // PHP Mailer
            $subject = "Jadwal sidang proposal - ITATS";
            $subjectDosen = "Jadwal sidang proposal - Dosen ITATS";
            $namaPengirim = "ITATS";
            $emailPengirim = "cs.ytguide@gmail.com";
            $messageMhs = '
                        <div align="center" style="background-color:white; border:1px solid #f0f0f0; border-radius: 10px; max-width: 480px; display: block; width: 100%;">
                            <br>
                            <img src="https://itats.ac.id/wp-content/uploads/2018/11/logo-itats.jpg" alt="Logo" width="195" align="left">
                            <br><br><br>
                            <h2 style="color:#0072bb;" align="center">Jadwal sidang proposal</h2>
                            <hr style="border-top: 1px solid #f0f0f0; margin-left:10px; margin-right:10px;">
                            <h3>' . $mhs_data->nama . '</h3>
                            <p>' . $mhs_data->npm . '</p>
                            <div align="center" style="background-color:#f0f2f5; padding:2px; margin-left:8px; margin-right:8px; border-radius: 10px;">
                                <h4 style="color:#0072bb;">JADWAL</h4>
                                <h3 style="color:#808080;">' . date('d-M-Y', strtotime($request->tanggal)) . ' | ' . $request->waktu . ' | ' . $request->tempat . '</h3>
                            </div>
                            <br>
                            <div align="center" style="background-color:#f0f2f5; padding:2px; margin-left:8px; margin-right:8px; border-radius: 10px;">
                                <h4 style="color:#0072bb;">PEMBIMBING</h4>
                                <h3 style="color:#808080;">' . implode($dosen_pembimbing) . '</h3>
                            </div>
                            <br>
                            <div align="center" style="background-color:#f0f2f5; padding:2px; margin-left:8px; margin-right:8px; border-radius: 10px;">
                                <h4 style="color:#0072bb;">PENGUJI</h4>
                                <h3 style="color:#808080;">' . $dosen_penguji_1->nama . '<br>' . $dosen_penguji_2->nama . '</h3>
                            </div>
                            <hr style="border-top: 1px solid #f0f0f0; margin-left:10px; margin-right:10px;">
                            <div align="center" style="background-color:#0072bb; color:#fff; padding:2px; margin-left:8px; margin-right:8px; border-radius: 10px;" >
                                <h4>INSTITUT TEKNOLOGI ADHI TAMA SURABAYA <br>Copyright &copy; 2021</h4>
                            </div>
                            <br>
                        </div>
            ';

            $messageDosen = '
                        <div align="center" style="background-color:white; border:1px solid #f0f0f0; border-radius: 10px; max-width: 480px; display: block; width: 100%;">
                            <br>
                            <img src="https://itats.ac.id/wp-content/uploads/2018/11/logo-itats.jpg" alt="Logo" width="195" align="left">
                            <br><br><br>
                            <h3 style="color:#0072bb; margin-left:10px; margin-right:10px;" align="left">Jadwal : ' . date('d-M-Y', strtotime($request->tanggal)) . ' | ' . $request->waktu . ' | ' . $request->tempat . '</h3>
                            <hr style="border-top: 1px solid #f0f0f0; margin-left:10px; margin-right:10px;">
                            <h3 align="left" style="margin-left:10px; margin-right:10px;">' . $mhs_data->nama . '</h3>
                            <p align="left" style="margin-left:10px; margin-right:10px;">' . $mhs_data->npm . '</p>
                            <div align="center" style="background-color:#f0f2f5; padding:2px; margin-left:8px; margin-right:8px; border-radius: 10px;">
                                <h4 style="color:#0072bb;">PEMBIMBING</h4>
                                <h3 style="color:#808080;">' . implode($dosen_pembimbing) . '</h3>
                                <h4 style="color:#0072bb;">PENGUJI</h4>
                                <h3 style="color:#808080;">' . $dosen_penguji_1->nama . '<br>' . $dosen_penguji_2->nama . '</h3>
                            </div>
                            <hr style="border-top: 1px solid #f0f0f0; margin-left:10px; margin-right:10px;">
                            <div align="center" style="background-color:#0072bb; color:#fff; padding:2px; margin-left:8px; margin-right:8px; border-radius: 10px;" >
                                <h4>INSTITUT TEKNOLOGI ADHI TAMA SURABAYA <br>Copyright &copy; 2021</h4>
                            </div>
                            <br>
                        </div>
            ';

            // siji ae +++++++++++++
            $mail = new PHPMailer(true);
            // if (count($getIdDosenPembimbing->pengajuan->notifikasi) > 1) {
            //     $toSend = array(
            //         DB::table('dosen')->where('id', $getIdDosenPembimbing->pengajuan->notifikasi->first()->dosen->id)->first()->email => DB::table('dosen')->where('id', $getIdDosenPembimbing->pengajuan->notifikasi->first()->dosen->id)->first()->nama, 
            //         DB::table('dosen')->where('id', $getIdDosenPembimbing->pengajuan->notifikasi->last()->dosen->id)->first()->email => DB::table('dosen')->where('id', $getIdDosenPembimbing->pengajuan->notifikasi->last()->dosen->id)->first()->nama, 
            //         $dosen_penguji_1->email => $dosen_penguji_1->nama, 
            //         $dosen_penguji_2->email => $dosen_penguji_2->nama
            //     );
            // } else {
            $toSend = array(
                DB::table('dosen')->where('id', $request->select_kehadiran_pembimbing)->first()->email => DB::table('dosen')->where('id', $request->select_kehadiran_pembimbing)->first()->nama, 
                $dosen_penguji_1->email => $dosen_penguji_1->nama, 
                $dosen_penguji_2->email => $dosen_penguji_2->nama
            );
            // }

            // Pengaturan Server
            // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'cs.ytguide@gmail.com';
            $mail->Password = '08155198171';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            $mail->setFrom($emailPengirim, $namaPengirim);
            $mail->addReplyTo($emailPengirim, $namaPengirim);

            foreach ($toSend as $email => $nama) {
                $mail->ClearAllRecipients();
                $mail->addAddress($email, $nama);
                $mail->isHTML(true);
                $mail->Subject = $subjectDosen;
                $mail->Body    = $messageDosen;
                $mail->AltBody = $messageDosen;
                $mail->send();
            }

            $mail->ClearAllRecipients();
            $mail->addAddress($mhs_data->email, $mhs_data->nama);
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $messageMhs;
            $mail->AltBody = $messageMhs;
            $mail->send();

            $mail->SmtpClose();


            return response()->json([
                'status' => 201,
                'message'  => 'Berhasil menambah jadwal',
            ]);
        } catch (Exception $e) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }

        // return view('admin.penjadwalan.tambah')->with('tanggal', $request->tanggal)->with('waktu', $request->waktu)->with('tempat', $request->tempat);
        // return redirect()->back()->with('success', 'Berhasil menambah jadwal');
    }

    public function fullPrint(Request $request)
    {
        // dd($request);
        // $returnHTML = view('dashboard.admin')->render();
        // return response()->json(array('success' => true, 'html'=>$returnHTML));

        // return response()->json([
        //     'data' => $request->id,
        //     'status' => 200,
        //     'message'  => 'Berhasil menambah jadwal',
        // ]);

        // $ex_id = [1, 2];

        $jadwal = PendaftaranProposal::whereIn('id', explode(',', $request->id))->orderBy('id', 'asc')->get();
        $pdf = \PDF::loadView('admin.penjadwalan.full_print', ['data' => $jadwal])->setPaper('a4', 'landscape');

        return $pdf->stream();
    }

    public function print(Request $request)
    {
        $jadwal = PendaftaranProposal::where('tanggal', $request->tanggal)->get();

        $pdf = \PDF::loadView('admin.penjadwalan.print', ['data' => $jadwal])->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function printBeritaAcara(Request $request)
    {

        // return $request->pendaftaran_id;
        $berita_acara = PendaftaranProposal::find($request->pendaftaran_id);
        $pembimbing = Penguji::where('id_pendaftaran', $request->pendaftaran_id)->orderBy('id')->get();
        $penguji = Penguji::where('id_pendaftaran', $request->pendaftaran_id)->where('id_dosen', '<>', $pembimbing->last()->dosen->id)->orderBy('id')->get();

        $nilai_pembimbing = Penguji::where('id_pendaftaran', $request->pendaftaran_id)->where('id_dosen', $pembimbing->last()->dosen->id)->first();
        $nilai_penguji_1 = Penguji::where('id_pendaftaran', $request->pendaftaran_id)->where('id_dosen', $penguji->first()->dosen->id)->first();
        $nilai_penguji_2 = Penguji::where('id_pendaftaran', $request->pendaftaran_id)->where('id_dosen', $penguji->last()->dosen->id)->first();
        $nilai_pmbb = DB::select("SELECT item_nilai.nama, item_nilai.persentase, nilai_dosen.id as id_nilai_dosen, nilai_dosen.nilai, ((item_nilai.persentase / 100) * nilai_dosen.nilai) as hasil_nilai_persentase 
                FROM nilai_dosen JOIN item_nilai ON nilai_dosen.id_item_nilai = item_nilai.id JOIN penguji ON nilai_dosen.id_penguji = penguji.id
                WHERE id_penguji = ".$nilai_pembimbing->id." ORDER BY id_nilai_dosen ASC;
        ");
        $nilai_akhir_pmbb = 0;
        foreach ($nilai_pmbb as $value) {
            $nilai_akhir_pmbb += $value->hasil_nilai_persentase;
        }
        $nilai_pngj_1 = DB::select("SELECT item_nilai.nama, item_nilai.persentase, nilai_dosen.id as id_nilai_dosen, nilai_dosen.nilai, ((item_nilai.persentase / 100) * nilai_dosen.nilai) as hasil_nilai_persentase 
                FROM nilai_dosen JOIN item_nilai ON nilai_dosen.id_item_nilai = item_nilai.id JOIN penguji ON nilai_dosen.id_penguji = penguji.id
                WHERE id_penguji = ".$nilai_penguji_1->id." ORDER BY id_nilai_dosen ASC;
        ");
        $nilai_akhir_pngj_1 = 0;
        foreach ($nilai_pngj_1 as $value) {
            $nilai_akhir_pngj_1 += $value->hasil_nilai_persentase;
        }
        $nilai_pngj_2 = DB::select("SELECT item_nilai.nama, item_nilai.persentase, nilai_dosen.id as id_nilai_dosen, nilai_dosen.nilai, ((item_nilai.persentase / 100) * nilai_dosen.nilai) as hasil_nilai_persentase 
                FROM nilai_dosen JOIN item_nilai ON nilai_dosen.id_item_nilai = item_nilai.id JOIN penguji ON nilai_dosen.id_penguji = penguji.id
                WHERE id_penguji = ".$nilai_penguji_2->id." ORDER BY id_nilai_dosen ASC;
        ");
        $nilai_akhir_pngj_2 = 0;
        foreach ($nilai_pngj_2 as $value) {
            $nilai_akhir_pngj_2 += $value->hasil_nilai_persentase;
        }

        $hari = date("D", strtotime($berita_acara->tanggal));
        switch ($hari) {
            case 'Sun':
                $hari_nya = "Minggu";
                break;

            case 'Mon':
                $hari_nya = "Senin";
                break;

            case 'Tue':
                $hari_nya = "Selasa";
                break;

            case 'Wed':
                $hari_nya = "Rabu";
                break;

            case 'Thu':
                $hari_nya = "Kamis";
                break;

            case 'Fri':
                $hari_nya = "Jumat";
                break;

            case 'Sat':
                $hari_nya = "Sabtu";
                break;

            default:
                $hari_nya = "-";
                break;
        }

        $pdf = \PDF::loadView('admin.penjadwalan.berita_acara_print', [
            'data' => $berita_acara,
            'hari' => $hari_nya,
            'tanggal' => date('d F Y', strtotime($berita_acara->tanggal)),
            'penguji_1' => $penguji->first()->dosen->nama,
            'penguji_2' => $penguji->last()->dosen->nama,
            'pembimbing' => $pembimbing->last()->dosen->nama,
            'nilai_penguji_1' => $nilai_akhir_pngj_1,
            'nilai_penguji_2' => $nilai_akhir_pngj_2,
            'nilai_pembimbing' => $nilai_akhir_pmbb,
            ])->setPaper('a4');
        return $pdf->stream();
    }
}
