<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Pengajuan;
use App\Models\Notifikasi;
use ArrayObject;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB as FacadesDB;

class PengajuanController extends Controller
{
    //
    public function view()
    {
        $pengajuan = Pengajuan::where('id_mahasiswa', Auth::user()->id)->orderBy('id', 'DESC')->get();
        $cek_status = Pengajuan::where('id_mahasiswa', Auth::user()->id)->where('status', '<>', 2)->first();
        // dd($cek_status);
        // $temp = new Collection();
        $cek_dosen = new Collection();

        if (!$pengajuan->isEmpty()) {
            for ($i = 0; $i < count($pengajuan); $i++) {
                // $cek = Notifikasi::where('id_pengajuan', $pengajuan[$i]->id)->get();
                $cekdos = Notifikasi::select('id_dosen')->where('id_pengajuan', $pengajuan[$i]->id)->orderBy('id', 'ASC')->get();
                // $temp->push($cek);
                $cek_dosen->push($cekdos);
            }
        } else {
            // $temp = [];
            $cek_dosen = [];
        }


        return view('mahasiswa.pengajuan')->with('pengajuan', $pengajuan)->with('cek_status', $cek_status)->with('cek_dosen', $cek_dosen);
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul_proposal' => 'required',
            'id_dosen' => 'required',
        ]);

        $pengajuan = Pengajuan::create([
            'judul_proposal' => $request->judul_proposal,
            'id_mahasiswa' => $request->id_mahasiswa,
            'status' => 0,
        ]);


        $dosen = $request->id_dosen_2;
        if ($dosen == null) {
            Notifikasi::create([
                'id_dosen' => $request->id_dosen,
                'id_pengajuan' => $pengajuan->id,
                'status' => 0,
                'judul_status' => 0,
            ]);
        } else {
            Notifikasi::insert(
                [
                    [
                        'id_dosen' => $request->id_dosen,
                        'id_pengajuan' => $pengajuan->id,
                        'status' => 0,
                        'judul_status' => 0,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    ],
                    [
                        'id_dosen' => $request->id_dosen_2,
                        'id_pengajuan' => $pengajuan->id,
                        'status' => 0,
                        'judul_status' => 0,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    ]
                ]
            );
        }

        return redirect()->back()->with('success', 'Pengajuan berhasil ditambahkan');
    }

    public function history(Request $request)
    {
        // return $request;
        $riwayat = Pengajuan::where('id_mahasiswa', Auth::user()->id)->orderBy('id', 'DESC')->get();
        Notifikasi::where(['id' => $request->id])->first()->update(['status' => '2']);
        return view('notifikasi.single.mhs')->with('riwayat', $riwayat);
    }
}
