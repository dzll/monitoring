<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Mahasiswa;
use App\Models\Revisi;

class MahasiswaController extends Controller
{
    //
    public function jadwal()
    {
        return view('mahasiswa.jadwal');
    }

    public function getRevisi(Request $request)
    {
        $revisi = Revisi::where('id_penguji', $request->id)->orderBy('created_at', 'asc')->get();

        return json_decode($revisi);
    }

    public function revisiPrint(Request $request)
    {
        $revisi = Revisi::where('id_penguji', $request->id_penguji)->orderBy('id', 'asc')->get();

        $hari = date("D", strtotime($request->tanggal));

        switch ($hari) {
            case 'Sun':
                $hari_nya = "Minggu";
                break;

            case 'Mon':
                $hari_nya = "Senin";
                break;

            case 'Tue':
                $hari_nya = "Selasa";
                break;

            case 'Wed':
                $hari_nya = "Rabu";
                break;

            case 'Thu':
                $hari_nya = "Kamis";
                break;

            case 'Fri':
                $hari_nya = "Jumat";
                break;

            case 'Sat':
                $hari_nya = "Sabtu";
                break;

            default:
                $hari_nya = "-";
                break;
        }

        $pdf = \PDF::loadView('dosen.print', [
            'data' => $revisi,
            'nama' => $request->nama,
            'npm' => $request->npm,
            'tanggal' => $request->tanggal,
            'hari' => $hari_nya,
            'penguji' => $request->penguji
        ])->setPaper('a4');
        return $pdf->stream();
    }

    // Profile Edit
    public function profile($id)
    {
        $user = Mahasiswa::find($id);
        return view('manage.profile.profile')->with('user', $user);
    }

    public function profileUpdate(Request $request, $id)
    {
        if ($request->username & $request->email) {
            Mahasiswa::find(Auth::user()->id)->update([
                'username' => $request->username,
                'email' => $request->email,
            ]);
        }
        if ($request->password_lama & $request->password_baru & $request->ulangi_password) {
            if (Hash::check($request->password_lama, Auth::user()->password)) {
                if ($request->password_baru === $request->ulangi_password) {
                    Mahasiswa::find(Auth::user()->id)->update([
                        'password' => Hash::make($request->password_baru),
                    ]);
                    return redirect()->back()->with('success', 'Berhasil mengubah password');
                } else {
                    return redirect()->back()->with('error', 'Konfirmasi password tidak sesuai');
                }
            } else {
                return redirect()->back()->with('error', 'Password salah !!');
            }
        }

        return redirect()->back()->with('success', 'Berhasil mengubah data profil');
    }
}
