<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use App\Models\Adminacc;
use App\Models\Dosen;
use App\Models\Notifikasi;
use App\Models\Pengajuan;
use App\Models\PendaftaranProposal;
use Illuminate\Support\Collection;

class AdminController extends Controller
{
    //
    public function pengajuan()
    {
        $pengajuan = Pengajuan::orderBy('id', 'DESC')->get();

        $cek_dosen = new Collection();

        if (!$pengajuan->isEmpty()) {
            for ($i = 0; $i < count($pengajuan); $i++) {
                // $cek = Notifikasi::where('id_pengajuan', $pengajuan[$i]->id)->get();
                $cekdos = Notifikasi::where('id_pengajuan', $pengajuan[$i]->id)->orderBy('id', 'ASC')->get();
                // $temp->push($cek);
                $cek_dosen->push($cekdos);
            }
        } else {
            // $temp = [];
            $cek_dosen = [];
        }
        // return $cek_dosen;

        return view('admin.pengajuan.index')->with('pengajuan', $pengajuan)->with('cek_dosen', $cek_dosen);
    }

    public function pengajuanAcc(Request $request)
    {
        Pengajuan::where(['id' => $request->id_pengajuan])->first()->update(['status' => '1']);
        Adminacc::where(['id_pengajuan' => $request->id_pengajuan])->update(['status' => 1]);
        return redirect()->back()->with('success', 'Mahasiswa ' . $request->nama_mahasiswa . ' di Approve');
    }

    public function pengajuanTolak(Request $request)
    {
        Pengajuan::where(['id' => $request->id_pengajuan])->first()->update(['status' => '2']);
        Adminacc::where(['id_pengajuan' => $request->id_pengajuan])->update(['status' => 1]);
        return redirect()->back()->with('success', 'Mahasiswa ' . $request->nama_mahasiswa . ' ditolak');
    }

    public function pendaftaran()
    {
        return view('admin.pendaftaran.index')->with('pendaftaran', PendaftaranProposal::all()->sortByDesc('id'));
    }

    public function detailPendaftaran($id)
    {
        return view('admin.pendaftaran.detail')->with('detail', PendaftaranProposal::find($id));
    }

    public function fileDhsDownload($id)
    {
        $file = PendaftaranProposal::find($id);

        return response()->download(public_path("data_file/" . $file->file_dhs));
    }

    public function fileKrsDownload($id)
    {
        $file = PendaftaranProposal::find($id);

        return response()->download(public_path("data_file/" . $file->file_krs));
    }

    public function fileLaporanDownload($id)
    {
        $file = PendaftaranProposal::find($id);

        return response()->download(public_path("data_file/" . $file->file_laporan));
    }

    public function fileBuktiPembayaranDownload($id)
    {
        $file = PendaftaranProposal::find($id);

        return response()->download(public_path("data_file/" . $file->file_bukti_pembayaran));
    }

    public function manageDosen()
    {
        return view('manage.dosen.index')->with('dosen', Dosen::all());
    }

    public function manageDosenStore(Request $request)
    {
        Dosen::create([
            'username' => $request->email,
            'password' => Hash::make($request->email),
            'nama' => $request->nama,
            'spesialis' => $request->spesialis,
            'email' => $request->email,
            'role' => 2,
        ]);

        return redirect()->back()->with('success', 'Berhasil menambah dosen');
    }

    public function manageDosenUpdate(Request $request, $id)
    {
    }

    // Profile Edit
    public function profile($id)
    {
        $user = Admin::find($id);
        return view('manage.profile.profile')->with('user', $user);
    }

    public function profileUpdate(Request $request, $id)
    {

        if ($request->username & $request->email) {
            Admin::find(Auth::user()->id)->update([
                'username' => $request->username,
                'email' => $request->email,
            ]);
        }
        if ($request->password_lama & $request->password_baru & $request->ulangi_password) {
            if (Hash::check($request->password_lama, Auth::user()->password)) {
                if ($request->password_baru === $request->ulangi_password) {
                    Admin::find(Auth::user()->id)->update([
                        'password' => Hash::make($request->password_baru),
                    ]);
                    return redirect()->back()->with('success', 'Berhasil mengubah password');
                } else {
                    return redirect()->back()->with('error', 'Konfirmasi password tidak sesuai');
                }
            } else {
                return redirect()->back()->with('error', 'Password salah !!');
            }
        }

        return redirect()->back()->with('success', 'Berhasil mengubah data profil');
    }
}
