@if(count($errors) > 0)
    @foreach ($errors->all() as $error)
        <script>
          $(function() {
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 3000
            });
        
            $(function() {
              toastr.error("{{$error}}")
            });
          });
        </script>
    @endforeach
@endif

@if(session('success'))
   <script>
     $(function() {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });
    
        $(function() {
          toastr.success("{{session('success')}}")
        });
      });
    </script>
@endif

@if(session('info'))
    <script>
      $(function() {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });
    
        $(function() {
          toastr.info("{{session('info')}}")
        });
      });
    </script>
@endif

@if(session('error'))
    <script>
      $(function() {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });
    
        $(function() {
          toastr.error("{{session('error')}}")
        });
      });
    </script>
@endif

{{-- $('.toastrDefaultSuccess').click(function() {
  toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
}); --}}
{{-- $('.toastrDefaultInfo').click(function() {
  toastr.info('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
});
$('.toastrDefaultError').click(function() {
  toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
});
$('.toastrDefaultWarning').click(function() {
  toastr.warning('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
}); --}}

