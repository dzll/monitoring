@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pendaftaran Proposal</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
            <div class="card-header border-transparent">
                <h3 class="card-title">Data Pendaftaran</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                <table class="table" id="tablePendaftaran">
                    <thead>
                    <tr>
                    <th style="width:5%">No</th>
                    <th style="width:10%">NPM</th>
                    <th style="width:20%">Nama</th>
                    <th style="width:35%">Judul</th>
                    <th style="width:15%">Tanggal</th>
                    <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    @php
                    $i = 0;
                    @endphp
                    <tbody>
                    @foreach ($pendaftaran as $item)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $item->pengajuan->mahasiswa->npm ?? '' }}</td>
                            <td>{{ $item->pengajuan->mahasiswa->nama ?? '' }}</td>
                            <td>{{ $item->pengajuan->judul_proposal ?? '' }}</td>
                            <td>{{ date("d-M-Y H:i:s",strtotime($item->created_at ?? '')) }}</td>
                            <td>
                                {{-- <div class="row"> --}}
                                <a href="/user/admin/pendaftaran/{{ $item->id }}/detail" class="btn btn-info btn-sm" title="Detail">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                {{-- </div> --}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            <!-- /.card-body -->
            <!-- /.card-footer -->
            </div>
            <!-- /.card -->


        </div><!--/. container-fluid -->
      </section>
      


    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@push('js')

    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script>
        $(function () {
          $("#tablePendaftaran").DataTable({
            "responsive": true,
            "autoWidth": false,
          });
        });
    </script>

@endpush