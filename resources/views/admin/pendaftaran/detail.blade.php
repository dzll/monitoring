@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Detail Pendaftaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
          <div class="invoice p-3 mb-3">
            <h4>
              <i class="fa fa-list-ol"></i> &nbsp; Detail Pendaftaran Proposal.
            </h4>
            <br>
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                Nama
                <address>
                  <strong>{{$detail->pengajuan->mahasiswa->nama ??''}}</strong>
                </address>
              </div>
              <div class="col-sm-4 invoice-col">
                NPM
                <address>
                  <strong>{{$detail->pengajuan->mahasiswa->npm ??''}}</strong>
                </address>
              </div>
              <div class="col-sm-4 invoice-col">
                <b>Tanggal Pendaftaran</b>
                <br>
                {{ date("d-M-Y H:i:s",strtotime($detail->created_at ?? '')) }}<br>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th width="100%">Judul Proposal</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{{$detail->pengajuan->judul_proposal ??''}}</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div><br>
            <div class="row">
              <div class="col-12">
  
                <div class="table-responsive">
                  <table class="table">
                    <tr style="background-color:#d9ffdd;">
                      <th style="width:20%">Dosen Pembimbing</th>
                      <td>
                        @foreach ($detail->pengajuan->notifikasi->sortBy('id') as $value)
                          {{ $value->dosen->nama ?? '' }}
                          <br>  
                        @endforeach
                      </td>
                    </tr>
                    <tr>
                      <th>File DHS</th>
                      <td><a href="/user/admin/pendaftaran/{{ $detail->id ?? '' }}/file_dhs/download" class="btn btn-success btn-sm"><i class="fa fa-download"></i> &nbsp; Download</a> &nbsp;{{$detail->file_dhs ??''}}</td>
                    </tr>
                    <tr>
                      <th>File KRS</th>
                      <td><a href="/user/admin/pendaftaran/{{ $detail->id ?? '' }}/file_krs/download" class="btn btn-success btn-sm"><i class="fa fa-download"></i> &nbsp; Download</a> &nbsp;{{$detail->file_krs ??''}}</td>
                      {{-- {{ \Illuminate\Support\Str::limit($string, 150, $end='...') }} --}}
                    </tr>
                    <tr>
                      <th>File Laporan</th>
                      <td><a href="/user/admin/pendaftaran/{{ $detail->id ?? '' }}/file_laporan/download" class="btn btn-success btn-sm"><i class="fa fa-download"></i> &nbsp; Download</a> &nbsp;{{$detail->file_laporan ??''}}</td>
                    </tr>
                    <tr>
                      <th>Bukti Pembayaran</th>
                      <td><a href="/user/admin/pendaftaran/{{ $detail->id ?? '' }}/file_bukti_pembayaran/download" class="btn btn-success btn-sm"><i class="fa fa-download"></i> &nbsp; Download</a> &nbsp;{{$detail->file_bukti_pembayaran ??''}}</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
            <br><br>
              <div class="row no-print">
                <div class="col-12">
                  <a href="/user/admin/pendaftaran" class="btn btn-default float-right mt-2"><i class="fa fa-undo"></i> &nbsp;Kembali</a>
                </div>
              </div>
          </div>
        </div>
      </section>

  </div>
  <!-- /.content-wrapper -->

@endsection
