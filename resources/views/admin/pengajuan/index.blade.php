@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pengajuan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">

              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Daftar Pengajuan</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="tablePengajuan" class="table">
                    <thead>
                      <tr>
                        <th style="width:5%">No</th>
                        <th style="width:10%">NPM</th>
                        <th style="width:15%">Nama</th>
                        <th style="width:25%">Judul</th>
                        <th style="width:15%">Pembimbing 1</th>
                        <th style="width:15%">Pembimbing 2</th>
                        <th style="width:10%">Status</th>
                        @php $countloop = 0; @endphp
                        @foreach ($pengajuan as $item) 
                          @if ($item->status == 0 && $item->notifikasi->first()->judul_status == 1 && $item->notifikasi->last()->judul_status == 1)
                            <?php if ( $countloop == 1) break; ?>
                              <th style="width:10%">Aksi</th>
                            <?php $countloop++; ?>
                          @endif
                        @endforeach
                      </tr>
                    </thead>
                    <tbody>
                    @php
                      $i=0
                      
                    @endphp
                    @php
                      $dosenarr=0;
                      $arr=0;
                    @endphp
                    @foreach ($pengajuan as $item) 
                      <tr>
                          <td>{{++$i}}</td>
                          <td>{{$item->mahasiswa->npm ?? ''}}</td>
                          <td>{{$item->mahasiswa->nama ?? ''}}</td>
                          <td>{{$item->judul_proposal ?? ''}}</td>
                            @if (count($cek_dosen[$dosenarr])==2)
                              @foreach ($cek_dosen[$arr] as $dosen)
                                @if ($dosen->judul_status == 1)
                                  <td style="background-color: #daffd1">
                                  <i class="fa fa-check"></i>
                                @elseif ($dosen->judul_status == 2)
                                  <td style="background-color: #ffcccc">
                                  <i class="fa fa-times"></i>
                                @else
                                  <td style="background-color: #ffffd4">
                                  <i class="fa fa-clock-o"></i>
                                @endif
                                {{$dosen->dosen->nama ?? ''}}
                                </td>
                              @endforeach
                            @else
                              @foreach ($cek_dosen[$arr] as $dosen)
                                @if ($dosen->judul_status == 1)
                                  <td style="background-color: #daffd1">
                                  <i class="fa fa-check"></i>
                                @elseif ($dosen->judul_status == 2)
                                  <td style="background-color: #ffcccc">
                                  <i class="fa fa-times"></i>
                                @else
                                  <td style="background-color: #ffffd4">
                                  <i class="fa fa-clock-o"></i>
                                @endif
                                {{$dosen->dosen->nama ?? ''}}
                                </td>
                                <td>-</td>
                              @endforeach
                              
                            @endif
                            @php $dosenarr++; $arr++; @endphp

                          @if ($item->status == 0)
                              <td><span class="badge badge-warning"><i class="fa fa-clock-o"></i> Menunggu</span></td>
                              @if ( $item->notifikasi->first()->judul_status == 1 && $item->notifikasi->last()->judul_status == 1)
                                <td>
                                    <div class="row">
                                    <form action="/user/admin/pengajuan/acc" method="POST">
                                        @csrf
                                        <input type="hidden" name="id_pengajuan" value="{{$item->id}}">
                                        <input type="hidden" name="nama_mahasiswa" value="{{$item->mahasiswa->nama ?? ''}}">
                                        <button class="btn btn-success btn-sm" type="submit" title="Approve">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                    &nbsp;
                                    <form action="/user/admin/pengajuan/tolak" method="POST">
                                        @csrf
                                        <input type="hidden" name="id_pengajuan" value="{{$item->id}}">
                                        <input type="hidden" name="nama_mahasiswa" value="{{$item->mahasiswa->nama ?? ''}}">
                                        <button class="btn btn-danger btn-sm" type="submit" title="Tolak">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                    </div>
                                </td>
                              @endif
                          @elseif ($item->status == 1)
                              <td><span class="badge badge-success"><i class="fa fa-check"></i> Approve</span></td>
                          @elseif ($item->status == 2)
                              <td><span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span></td>
                          @endif
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
      </section>

  </div>

@endsection

@push('js')

    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script>
        $(function () {
          $("#tablePengajuan").DataTable({
            "responsive": true,
            "autoWidth": false,
          });
        });
    </script>

@endpush