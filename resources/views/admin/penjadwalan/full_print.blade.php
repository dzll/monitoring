<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            html{
                font-size: 12px;
                font-family: Arial, Helvetica, sans-serif;
                margin-right: 30px;
                margin-left: 30px;
                margin-top: 10px;
            }
            .heading{
                text-align: center;
            }
            table {
                border-collapse: collapse;
                width: 100%;
            }
            .th-item {
                padding: 7px;
                text-align: left;
                border: 1px solid rgb(95, 95, 95);
            }
            .td-item {
                padding-left: 7px;
                padding-right: 7px;
                padding-top: 5px;
                padding-bottom: 5px;
                text-align: left;
                border: 1px solid rgb(95, 95, 95);
            
            }
            .th-item-no, .td-item-no {
                padding: 0px;
                text-align: center;
                border: 1px solid rgb(95, 95, 95);
            }
            .footer{
                padding:2px;
                font-size: 10px;
            }

        </style>
    </head>
    <body>

        {{-- {{dd($data)}} --}}
        <div class="heading">
            <h2>JADWAL PESERTA PROPOSAL SKRIPSI</h2>
            <h3>TEKNIK INFORMATIKA - ITATS</h3>
        </div>
    
        <br>

        <table>
            <thead>
            <tr>
                <th class="th-item-no">No</th>
                <th class="th-item">Npm</th>
                <th class="th-item">Nama</th>
                <th class="th-item">Pembimbing</th>
                <th class="th-item">Penguji</th>
                <th class="th-item">Jadwal</th>
            </tr>
            </thead>
            <tbody>
                @php
                    $no = 0
                @endphp
                @foreach($data as $data)
                    @php
                        $penguji = App\Models\Penguji::where('id_pendaftaran', $data->id)->get();
                    @endphp
                    @if (count($penguji) > 0)
                    <tr>
                        <td class="td-item-no">{{ ++$no }}</td>
                        <td class="td-item">{{ $data->pengajuan->mahasiswa->npm }}</td>
                        <td class="td-item">{{ $data->pengajuan->mahasiswa->nama }}</td>
                        <td class="td-item">
                            @foreach ($data->pengajuan->notifikasi->sortBy('id') as $item)
                                {{ $item->dosen->nama }} <br>
                            @endforeach
                        </td>
                        <td class="td-item">
                            @foreach (App\Models\Penguji::where('id_pendaftaran', $data->id)->where('id_dosen','<>', $data->pengajuan->notifikasi->first()->dosen->id)->where('id_dosen','<>', $data->pengajuan->notifikasi->last()->dosen->id)->get() as $penguji)
                                {{ $penguji->dosen->nama }} <br>
                            @endforeach
                        </td>
                        <td class="td-item">
                            {{ date('d-M-Y', strtotime($data->tanggal ??'')) }} <br>
                            {{ $data->waktu ?? '' }} <br>
                            {{ $data->tempat ?? '' }}
                        </td>
                    </tr>
                    @endif
                @endforeach
            </tbody>
        </table>

        {{-- <div class="footer">
            Copyright &copy; 2021 ITATS. All rights reserved.
        </div> --}}
    </body>
</html>