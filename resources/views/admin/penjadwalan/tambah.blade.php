@extends('layout.app')

@push('css')
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/bootstrap-clockpicker.min.css')}}">
@endpush

@push('js')
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  {{-- <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script> --}}

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>
@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Penjadwalan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
          <div class="callout callout-info">
            <h5 class="m-2"><i class="fa fa-calendar"></i> &nbsp; {{date('d-M-Y', strtotime($tanggal ??''))}} : ({{$waktu}}) | {{ $tempat }}</h5>
          </div>
          <form id="formJadwal" name="formJadwal" class="row">
            @csrf
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Tambah Mahasiswa</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                          <div class="col-md-7">
                            <div class="callout callout-success">
                              <div class="form-group ml-2">
                                  <label for="id_pendaftaran" class="mb-3">Mahasiswa</label>
                                  <select class="form-control" name="id_pendaftaran" id="id_pendaftaran" required>
                                      <option disabled selected>- Pilih Mahasiswa  -</option>
                                      @foreach (App\Models\PendaftaranProposal::whereColumn('created_at', 'updated_at')->get() as $data)
                                        <option value="{{ $data->id }}">{{ $data->pengajuan->mahasiswa->npm }} - {{ $data->pengajuan->mahasiswa->nama }}
                                          
                                          {{-- pembimbing : {{ $data->pengajuan->notifikasi->first()->dosen->nama ?? '' }}
                                          & {{ $data->pengajuan->notifikasi->last()->dosen->nama ?? '' }} --}}
                                        
                                        </option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="callout callout-success">
                              <div class="row mb-3">
                                {{-- <div class="col-md-6" id="col_dosen_1">
                                  <label for="dosen" class="ml-2 mb-3">Pembimbing 1</label>
                                  <div class="form-group ml-2">
                                    <h2 class="form-control" id="dosen_pembimbing_1">...</h2>
                                  </div>
                                </div> --}}
                                <div class="col-md-6" id="col_dosen_1">
                                  <label for="dosen" class="ml-2 mb-3">Pembimbing 1</label>
                                  <div class="input-group ml-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><input type="radio" value="" id="kehadiran_pembimbing_1" name="select_kehadiran_pembimbing" checked></span>
                                    </div>
                                    <h2 class="form-control" id="dosen_pembimbing_1">...</h2> 
                                  </div>
                                </div>
                                <div class="col-md-6" id="col_dosen_2">
                                  <label for="dosen" class="mb-3">Pembimbing 2</label>
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><input type="radio" value="" id="kehadiran_pembimbing_2" name="select_kehadiran_pembimbing"></span>
                                    </div>
                                    <h2 class="form-control" id="dosen_pembimbing_2">...</h2> 
                                  </div>
                                </div>
                                {{-- <div class="col-md-6" id="col_dosen_2">
                                  <label for="dosen" class="ml-2 mb-3">Pembimbing 2</label>
                                  <div class="form-group ml-2">
                                    <h2 class="form-control" id="dosen_pembimbing_2">...</h2>
                                  </div>
                                </div> --}}
                              </div>
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="callout callout-success">
                                <label for="id_dosen_1" class="ml-2 mb-3">Penguji 1</label>
                                <div class="form-group ml-2">
                                    <select class="form-control" name="id_dosen_1" id="id_dosen_1" required>
                                        <option disabled selected>- Pilih dosen  -</option>
                                        @foreach (App\Models\Dosen::all()->sortBy('id') as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama }} ( {{ $data->spesialis }} )</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="callout callout-success">
                                <label for="id_dosen_2" class="ml-2 mb-3">Penguji 2</label>
                                <div class="form-group ml-2">
                                    <select class="form-control" name="id_dosen_2" id="id_dosen_2" required>
                                        <option disabled selected>- Pilih dosen  -</option>
                                        @foreach (App\Models\Dosen::all()->sortBy('id') as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama }} ( {{ $data->spesialis }} )</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <input type="hidden" name="tanggal" value="{{$tanggal}}">
                    <input type="hidden" name="waktu" value="{{$waktu}}">
                    <input type="hidden" name="tempat" value="{{$tempat}}">
                    <div class="card-footer mt-3">
                        <a href="/user/admin/penjadwalan" class="btn btn-default"><i class="fa fa-undo"></i> &nbsp;Kembali</a>
                        {{-- <button type="submit" class="btn btn-primary float-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> &nbsp;Save</button> --}}
                        <button type="submit" class="btn btn-primary float-right" id="btn_save"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Tambah</button>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-4">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Penguji</h3>
                </div>
                <div class="card-body">
                    
                </div>
              </div>
            </div> --}}
          </form>
        {{-- </div>   --}}
        <div class="card">
            <div class="card-header border-transparent">
              <h3 class="card-title">Daftar Peserta</h3>
              <div class="card-tools">
                <form action="/user/admin/penjadwalan/print" method="POST" target="_blank">
                  @csrf
                  <input type="hidden" name="tanggal" value="{{$tanggal}}">
                  <button class="btn btn-success float-right" type="submit"><i class="fa fa-print"></i> &nbsp;Print</button>
                </form>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                <table class="table" id="tablePenjadwalan">
                    <thead>
                      <tr>
                        <th style="width:5%">No</th>
                        <th style="width:15%">NPM</th>
                        <th style="width:25%">Nama</th>
                        <th style="width:20%">Pembimbing</th>
                        <th style="width:20%">Penguji</th>
                        <th style="width:15%">Jadwal</th>
                      </tr>
                    </thead>
                    @php
                    $i = 0;
                    @endphp
                    <tbody>
                        @foreach (App\Models\PendaftaranProposal::where('tanggal', $tanggal)->get()  as $item)
                            @php
                                $penguji = App\Models\Penguji::where('id_pendaftaran', $item->id)->get();
                            @endphp
                            @if (count($penguji) > 0)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $item->pengajuan->mahasiswa->npm }}</td>
                                    <td>{{ $item->pengajuan->mahasiswa->nama ?? '' }}</td>
                                    <td>
                                      @foreach ($item->pengajuan->notifikasi->sortBy('id') as $item_pembimbing)
                                        {{ $item_pembimbing->dosen->nama ?? '' }} <br>
                                      @endforeach
                                    </td>
                                    <td>
                                      @foreach (App\Models\Penguji::where('id_pendaftaran', $item->id)->where('id_dosen','<>', $item->pengajuan->notifikasi->first()->dosen->id)->where('id_dosen','<>', $item->pengajuan->notifikasi->last()->dosen->id)->orderBy('id')->get() as $penguji)
                                          {{ $penguji->dosen->nama }} <br>
                                      @endforeach
                                    </td>
                                    <td>
                                        {{ date('d-M-Y', strtotime($item->tanggal ??'')) }} <br>
                                        {{ $item->waktu ?? '' }} <br> 
                                        {{ $item->tempat ?? '' }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>

            </div>
        </div><!--/. container-fluid -->
    
      </section>
      


    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@push('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script>
      $(function () {

        var SITEURL = "{{url('/')}}";
          $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

        $('#id_pendaftaran').on('change', function() {
          var optionSelected = $(this).find("option:selected");
          var id = optionSelected.val();
              // $("#dosen_pembimbing").text(id)
              $.ajax({
                  url: SITEURL + "/user/admin/penjadwalan/get_dosen",
                  type: "POST",
                  data: 'id=' + id,
                  success:function(result){
                    var respData = JSON.parse(result);
                    // console.log(respData.count_dosen);
                    if (respData.count_dosen == 2) {
                      $('#col_dosen_2').show();
                      $('#col_dosen_1').attr({"class" : "col-md-6"});
                    } else {
                      $('#col_dosen_2').hide();
                      $('#col_dosen_1').attr({"class" : "col-md-12"});
                    }
                    $("#dosen_pembimbing_1").text(respData.dosen_1);
                    $("#kehadiran_pembimbing_1").val(respData.id_dosen_1);
                    $("#dosen_pembimbing_2").text(respData.dosen_2);
                    $("#kehadiran_pembimbing_2").val(respData.id_dosen_2);
                    
                      // $("#dosen_pembimbing_1").text(result);
                  }
              })
              // .done(function() {
              //     alert('im here');
              // });
          });

        $('#btn_save').click(function(e){
          e.preventDefault();
          $(this).html('<i class="fa fa-spinner fa-spin"></i> &nbsp;Menambahkan...');

          $.ajax({ 
            data: $('#formJadwal').serialize(),
            url: SITEURL + "/user/admin/penjadwalan/store",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                // alert('Success WOEYY');
                location.reload();
          
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btn_save').html('<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Tambah');
            }
          });

        });

        });
    </script>
@endpush