<!DOCTYPE html>
<html lang="en">
<head>
    <title>Berita Acara | Document ITATS</title>
    <style>
        <!--  /* Font Definitions */  @font-face 	{font-family:"Cambria Math"; 	panose-1:2 4 5 3 5 4 6 3 2 4; 	mso-font-charset:0; 	mso-generic-font-family:roman; 	mso-font-pitch:variable; 	mso-font-signature:-536869121 1107305727 33554432 0 415 0;}  /* Style Definitions */  p.MsoNormal, li.MsoNormal, div.MsoNormal 	{mso-style-unhide:no; 	mso-style-qformat:yes; 	mso-style-parent:""; 	margin:0cm; 	margin-bottom:.0001pt; 	mso-pagination:widow-orphan; 	font-size:10.0pt; 	font-family:"Times New Roman","serif"; 	mso-fareast-font-family:"Times New Roman"; 	mso-ansi-language:EN-US; 	mso-fareast-language:EN-US;} p.MsoHeading9, li.MsoHeading9, div.MsoHeading9 	{mso-style-unhide:no; 	mso-style-qformat:yes; 	mso-style-link:"Heading 9 Char"; 	mso-style-next:Normal; 	margin:0cm; 	margin-bottom:.0001pt; 	text-align:center; 	mso-pagination:widow-orphan; 	page-break-after:avoid; 	mso-outline-level:9; 	font-size:20.0pt; 	mso-bidi-font-size:10.0pt; 	font-family:"Arial","sans-serif"; 	mso-fareast-font-family:"Times New Roman"; 	mso-bidi-font-family:"Times New Roman"; 	mso-ansi-language:EN-US; 	mso-fareast-language:EN-US; 	font-weight:bold; 	mso-bidi-font-weight:normal;} p.MsoBodyText2, li.MsoBodyText2, div.MsoBodyText2 	{mso-style-unhide:no; 	mso-style-link:"Body Text 2 Char"; 	margin:0cm; 	margin-bottom:.0001pt; 	text-align:center; 	mso-pagination:widow-orphan; 	font-size:12.0pt; 	mso-bidi-font-size:10.0pt; 	font-family:"Arial","sans-serif"; 	mso-fareast-font-family:"Times New Roman"; 	mso-bidi-font-family:"Times New Roman"; 	mso-ansi-language:EN-US; 	mso-fareast-language:EN-US; 	font-weight:bold; 	mso-bidi-font-weight:normal;} span.Heading9Char 	{mso-style-name:"Heading 9 Char"; 	mso-style-unhide:no; 	mso-style-locked:yes; 	mso-style-link:"Heading 9"; 	mso-ansi-font-size:20.0pt; 	font-family:"Arial","sans-serif"; 	mso-ascii-font-family:Arial; 	mso-hansi-font-family:Arial; 	mso-ansi-language:EN-US; 	mso-fareast-language:EN-US; 	font-weight:bold; 	mso-bidi-font-weight:normal;} span.BodyText2Char 	{mso-style-name:"Body Text 2 Char"; 	mso-style-unhide:no; 	mso-style-locked:yes; 	mso-style-link:"Body Text 2"; 	mso-ansi-font-size:12.0pt; 	font-family:"Arial","sans-serif"; 	mso-ascii-font-family:Arial; 	mso-hansi-font-family:Arial; 	mso-ansi-language:EN-US; 	mso-fareast-language:EN-US; 	font-weight:bold; 	mso-bidi-font-weight:normal;} .MsoChpDefault 	{mso-style-type:export-only; 	mso-default-props:yes; 	font-size:10.0pt; 	mso-ansi-font-size:10.0pt; 	mso-bidi-font-size:10.0pt;} @page WordSection1 	{size:612.0pt 792.0pt; 	margin:72.0pt 72.0pt 72.0pt 72.0pt; 	mso-header-margin:36.0pt; 	mso-footer-margin:36.0pt; 	mso-paper-source:0;} div.WordSection1 	{page:WordSection1;} 
        -->
    </style>
    <style>
        /* Style Definitions */
       
       
       @page { margin: 100px 25px; }
       header { position: fixed; top: -60px; left: 0px; right: 0px; height: 50px; }
       footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 50px; }
       
       .break { page-break-after: always; }
       .break:last-child { page-break-after: never; }
    </style>
</head>
<body>
    <header>
        <table style="margin-bottom:-10pt;">
        <tr>
            <th rowspan="5"><p class="MsoHeading9" style="text-align: center; margin-top:-50pt; margin-bottom:-30pt;color:#6b6b6b;"><span lang="EN-US"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4RD6RXhpZgAATU0AKgAAAAgABAE7AAIAAAAQAAAISodpAAQAAAABAAAI
                WpydAAEAAAAgAAAQ0uocAAcAAAgMAAAAPgAAAAAc6gAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAER6dWxrYXJuYWluIEluYwAABZADAAIAAAAUAAAQqJAEAAIAAAAUAAAQvJKRAAIAAAADMDQAAJKSAAIAAAADMDQAAOocAAcAAAgMAAAInAAAAAAc6gAAAAgAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADIwMjE6MDE6MjQgMDg6MDY6NTcAMjAyMTowMToyNCAwODowNjo1NwAAAEQAegB1AGwAawBhAHIAbgBhAGkAbgAgAEkAbgBjAAAA/+ELImh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSfvu78nIGlkPSdXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQnPz4NCjx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iPjxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+PHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9InV1aWQ6ZmFmNWJkZDUtYmEzZC0xMWRhLWFkMzEtZDMzZDc1MTgyZjFiIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iLz48cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0idXVpZDpmYWY1YmRkNS1iYTNkLTExZGEtYWQzMS1kMzNkNzUxODJmMWIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyI+PHhtcDpDcmVhdGVEYXRlPjIwMjEtMDEtMjRUMDg6MDY6NTcuMDM3PC94bXA6Q3JlYXRlRGF0ZT48L3JkZjpEZXNjcmlwdGlvbj48cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0idXVpZDpmYWY1YmRkNS1iYTNkLTExZGEtYWQzMS1kMzNkNzUxODJmMWIiIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+PGRjOmNyZWF0b3I+PHJkZjpTZXEgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj48cmRmOmxpPkR6dWxrYXJuYWluIEluYzwvcmRmOmxpPjwvcmRmOlNlcT4NCgkJCTwvZGM6Y3JlYXRvcj48L3JkZjpEZXNjcmlwdGlvbj48L3JkZjpSREY+PC94OnhtcG1ldGE+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw/eHBhY2tldCBlbmQ9J3cnPz7/2wBDAAcFBQYFBAcGBQYIBwcIChELCgkJChUPEAwRGBUaGRgVGBcbHichGx0lHRcYIi4iJSgpKywrGiAvMy8qMicqKyr/2wBDAQcICAoJChQLCxQqHBgcKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKir/wAARCACbAIEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD6RooooAKKKKACioLi+tLR40urqGBpTiNZJApc+2etczc/FLwbb3y2UGtJqN02QINLhkvWzzwRCrYPB4OOlAHW0Vx9r49ur9z9h8D+KpE27g81tBbA846TTI2fbHv0rQXxHqjKCfBmuLkdDNY5H/kzQB0FFcNH8VNLmuEgisZHmdwiRrqmmlmYnAUD7Vyc8YrRuvF+p2kbyP4F8RyInUxNZOT9FFzk/lQB1FFcT/wtbQLSIP4gtNa8P5fywdU0qeNCcZP7xVaPAwed2OCenNdRpWt6VrtsbjRNTs9RhBwZLSdZVB9CVJoAvUUUUAFFFFABRRRQAUUUUAFcz4q8YHRLq30nR7CTV9fvI2lt7GM7VSNfvTSv0SMHAzyWJCqCemh4o8QReGfD1xqUkLXMq7Y7a1Q/PczuwWOJeDyzlRnHGcngGvDfHOta14MjurSG2vku7qa2u/F/iaxhcNGkjnZFbFz9xVBjU/dGMH5i1AGxeWreJfEXh2PxPqc97qOqR+cyW9ube1t4c7diDcS5zu++W+8DgfKKsz3fjLVfhtf/APCLTWXhG60AXyXdhp1lHJHMyMfLiiP8B2qQWA+8QQB20tc8My3UEHijwrrlmmlRRPe20lxcGOO3D4ZmVsEKh2gnOMYrC+G2p31hZ6veaVpN74u1LWbgGWe2VoNPjVAVVRPcbTJ1csyoQScY+7nqquHIrO5z01Pnd0d38ILcL8MtKvjq2parLqUKXk82pXBmdZWRQ6KSMhAynA+pyc129eb+FPDXjjRfDlto+lnw/wCGdNhV/It/Ln1KeHc2/DO0kak7mbOAR6e0lxpXiHT7yC21/wCLLW819KI7OGPTrO3aR+fkUOGLdgAOfrmuU6DkrH4f+FbX9pg6dbaHax2lv4dXUIoVBwlwLpQJBz1xxXNabo+leLPjF4pudXsNJvZ311rKBrnxDJZ3USRjY5jiTmQbcYzj7pX1x6ve+HdW064ga6+KF1aXd03kWzXNnYAyueQgBiBb12g1mD4V6zBdf2i2peGNW1NJhcR3GoeGI45BIHDhvMhkRgdwJ5zkk5oA4i8+LvidPil4i0bQyZvtWs2+kafNfKxsrBwWRs7Rku7KcD2J5xXZeNbbwb4d1zw3FrcM1r4n12f7Ius6Ev2KYyttV5n2tgqXZflfeMHo2DWPq3gzWIdBm0vUPCc8UZ1ca2dW8Oaj9qkF1z+98m5w5PTIUnqSPQ8745e38R6v4q8b6ZfJf6rocNh/ZukPbSR3dkkU8bzmWB1DAAnO8ZG1mBxQB6nbeJdX8GzLbeNLqDVdG83yE8Q2yBDbPxhbxBwnJx5q/LnG4JnNd9XjeleJ9C+GngiysvG8ct/r/ip5dR1HT7a186SQz5L74ycBFXCEd9rYBwcdF4D1mz0u8tfDlndpdaHf2zXnhm7M+8yW67fMtSG+bdEWGM5OwgHlGoA9CooooAKKKKACiiigDiNWaTXviMLazSO5PhexN6tvJLtjkvpw6QB+DjbGkpzzjzlOOBWL4a+KB1XVB4L+JvhyTRNcu42j8iSEy2l+m35tp5G0jdkElcAjcTxXKeKbvVLrS5H0S3j1TVPEvjJ5YLWKVYzcWun/AC7d54HNqGz/ALXfvp33xAvvGnhW/wBI1bwpfeGNcaa3sLea9jDLG91J5BaJmCtu8tpTlRwqtyehAL3w3+H+kalo9pqNxbSv4bgnln0DSLxmkRUdji6lVx8zsCSikFUQqeXZmr1oAKoCgAAYAHao7W2hsrSG1tY1iggjWOONeiqBgAfQCsvxFeEWLWVrPIl1PhSLdh5yKVdsqOSCwjdVOOvPagDZrw7xDcyeKvGH/Es8M6bqL6v5cujXus2bzCWGMGOV0KsPKhTcJQSwYmYFVZmXHuNeD+MrTTfDvxBhji8Q3Og3azQjRLf+y5r/AGKdzuLcIwKq7lomj+YYKjAGzaAJr/hzxN4U0u80bUf7P8VT63ZHTrPVby3kF3PLISotzK0kmzaD5ijAQ7HZmUgs3tGgapFrGixXUSTRkM8MsdwAJI5I3Mbq2CRkMrDIJBxkEgg14f4kvfE+u6TLdeOtVh8OazZQO+m6LHpsn7y4GHjkt5zIySTH5U+TcybioClm3e0+FPDen+EvDFpoujwvBZ24YpHJIXZS7F2G49eWNAGxWH4l8I6V4ohia9i8m+tjus9RgAW4tHzkNG+MjkDI5B6EGn3d01j4mimlmkjs5bfy5BI2IgwDyBwTwpVUcN6hlz90Vs0AeDw6r4n8O/EjWdYl8Kt4q1mK2h0yc2jBJIWUFo5lXB2wzoQSR9143XsC21ZeDta8PfB621DUlU+I9HvZPEC29scLExdnltU2jo0TSIVGRubjIANaXxNvNX8PawdU8PO8F1qGiXlqk6xK4Fxbr9phBDZByi3QHHU0z4a+DdUmGl+Ndf8AG2s63cXtks8Vq5EVsiTRhipiyQSMjpt5UcUAek2d3Bf2MF5aSCSC4jWWJx/ErDIP5Gpq5T4azf8AFEQafv3HR7m40oZGDtt5niTPqdiJkjvXV0AFFFFABTJ5kt7eSaTOyNC7YGeAMmn1gePbh7T4b+JbiLG+HSbqRc+ohYigDyQ+PdV8G+FPDen6dBZT3UWhW16La5y9xeX14XSONAWzgOXdyOcAKCN1b+keKNS8W6x4X0nxLaR2us6Zrl2L+O1IaFzbWxIdTk/Lm5hODnn04rM8a+KvD3g3X7T+2fh7qmoSeG7aCC316K2DRRDYu0rIw2naW43dGzjBq/8ACwaNJrugnwna3Nro0Xh65mjjvmU3LPNdoDJJhjnd9nJBHGB26AA9frg9Ze3vPFN3vuH8yEx2pDx7fJjmjeMFeQHBZmbcenlOvXFd5XkGvQNNbeIWO0SNc3KpETJcibLRoflUZHPlnYOUeOM52u+QD1LRtQ/tbQ7LUPKaE3MCStG64ZCQCVI7EHirMxhRPOuNirDmTe+MJwctk9OCefTNcP4WuJLfxIbSGVhBDCizwB96lpFIjkZvYWojXpuDq/V66TxhcPaeB9duYvvw6dcSLxnkRsRQBQ8CeLI/Fmjy3P2i1llSXdi2lV9sUgEkQYA5DKrhGzjLIx6V09eJ/s7rDCfENpal/JtFtIUVv4R+9f8AM78n3Jr0zxrfz6f4bnmtLmS1mjUz+dGgcokSmVsqeoITYRx9/qDigDB8UXFvqOoXkc+6KG1c+ZI
                ynYEhh3tJ2ztNznGQD5TDOTiuz0m5N5o9pcM0jNJCrMZI/LYnHOV/hOe3avLYNPEPi2aWNREs9pblCytLvCSlQivj5kU+Udo5kZFJIWU577wPkeDrIEbQPMCr9oM+F8xtuZD944xz+p60AZ/xEhYWOh34MWyx1yzMqyjKvHM/2Vx6fduCefSvJDP4ttPBdz8SLTxbDo1hptxLHZeGIYFjtBFFO0fkMFbaWbDYwCeVwem31/4oQGb4V+I2Qqr29hJdIWXcA0Q8xePqgryXxrD8M9N+JuoXzeBda1+8sWW71iWwjZ7W3dvn3SKW2k4+ZgcL65O7AB6v4MZYfEnjKyjUqq6rHcoCe01rAxwPTeJPxz6V11cb4ZuoL74ha3f2DpJZ6jo+l3sLouPMDm6AY8ZyVRfwArsqACiiigArB8dQrcfDvxHDJnbJpV0jY64MTCt6sXxn/wAiHr//AGDbn/0U1AHlPj/wD8RPEfxAn1mO30HWtFs8HTdH1WeUwqQo+YxrtVnJDffJGGweAMdr4MGpN40vm8QWtnbaqnh7TFnis1xHETNeEomSeBwOpBxTNb+CXgHxFrd1q2r6K097dv5k0gvJl3N64VwB07Vb8MaZaaL4+1bS9NiMVpZ6HpcECFi21FkvABkkk9O9AHZ15Fr8p05tfjne1lnjvWukjl/0Zo4yy/vGZASQBJt8xQcrJskADAr67Xkvjy1Gn69qjRozLcW/27EUzloWVCvmBR80edpDMgKlcrKCrK0YBN4ejgu/E2sRzPLDJP5VtJE0gDAJEskcq4yFdWfgL8o3Bh8rQluj8Was5+GPiH7SFa8gspLecKhRSzptDqDn5SGDDk45XOQceXQ28Ul1aTJZzRmWeCa1myoSIlGVZE2jayKzIwKMEG7cphDrG/VeItbTU/h7cauEVkeOKC7ZPu3BjvFGxSQNwIE5xgcScjnAAM74PWg0bxtr1pITHH5TFN7D51Rk+ZiOM7Hi5OOuOxA7PXnj1PR7u6vZHht7+E2yLhwyWzdcJ97fJkAgAMAUAG5cNwXg6Ap4pmsNxdP7EAkCjDzDfAJAMcljHHgD+XWrHinWRrHiCOdgLiK2uLi2toYwW2SRhlLMCDy5IG0KcjavzsREwA7QL0M2nvdeTGo0eKfzp7k8F9sYfoQA2SgkDFiCyRgoUevSvBEUsPg2wW4ZmbDsGaBYdyl2KkIvCgqQQPTFeO2MsVp56mOVJbGCMSSNKVMO9TmUkEeXIwXAcjzmXiJUDr5Xuei2A0vQrKxC7TBCqMA27nHPOBnnPYfQUAUfG9s178P/ABDaxlQ0+l3Mal+gLRMOfbmvLtE0T4h2HiKNPD1rbr4c1Rf7Sa/edCwnk08RbJULZZBKqNgKfqecet+Iv+RX1X/rzm/9ANeU6P8AArwf4u8L6DretxXwvrnR7ESiO42KNltGg+XHHCigDr/h94cPhS4GjyyJNcWeg6dBLKmcOyyXROPbLNjvXc1yPhDw/Z+FdZudE0szNaWelWaRefJvYAzXZwT7ZwPQYFddQAUUUUAFYHjyb7N8OPEk5XcItJun25xnELHFb9Yfje0fUPh94hs4s77jS7mJcDJy0TAcfjQBuVytjJGnxe1yEt+8l0TT5AvqFnvAT+o/OvPfGvjvxf4b1XR9W0/xDosGkaxpTXqWmuQ7EhaOOMuqtEN7u3mAheeQcelbHgXxVc+LfGGg+IL20bTTquhXkQtmLbZGhuYSsiFgCyssu4ccDd160AeqVw/xDsZVksdWSITQW58ufMcf7oFlIdXLK6tkbQVOV3bhgqVfuKqapptvq2my2d3FFKjjKiWMOFYcq2D3BAI9xQB8x67Msdrvu45me0YFmdYklmeOQFvMG6NssG3hjGZAhzl97GrT65dXUemaFCjXOkTW1xd3bx25RiySXrJtYj5F+QEHAyAeo4o+JGmzaLqT6RdwSyfJE8k8cIKHL7VeOM4ALAuGJcKQADyvKeEJ762t4NN8R6fBqGgiLfENN1DyZI3d1hAMscgyi/anDjggzksNuwkAl8TazfeH7f8AtPSoIWuEaKF5JIPMzE9vPuQ/3QdmcjB4POKqavNb3nja+iiDyvBd3RjBiVSWkuCqq++SMnC5OFJDqSpIXGOi1S7tbTTJJ/CnhPU7fVsbkOp6vNdQhRK1q6vF5rFiQ8qAAZ+ZiMEV54r3Vhr9xdaoUubybe9xNZSRfxFA7rKhcJN879RtBJKsc4oA9X8J2k+vahawwxv5Uly1wxeOJlghDLloR8qCNyp5jQ+YJtzs5Uh/bq5LwR4WGkW/2+8ggF1MgEQ+zBJLeMgZTd15wCRx0GeldbQBg+Orl7P4d+I7qMgPDpV1IpIyARExHH4Vd8Pwvb+GdLhmRkkjs4kZW6qQgBBrE+KVwYPhbr6KVVru0NkhcEjdORCOnPWQV5x4y+KfjT+3tV0PwtDo+kPY3kdnGt+XN5cb5FjSWGJlAZCzdQGGBnOMEgHq1pKp+ImrwjO9NKsWPHYy3YH/AKCa3a4nwgL4eONaTVrtby9tdI0u0up1TYHmUXDuwAAGCZQePXHGK7agAooooAKbJGksTRyKGR1Ksp7g06igD5v1C40ey0j4dX174Uv/ABhrX9myaXZ6X54+zxyWh2Sv5YVtzZ4ycjauTyoNdJpvjbV9d8daNceLPDqaBfaTqqWqxNNubyL23nVAT3PmQxjjr6DFR+I7Oyg07xMmoeK5PDd34Y8Qvf2WpxQtK0Md7GJCmwHLBmmlXj+76Zzy3gGXWfEHh3xbrGjWd7rTF4L7+3NSdEmuryyeKaKAW6sxUbCQCC2cjkZwAD6Zoqvp9/b6pplrqFjIJba7hSeFx/EjAMp/IirFAHiv7QWh69e6WNUF9bHw9p6RSNZtgTfaN7LvTCZYbXX5d+OD8tYo8QfadCWOe+0Z2+ypbRx2WswtFbM6eTHtwcfKZJJ5DjcD5XOIwa9n8V+DdH8aWVta69C8sVtcLcJ5blDkZBXI5wQSDj8MEA18/PbeNfhpqGsa7oNreaXokF+1uYr5ldZEyNhZM/MMFQHX1I3feoA6CXxBaT7gkenB2mvJyw1eNCFS9lwikH5WeG7uFQjk7gy/dBHP2lh4i8YfFzz/AA7rej/2vaWizLqVvdRSROAojZ/3aMvmtksU2/LuODjBN7UPFfxT8Y3o8KyQHTrm4hM7wRxfZJJIR3LMc7cjHy4zyDkZFdn8I/hxpKaHoniXUdKvbTWrczNi6cgPuJVX8s9AF+7wDySc/KaAPXQMKAST7nvRRRQByXj93lj8PacGRI77XLbzWcZASDddn/0mxn3rx+28deDvFesah4F+I1zFrx/tN00bXLJGY4lfKorgblZWIGRlCAByq5btvirqCjVLy4/s19Zh8O6DcTzWUe7BkuyIEL7edoiW4JxghcnPNYPw/wDHnia9v9N0nSfCnhm10Awx31xPp1wvl21uXILkbhh8o3BG4EHI4NAHofgCGM3/AIquod5ifWPs0Rd9xK29vDAT1PO+N855+ldlXL/DiAp4DsLx4xHLqpk1SRe4a5kafBPcgSBf+A11FABRRRQAUUUUAcH4tt4tM8a2WoXVhb3+na5brpdxDcgeV9qjcy2ZfIOAXMibsHDNH7V4/qWreN9A8Xf2xrN0NK1bXLuG8tfCGjEyzXjxjYhnIJ8uMhQGwTvw3ygqSv0V4h0O08S+H7zR9RDG3u49hZThkbOVdT2ZWAYHsQK5DwTZ6VD411ZtYsVj8cCGNby8lUA39ugEaXMAyQqMFXeq/df5WzhSQCLStSf4ZxwabrVpPb+Gbg+ZaXhIkXSWf5jazsvSNWJCSdACFJ+XcfQLS7tr+ziu7G4iubaZQ8U0Lh0dT0IYcEe4ryTx9p17qfxz8PWXg3V7rRtck0+W41K9TdNGlqhIiV4SwRlMhYY4wWzycVhaLNoutfE06BYWLaHfzQvBPr/hu8ltIZ763VJJ0WDlHVd4GZAc++RQB6x408R3vh5vDyafHA76rrdvp8nnqxCxuHZyMEYbahwTkZ6iuUvfEl7eWXjOD4gaDoV3pXhtXkykjt5z+SksShZEwG2SY3BshiABzmuZ0/xqmp6Te+IT46tLi10C92RN4k0FTJFI/wAiOjW0iEhhvwdpOC2Rxxe1U61Lbx2+sT+A7qw8U3cF5DG15cwHVJV8jYQriQbSI4QUHBBGT8xyAa9x4tubvQvDnjXS/C+kz6nfzJpc0l5cFJLOVp/ICI4jYlBI0m4jHA/iq03xB1yz+LkXhzVrG1s9HkaO3S7MEzCad4PMCJMPkDbsgIyqSqls9FNP/hFfE2naK9jpvhzwrp1hFejVI0n1i4aG2nVxIGVUgTCBlzszgc9uK45Nc07VNdvPF194h8I2Or6farNPf2mh31w6pvEKyoHkVZR823zAjbc4zjmgD6FrmPEnjrT9EmbTdPRtZ8QOALfR7Jg0zMSADIRkQx/MCZHwAMnk8HzfxVeRWOoLp3ibxH4g8S3Fzod1qNrbQzpp1jdoqO5jb7PtkY7FOAcjHrk45bStfvtc8Paj4d0GPw/Fpup+GLnUJ7Tw+rxyWMqr+7WaQnMkjEBGXqRnIxywBP4n1PxDYa9LpGmeKPsfiW48jWZWs4So1m7leOKC2tywAa2iQBcsxB+YlTtfavhrT0K+ItM021TRr/xfrMmhT6XaFXt447cs13cRMwB2CKRkCjgOTjgqqc/pWhTav4ZS6lsLvT/ASILy11i9uEjn0C5wDJ9mJYySwGT5QmPnOMYcbz7d8NPCC2EMOtXNlcWKpai00jTbkBXsLXOWaRRx58zDzJDyQcLng5APQIoo4IUihRY441CoijAUDgACnUUUAFFFFABRRRQAVi+I/Cth4lige4Mtrf2bF7LUbVtk9q5GCUb0I4KnKsOCDW1RQB5XqNprvhy41W/1bSZrzUL2yWz/AOEr0GDzZ0TOAXsy2VYE53R7wcAkADA4Hwno/hnQde8M6l4M8dw6oujQ3gudN1O6FrI80sGAYIJFQgM/JDN2T5uM19JVR1PRNK1uHydZ0yz1CLGNl3bpKvUHowPcA/gKAPmTVvh/4h0TQ54l0mSXTo/DlvcSx28CzebqJje12ZQHLJ9plf5c/cUk96v/ABPsfEV7qmqTaVoGo3Fj4U0q2tLLUQ6oltLG8c0s4VsFyAnlkpkALn0r2uX4T+BJZkmTwvYW8sedkloht2GfeMqaktfht4esYxHZNrNvGG3bItfvlXPrgTY7UAac1za+IfBEl0tnNeWmo6cZBar8sk0ckednJGGIOOSOTXguh+GdX1uSXw1oX9uT6Fe6Jc280XiXTfLl0mRR+4jWdkG4eYEwiHAXJxkfL7hJ4G0mUgvda7kdNviG/H8pqzf+FReCHCi50U3gVw6i9vJ7kA/SR2/+vQB5hYy3z+JPAcnjqzs/C9r4T064tr241LVYM3pMIhCxoGy2duT25YZJA3VPCWhaO+n+HYfDq6r4m1/w5PKbbUtGhNnbmMsX8ie4mG1oyXf7oYlWYAEE59vsPAfhHS5/O07wxo9tMOkkVjGrD8cZ7Vv0AcF4U+HK2NrpjeIltJF0vD6dpFmhFnp7c/ONxLTS5JPmvzkkgAkk97RRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH//Z" style=" width: 110px;"></span></p>
            </th>
            <th>
                <p align="center" class="MsoNormal" style="text-align:center; color:#6b6b6b;"><strong><span lang="EN-US" style="font-size: 25px; font-family: Arial, sans-serif; margin-left:15px;">INSTITUT TEKNOLOGI ADHI TAMA SURABAYA</span></strong></p>
            </th>
        </tr>
        <tr>
            <td><p align="center" class="MsoNormal" style="text-align:center; color:#6b6b6b;"><strong><span lang="EN-US" style="font-size: 25px; font-family: Arial, sans-serif; margin-left:15px;">( ITATS )</span></strong></p></td>
        </tr>
        <tr style="margin-bottom:-70pt;">
            <td><p class="MsoBodyText2" style="text-align: center; color:#6b6b6b;"><span lang="EN-US" style="font-size: 14px; margin-left:15px;">Jl. Arief Rachman Hakim No. 100 Surabaya &ndash; 60117 Telp.(031) 5945043 Fax.(031) 5994620</span></p></td>
        </tr>
        <div style='margin-top:0cm;margin-right:0cm;margin-bottom:1.5pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri","sans-serif";border:none;border-bottom:double windowtext 2.5pt;padding:0cm 0cm 1.0pt 0cm;'>
            <p style='margin-top:0cm;margin-right:0cm;margin-bottom:1.5pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri","sans-serif";border:none;padding:0cm;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
        </div>
    </table></header>
    <footer>
        <p style='margin:0cm;margin-left:1cm;margin-right:0.3cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;color:#6b6b6b;position: static'><span style="font-size:12px;">Teknik Sipil, Teknik Mesin, Teknik Elektro, Teknik Arsitektur, Teknik Perkapalan, Teknik Informatika, Teknik Industri, Teknik Kimia, Teknik Lingkungan,&nbsp;</span><span style="font-size:12px;">Teknik Pertambangan, Teknik Geologi, Teknik Fisika, Planologi, Desain Produk Industri, Desain Komunikasi Visual, Sistem Informasi, Sistem Komputer, Magister Teknik &amp; Manajemen Industri, Magister Manajemen Lingkungan</span></p>
    </footer>
    
    
    <div class="break">
        {{-- Jarak header --}}
        <p style='margin:0cm;margin-bottom:40pt;font-size:13px;font-family:"Times New Roman","serif";'>&nbsp;</p>

        {{-- Konten --}}
        <p style='margin: 0cm 0cm 0.0001pt; font-size: 13px; font-family: "Times New Roman", serif; text-align: center; line-height: 18pt;'><strong><u><span style="font-size:21px;">BERITA ACARA SIDANG PROPOSAL SKRIPSI SARJANA S-1 ITATS</span></u></strong></p>
        <p style='margin:0cm;margin-bottom:.0001pt;text-align:center;line-height:20.0pt;font-size:20px;font-family:"Times New Roman","serif";'><span style="font-size:16px;">No.3/BA-UJI PROPOSAL/ITATS/S1/</span><span style="font-size:16px;">X</span><span style="font-size:16px;">/{{date('Y')}}</span></p>
        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">Nama Mahasiswa &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp;&nbsp;</span><span style="font-size:16px;">{{ $data->pengajuan->mahasiswa->nama ?? '--' }}</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";margin-left:192.05pt;text-align:justify;text-indent:-163.05pt;line-height:20.0pt;'><span style="font-size:16px;">A l a m a t &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp; --</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">Tempat / Tgl. Lahir &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp; -- &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">Nomor Pokok Mahasiswa &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp;&nbsp;</span><span style="font-size:16px;">{{ $data->pengajuan->mahasiswa->npm ?? '--' }}</span></p>
        
        <p class="MsoBodyTextIndent" style="margin-left:192.05pt; margin-right:0.5cm; margin-top:0pt;text-indent:-163.05pt;line-height:1.8em;
        tab-stops:36.0pt 72.0pt 108.0pt 130.5pt 144.0pt 163.05pt 216.0pt 252.0pt 288.0pt 324.0pt 360.0pt 396.0pt 432.0pt 450.0pt 468.0pt"><span lang="PT-BR" style="mso-ansi-language:PT-BR">Judul Proposal Skripsi<span style="mso-tab-count:2;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>:<span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp; </span>
        {{ $data->pengajuan->judul_proposal ?? '--' }}
        <o:p></o:p></span></p>
        <hr style="border: 0.2px solid gray; margin-left:1cm; margin-right:0.5cm; margin-bottom:15pt;">
        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; Sidang dimulai &nbsp;: &nbsp; &nbsp;Jam &nbsp; &nbsp; {{ $data->waktu ?? '' }} &nbsp; &nbsp; Wib. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-right:0.5cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:2em;'><span style="font-size:16px;">Pada hari <strong>{{ $hari }}</strong> tanggal&nbsp;</span><strong><span style="font-size:16px;">{{ $tanggal }}</span></strong><span style="font-size:16px;">&nbsp;dilaksanakan Sidang Proposal Skripsi Sarjana S-1 ITATS Jurusan Teknik Informatika.</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><strong><span style="font-size:16px;">I. &nbsp; &nbsp;<u>PESERTA SIDANG</u></span></strong></p>
        <p style='margin:0cm;margin-left:1.05cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; 1. &nbsp; Ketua Sidang / Penguji 1 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp;&nbsp;</span><span style="font-size:16px;">{{ $penguji_1 }} &nbsp;</span></p>
        <p style='margin:0cm;margin-left:1.05cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; 2. &nbsp; Penguji 2 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp;&nbsp;</span><span style="font-size:16px;">{{ $penguji_2 }}</span></p>
        <p style='margin:0cm;margin-left:1.05cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; 3. &nbsp; Penguji 3 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp; {{ $pembimbing }}</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><strong><span style="font-size:16px;">II. &nbsp;<u>KETENTUAN SIDANG</u></span></strong></p>
        <ul style="list-style-type: disc;margin-left:38px; margin-right:0.5cm;margin-top:0pt;">
            <li style="line-height:1.8em;"><span style="font-size:16px;">Banyaknya penguji &nbsp; : &nbsp;3 orang.</span></li>
            <li style="line-height:1.8em;"><span style="font-size:16px;">Waktu yang diberikan kepada penguji untuk memberikan pertanyaan - pertanyaan &nbsp; &nbsp; masing-masing 10 menit.</span></li>
            <li style="line-height:1.8em;"><span style="font-size:16px;">Bahwa penguji wajib menilai jawaban panelis dan pertanyaan-pertanyaan yang telah diajukan penguji maupun para penguji lainnya.</span></li>
            <li style="line-height:1.8em;"><span style="font-size:16px;">Hasil penilaian adalah dari &nbsp;angka 0 s/d 100 dan dinyatakan lulus bilamana mendapat nilai terendah : 56,0 ( lima puluh enam koma nol ).</span></li>
        </ul>
    </div>
    
    {{-- Halaman Selanjutnya --}}
    <div class="break">
        {{-- Jarak header --}}
        <p style='margin:0cm;margin-bottom:40pt;font-size:13px;font-family:"Times New Roman","serif";'>&nbsp;</p>

        {{-- Konten --}}
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><strong><span style="font-size:16px;">III.<u>&nbsp;JALANNYA SIDANG PROPOSAL</u></span></strong></p>
        <p style='margin:0cm;margin-left:1cm;margin-right:0.5cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";margin-left:48.0pt;text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">Sidang dijelaskan oleh ketua sidang tentang ketentuan-ketentuan sidang, maka para penguji secara bergantian diberikan kesempatan mengajukan pertanyaan-pertanyaan.</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:-14pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><strong><span style="font-size:16px;">IV.&nbsp;<u>HASIL SIDANG PROPOSAL</u></span></strong></p>
        <ul style="list-style-type: undefined;">
            <table style="list-style-type: undefined; margin-left:1.55cm; margin-right:1.2cm;line-height:22pt; width:100%;">
                <tr>
                    <td style="width:63%;"><li> Penguji &nbsp;1&nbsp; {{ $penguji_1 }}</li></td>
                    <td style="width:17%;">Nilai</td>
                    <td style="width:20%;">: {{ $nilai_penguji_1 }}</td>
                </tr>
                <tr>
                    <td style=""><li> Penguji &nbsp;2&nbsp; {{ $penguji_2 }}</li></td>
                    <td style="">Nilai</td>
                    <td style="">: {{ $nilai_penguji_2 }}</td>
                </tr>
                <tr>
                    <td style=""><li> Penguji &nbsp;3&nbsp; {{ $pembimbing }}</li></td>
                    <td style="">Nilai</td>
                    <td style=""> : {{ $nilai_pembimbing }}</td>
                </tr>
            </table>
        </ul>

        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:right;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><strong><span style="font-size:16px;">V.&nbsp;&nbsp;<u>KESIMPULAN DAN PENUTUP</u></span></strong></p>
        <p style='margin:0cm;margin-left:1cm;margin-right:0.5cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";margin-left:48.0pt;text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">Atas dasar evaluasi dari jumlah nilai rata-rata dibagi dengan jumlah penguji, maka mahasiswa yang bersangkutan dinyatakan &nbsp;:</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;line-height:20.0pt;'><strong><span style='font-size:17px;font-family:"Arial Black","sans-serif";'>
        @if ((($nilai_penguji_1 + $nilai_penguji_2 + $nilai_pembimbing) / 3) >= 56.0)    
            LULUS / <s>TIDAK LULUS</s>
        @else
            <s>LULUS</s> / TIDAK LULUS
        @endif
        </span></strong></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><strong><span style="font-size:16px;">VI.&nbsp;<u>SIDANG PROPOSAL DITUTUP PADA JAM &nbsp; :</u> {{ date('H:i', strtotime('+1 hour',strtotime($data->waktu))) }}</span></strong></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>
        {{-- <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p> --}}
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Surabaya,&nbsp;</span><span style="font-size:16px;">{{date('d F Y')}}</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Ketua Sidang,</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><strong><u><span style="font-size:16px;">{{ $penguji_1 }} &nbsp;</span></u></strong></p>    
    </div>

    {{-- <div class="break"> --}}
        {{-- ///// Jarak header --}}
        {{-- <p style='margin:0cm;margin-bottom:40pt;font-size:13px;font-family:"Times New Roman","serif";'>&nbsp;</p> --}}

        {{-- ///// Konten --}}
        {{-- <p>Lorem ipsum ... </p> --}}
        {{-- <p style='margin: 0cm 0cm 0.0001pt; font-size: 13px; font-family: "Times New Roman", serif; line-height: 20pt; text-align: center;'><strong><span style="font-size:24px;">PENILAIAN SIDANG PROPOSAL SKRIPSI</span></strong></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">N a m a&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;:&nbsp; &nbsp; &nbsp;</span><span style="font-size:16px;">Charles Blanov Hasudungan Pakpahan</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">Nomor Pokok Mahasiswa&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:&nbsp; &nbsp; &nbsp;06.2016.1.06649</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">Tanggal Sidang&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:&nbsp; &nbsp; &nbsp;8 Oktober 2020</span><span style="font-size:16px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp;</span></p>
        <table style="width: 4.8e+2pt;border: none;margin-left:1cm;border-collapse:collapse;">
            <tbody>
                <tr>
                    <td rowspan="2" style="width:49.5pt;border-top:double windowtext 2.25pt;border-left:solid windowtext 1.5pt;border-bottom:none;border-right:solid windowtext 1.5pt;padding:0cm 5.4pt 0cm 5.4pt;height:1.0cm;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'><strong><span style="font-size:19px;">No.</span></strong></p>
                    </td>
                    <td rowspan="2" style="width:180.0pt;border-top:double windowtext 2.25pt;border-left:none;border-bottom:none;border-right:solid windowtext 1.5pt;padding:0cm 5.4pt 0cm 5.4pt;height:1.0cm;">
                        <h1 style='margin:0cm;margin-bottom:.0001pt;text-align:center;line-height:normal;font-size:16px;font-family:"Times New Roman","serif";'><span style="font-size:19px;">MATERI YANG DIUJI</span></h1>
                    </td>
                    <td colspan="3" style="width: 252.45pt;border-top: 2.25pt double windowtext;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:6.0pt;font-size:13px;font-family:"Times New Roman","serif";margin-top:6.0pt;margin-right:0cm;margin-left:0cm;text-align:center;'><strong><span style="font-size:19px;">N I L A I</span></strong></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 105.15pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 29.75pt;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:6.0pt;font-size:13px;font-family:"Times New Roman","serif";margin-top:6.0pt;margin-right:0cm;margin-left:0cm;text-align:center;'><strong><span style="font-size:19px;">Rata-rata</span></strong></p>
                    </td>
                    <td colspan="2" style="width: 147.3pt;border-top: none;border-bottom: 1.5pt solid windowtext;border-left: none;border-image: initial;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 29.75pt;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:6.0pt;font-size:13px;font-family:"Times New Roman","serif";margin-top:6.0pt;margin-right:0cm;margin-left:0cm;text-align:center;'><strong><span style="font-size:19px;">Tertimbang</span></strong></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49.5pt;border-top: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;border-left: 1.5pt solid windowtext;border-image: initial;border-bottom: none;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'><span style="font-size:16px;">1.</span></p>
                    </td>
                    <td style="width: 180pt;border-top: 1.5pt solid windowtext;border-left: none;border-bottom: none;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;Perumusan Masalah Penelitian</span></p>
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;Latar Belakang</span> <span style="font-size:16px;line-height:150%;">Penelitian</span></p>
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;Kebaruan Penelitian&nbsp;</span></p>
                    </td>
                    <td style="width: 105.15pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;</span></p>
                    </td>
                    <td style="width: 55.2pt;border-top: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;border-bottom: 1.5pt solid windowtext;border-image: initial;border-left: none;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;line-height:150%;'><span style="font-size:16px;line-height:  150%;">30 &nbsp;%</span></p>
                    </td>
                    <td style="width: 56.1pt;border-top: none;border-bottom: none;border-left: none;border-image: initial;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49.5pt;border-top: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;border-left: 1.5pt solid windowtext;border-image: initial;border-bottom: none;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'><span style="font-size:16px;">2.</span></p>
                    </td>
                    <td style="width: 180pt;border-top: 1.5pt solid windowtext;border-left: none;border-bottom: none;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">Relevansi, Kebaruan</span> <span style="font-size:16px;line-height:150%;">Pustaka, dan Penyusunan Pustaka</span></p>
                    </td>
                    <td style="width: 105.15pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;</span></p>
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'><span style="font-size:16px;">&nbsp;</span></p>
                    </td>
                    <td style="width: 55.2pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;line-height:150%;'><span style="font-size:16px;line-height:  150%;">10 &nbsp;%</span></p>
                    </td>
                    <td style="width: 56.1pt;border-top: 1.5pt solid windowtext;border-left: none;border-bottom: none;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49.5pt;border: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'><span style="font-size:16px;">3.</span></p>
                    </td>
                    <td style="width: 180pt;border-top: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;border-bottom: 1.5pt solid windowtext;border-image: initial;border-left: none;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">Perancangan Sistem dan Metode Yang Digunakan&nbsp;</span></p>
                    </td>
                    <td style="width: 105.15pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;</span></p>
                    </td>
                    <td style="width: 55.2pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;line-height:150%;'><span style="font-size:16px;line-height:  150%;">30 &nbsp;%</span></p>
                    </td>
                    <td style="width: 56.1pt;border-top: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;border-bottom: 1.5pt solid windowtext;border-image: initial;border-left: none;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49.5pt;border-right: 1.5pt solid windowtext;border-bottom: 1.5pt solid windowtext;border-left: 1.5pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'><span style="font-size:16px;">4.</span></p>
                    </td>
                    <td style="width: 180pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">Penyampaian Materi dan Tanya Jawab&nbsp;</span></p>
                    </td>
                    <td style="width: 105.15pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;</span></p>
                    </td>
                    <td style="width: 55.2pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;line-height:150%;'><span style="font-size:16px;line-height:  150%;">30 &nbsp;%</span></p>
                    </td>
                    <td style="width: 56.1pt;border-top: none;border-left: none;border-bottom: 1.5pt solid windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:150%;'><span style="font-size:16px;line-height:  150%;">&nbsp;</span></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 229.5pt;border-top: none;border-bottom: none;border-left: none;border-image: initial;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;'>&nbsp;</p>
                    </td>
                    <td style="width: 105.15pt;border-top: none;border-right: none;border-left: none;border-image: initial;border-bottom: 2.25pt double windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:6.0pt;font-size:13px;font-family:"Times New Roman","serif";margin-top:6.0pt;margin-right:0cm;margin-left:0cm;text-align:center;'><strong><span style="font-size:19px;">Nilai Akhir &nbsp; &nbsp;:</span></strong></p>
                    </td>
                    <td style="width: 55.2pt;border-top: none;border-left: 1.5pt solid windowtext;border-bottom: 2.25pt double windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;'>&nbsp;</p>
                    </td>
                    <td style="width: 56.1pt;border-top: none;border-left: none;border-bottom: 2.25pt double windowtext;border-right: 1.5pt solid windowtext;padding: 0cm 5.4pt;height: 1cm;vertical-align: top;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;'>&nbsp;</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";line-height:20.0pt;'><strong>&nbsp;</strong></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";line-height:20.0pt;'><strong>RANGE NILAI</strong></p>
        <table style="margin-left:1cm;border-collapse:collapse;border:none;line-height:15pt;">
            <tbody>
                <tr>
                    <td style="width:83.55pt;border:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:11.5pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>NILAI HURUF</p>
                    </td>
                    <td style="width:107.2pt;border:solid windowtext 1.0pt;border-left:  none;padding:0cm 5.4pt 0cm 5.4pt;height:11.5pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>NILAI ANGKA</p>
                    </td>
                </tr>
                <tr>
                    <td style="width:83.55pt;border:solid windowtext 1.0pt;border-top:  none;padding:0cm 5.4pt 0cm 5.4pt;height:11.5pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>A</p>
                    </td>
                    <td style="width:107.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:11.5pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>81 - 100</p>
                    </td>
                </tr>
                <tr>
                    <td style="width:83.55pt;border:solid windowtext 1.0pt;border-top:  none;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>B+</p>
                    </td>
                    <td style="width:107.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>71 - 80</p>
                    </td>
                </tr>
                <tr>
                    <td style="width:83.55pt;border:solid windowtext 1.0pt;border-top:  none;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>B</p>
                    </td>
                    <td style="width:107.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>66- 70</p>
                    </td>
                </tr>
                <tr>
                    <td style="width:83.55pt;border:solid windowtext 1.0pt;border-top:  none;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>C+</p>
                    </td>
                    <td style="width:107.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>61 - 65</p>
                    </td>
                </tr>
                <tr>
                    <td style="width:83.55pt;border:solid windowtext 1.0pt;border-top:  none;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>C</p>
                    </td>
                    <td style="width:107.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>56 - 60</p>
                    </td>
                </tr>
                <tr>
                    <td style="width:83.55pt;border:solid windowtext 1.0pt;border-top:  none;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>D</p>
                    </td>
                    <td style="width:107.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>41 - 55</p>
                    </td>
                </tr>
                <tr>
                    <td style="width:83.55pt;border:solid windowtext 1.0pt;border-top:  none;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>E</p>
                    </td>
                    <td style="width:107.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;">
                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:center;'>0 - 40</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><strong><u><span style="font-size:16px;">Catatan &nbsp;:</span></u></strong></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">1.&nbsp; &nbsp;Perlu perbaikan &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Dosen Penguji,</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">2.&nbsp; &nbsp;Tidak Perlu Perbaikan</span></p>
        <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp;(&nbsp;</span>Lingkari yang dimaksud<span style="font-size:16px;">&nbsp;)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<u>____________</u></span></p> --}}
        
        {{-- <p style='margin:0cm;margin-left:1cm;margin-bottom:.0001pt;font-size:13px;font-family:"Times New Roman","serif";text-align:justify;line-height:20.0pt;'><span style="font-size:16px;">&nbsp; &nbsp; &nbsp;(&nbsp;</span>Lingkari yang dimaksud<span style="font-size:16px;">&nbsp;) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Dosen 2 S.Kom.M.Kom<u></u></span></p> --}}
    {{-- </div> --}}

    {{-- <div class="break"> --}}
        {{-- ///// Jarak header --}}
        {{-- <p style='margin:0cm;margin-bottom:40pt;font-size:13px;font-family:"Times New Roman","serif";'>&nbsp;</p> --}}

        {{-- ///// Konten --}}
        {{-- <p>Lorem ipsum ... </p>
    </div> --}}
    
</body>
</html>


