@extends('layout.app')

@push('css')
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/bootstrap-clockpicker.min.css')}}">
  {{-- date range --}}
  {{-- <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /> --}}

@endpush

@push('js')

  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

  {{-- Date range --}}
  {{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script> --}}

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Penjadwalan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
          <div class="card">
            <div class="card-body">
              <button class="btn btn-success float-left" type="submit" id="downloadJadwal" target="_BLANK"><i class="fa fa-download"></i> &nbsp;Download</button>
              {{-- <a href="/user/admin/file/berita_acara" class="btn btn-info ml-2" target="_BLANK">Cek</a> --}}
              <div class="card-tools">
                <a href="" class="btn btn-primary float-right" type="button" data-toggle="modal" data-target="#modal-tambah-jadwal"><i class="fa fa-plus"></i> &nbsp;Tambah jadwal</a>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header border-transparent">
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                <table class="table" id="tablePenjadwalan">
                    <thead>
                    <tr>
                      <th style="width:2%"><input name="select_all" value="1" id="select_all" type="checkbox" /></th>
                      <th style="width:5%">No</th>
                      <th style="width:13%">NPM</th>
                      <th style="width:20%">Nama</th>
                      <th style="width:20%">Pembimbing</th>
                      <th style="width:20%">Penguji</th>
                      <th style="width:15%">Jadwal</th>
                      <th style="width:5%">Aksi</th>
                    </tr>
                    </thead>
                    @php
                    $i = 0;
                    @endphp
                    <tbody>
                      @foreach (App\Models\PendaftaranProposal::orderBy('id', 'desc')->get() as $item)
                        @php
                            $penguji = App\Models\Penguji::where('id_pendaftaran', $item->id)->get();
                        @endphp
                        @if (count($penguji) > 0)
                          <tr>
                            <td><input type="checkbox" name="id[]" value="{{ $item->id }}"></td>
                            <td>{{ ++$i }}</td>
                            <td>{{ $item->pengajuan->mahasiswa->npm }}</td>
                            <td>{{ $item->pengajuan->mahasiswa->nama ?? '' }}</td>
                            <td>
                              @foreach ($item->pengajuan->notifikasi->sortBy('id') as $item_pembimbing)
                                {{ $item_pembimbing->dosen->nama ?? '' }} <br>
                              @endforeach
                            </td>
                            <td>
                              @foreach (App\Models\Penguji::where('id_pendaftaran', $item->id)->where('id_dosen','<>', $item->pengajuan->notifikasi->first()->dosen->id)->where('id_dosen','<>', $item->pengajuan->notifikasi->last()->dosen->id)->orderBy('id')->get() as $penguji)
                                {{ $penguji->dosen->nama }} <br>
                              @endforeach
                            </td>
                            <td>
                                {{ date('d-M-Y', strtotime($item->tanggal ??'')) }} <br>
                                {{ $item->waktu ?? '' }} <br> 
                                {{ $item->tempat ?? '' }}
                            </td>
                            <td>
                              <form action="/user/admin/file/berita_acara" target="_BLANK" method="POST">
                                @csrf
                                <input type="hidden" name="pendaftaran_id" value="{{ $item->id }}">
                                <button type="submit" class="btn btn-info"><i class="fa fa-file-text" aria-hidden="true"></i></button>
                              </form>
                            </td>
                          </tr>
                          <input type="hidden" name="id_pend" id="id_pend" value="{{ $item->pengajuan->mahasiswa->nama }}">
                        @endif
                      @endforeach
                    </tbody>
                </table>
                </div>
            </div>

          </div> 
        </div><!--/. container-fluid -->

        <div class="modal fade" id="modal-tambah-jadwal">
          <div class="modal-dialog">
            <div class="modal-content">
              <form role="form" action="/user/admin/penjadwalan/tambah" method="POST">
                @csrf
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Jadwal</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body"> 
                  <div class="form-group">
                    <label>Tanggal</label>
                    <input class="form-control" id="tanggal" name="tanggal" placeholder="MM-DD-YYY" type="date" required/>
                  </div>
                  <div class="form-group clockpicker" data-autoclose="true">
                    <label>Waktu</label>
                    <input type="text" class="form-control" name="waktu" value="09:00">
                  </div>
                  <div class="form-group">
                    <label>Tempat</label>
                    <input type="text" class="form-control" name="tempat" id="tempat" placeholder="Masukkan Tempat" required>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> &nbsp;Tambah</button>
                </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@push('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    
    <script>
        $(function () {

          var SITEURL = "{{url('/')}}";
          $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

          var table = $("#tablePenjadwalan").DataTable({
            "responsive": true,
            "autoWidth": false,
            'columnDefs': [{
              'targets': 0,
              'searchable':false,
              'orderable':false,
              'className': 'dt-body-center',
            }],
            'order': [1, 'asc']
          });

          $('#select_all').on('click', function(){
              var rows = table.rows({ 'search': 'applied' }).nodes();
              $('input[type="checkbox"]', rows).prop('checked', this.checked);
          });

          $('#tablePenjadwalan tbody').on('change', 'input[type="checkbox"]', function(){
              if(!this.checked){
                var el = $('#select_all').get(0);
                if(el && el.checked && ('indeterminate' in el)){
                    el.indeterminate = true;
                }
              }
          });

          $('#downloadJadwal').click(function(e){
            e.preventDefault();
            var id = []; 
            $('input[name="id[]"]:checked').each(function(i){
              id[i] = $(this).val();
            });

            if( id.length < 1){
              alert('Select Checkbox');
            } else {
              $(this).html('<i class="fa fa-spinner fa-spin"></i> &nbsp;Downloading...');

              $.ajax({
                data: 'id=' + id,
                url: SITEURL + "/user/admin/penjadwalan/full_print",
                type: "POST",
                xhrFields: {
                  responseType: 'blob'
                }, 
                success: function (data) {
                  var blob = new Blob([data]);
                  var link = document.createElement('a');
                  link.href = window.URL.createObjectURL(blob);
                  link.download = "Jadwal_Sidang_Proposal_"+Date.now()+".pdf";
                  link.target = "_blank";
                  link.click();
                  $('#downloadJadwal').html('<i class="fa fa-download"></i> &nbsp;Download');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
              });
            }
            
          });
          
        });

    </script>
    <script type="text/javascript" src="{{asset('assets/dist/js/bootstrap-clockpicker.min.js')}}"></script>
    <script type="text/javascript">
        $('.clockpicker').clockpicker();
    </script>

@endpush