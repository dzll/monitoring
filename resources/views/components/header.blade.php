<!-- Navbar -->
<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      {{-- <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> --}}
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-bell"></i>
          {{-- @foreach ($jadwal as $jadwal)
            @php
              $status = $jadwal->status;
              dd($status);
             @endphp
          @endforeach --}}
          <!-- NOTIFIKASI BEL -->
          @if (Auth::user()->role === 2)
            @php
            $x=0;
            $y=0;
            @endphp
            @if(count($pengajuan)>0)
              @php
              $x=count($pengajuan);
              // dd($pengajuan);
              @endphp
            @endif
            @if(count($jadwal)>0)
              @php
              $y=count($jadwal);
              @endphp
            @endif
            @php
            $z=$y+$x;
            @endphp
            @if($z!=0)
            <span class="badge badge-warning navbar-badge">{{$z}}</span>
            @endif
            <span class="badge badge-warning navbar-badge"></span>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">{{$z}} Notifications</span>
            @if(count($pengajuan)>0)
              <div class="dropdown-divider"></div>
              <a href="/user/dosen/newnotification" class="dropdown-item">
                <i class="fa fa-envelope mr-2"></i>{{$x}} Pesan Bimbingan
                {{-- rencana looping notifikasi  --}}
                @foreach ($pengajuan as $item)  
                @endforeach
                <span class="float-right text-muted text-sm">&nbsp;{{Carbon\Carbon::parse($item->created_at ?? '')->diffForHumans()}}</span>
              </a>
            @endif
              <div class="dropdown-divider"></div>
            @if(count($jadwal)>0)
              <div class="dropdown-divider jadwal"></div>
                <a href="/user/dosen/jadwal" class="dropdown-item">
                  {{-- rencana looping notifikasi  --}}
                  <i class="fa fa-calendar mr-2"></i>{{$y}} Jadwal Baru
                  @foreach ($jadwal as $jadwal)
                  @endforeach
                  <span class="float-right text-muted text-sm">&nbsp;{{Carbon\Carbon::parse($jadwal->created_at ?? '')->diffForHumans()}}</span>
                </a>
                <div class="dropdown-divider"></div>
              </div>
            @endif
          @elseif (Auth::user()->role === 1)
            @if(count($adminnotif)>0)
              <span class="badge badge-warning navbar-badge">{{count($adminnotif)}}</span>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">{{count($adminnotif)}} Notifications</span>
              <div class="dropdown-divider"></div>
              @foreach ($adminnotif as $notif)
              @endforeach
              {{-- <a href="/user/mahasiswa/historyPengajuan/{{$notif->id}}" class="dropdown-item">
                <i class="fa fa-envelope mr-2"></i>{{count($notifmhs)}} Pesan Pengajuan
                <span class="float-right text-muted text-sm">&nbsp;{{Carbon\Carbon::parse($notif->created_at ?? '')->diffForHumans()}}</span>
              </a> --}}
              {{-- <form id="suform" action="/user/mahasiswa/historyPengajuan" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $notif->id ?? '' }}">
              </form> --}}
              {{-- <a href="/user/admin/pengajuan"></a> --}}
              {{-- <a href="/user/mahasiswa/historyPengajuan" onclick="event.preventDefault(); document.getElementById('suform').submit();"> --}}
              
              {{-- <div onclick="submitform()"> --}}
                <a href="/user/admin/pengajuan" class="dropdown-item"><i class="fa fa-envelope mr-2"></i>{{count($adminnotif)}} Pesan Pengajuan
                  <span class="float-right text-muted text-sm">&nbsp;{{Carbon\Carbon::parse($notif->created_at ?? '')->diffForHumans()}}</span>
                </a>
              {{-- </div> --}}
            @endif
          @elseif(Auth::user()->role === 3)
            @if(count($notifmhs)>0)
              {{--  --}}
              <span class="badge badge-warning navbar-badge">{{count($notifmhs)}}</span>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">{{count($notifmhs)}} Notifications</span>
              <div class="dropdown-divider"></div>
              @foreach ($notifmhs as $notif)
              @endforeach
              {{-- <a href="/user/mahasiswa/historyPengajuan/{{$notif->id}}" class="dropdown-item">
                <i class="fa fa-envelope mr-2"></i>{{count($notifmhs)}} Pesan Pengajuan
                <span class="float-right text-muted text-sm">&nbsp;{{Carbon\Carbon::parse($notif->created_at ?? '')->diffForHumans()}}</span>
              </a> --}}
              <form id="suform" action="/user/mahasiswa/historyPengajuan" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $notif->id ?? '' }}">
              </form>
              {{-- <a href="/user/mahasiswa/historyPengajuan" onclick="event.preventDefault(); document.getElementById('suform').submit();"> --}}
              
              <div onclick="submitform()">
                <i class="fa fa-envelope mr-2"></i>{{count($notifmhs)}} Pesan Pengajuan
                <span class="float-right text-muted text-sm">&nbsp;{{Carbon\Carbon::parse($notif->created_at ?? '')->diffForHumans()}}</span>
              </div>
            @endif
            {{-- <span class="badge badge-warning navbar-badge">0</span> --}}
          @endif
        </a>
        <script>
          function submitform()
          {
            document.getElementById("suform").submit();
          }
        </script>
            
        {{-- BACK UP NOTIFIKASI --}}
        {{-- <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fa fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div> --}}
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-user-circle-o"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        @if ( Auth::user()->role === 3 )
          <a href="/user/mahasiswa/profile/{{Auth::user()->id}}" class="dropdown-item" type="submit">
        @elseif( Auth::user()->role === 2 )
          <a href="/user/dosen/profile/{{Auth::user()->id}}" class="dropdown-item" type="submit">
        @elseif( Auth::user()->role === 1 )
          <a href="/user/admin/profile/{{Auth::user()->id}}" class="dropdown-item" type="submit">
        @endif
            <!-- Message Start -->
            <div class="media">
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRkdYrHRn224Kyw57NTR8dKliuT6PD8-QDdaTmgwTLw4vKsBAa5" alt="User Avatar" class="img-size-50 mr-3 mt-1 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title mt-1">
                  {{-- <b>{{ Auth::user()->role ?? '' }}</b> --}}
                  @if ( Auth::user()->role === 3 )
                    <b>Mahasiswa</b>
                  @elseif( Auth::user()->role === 2 )
                    <b>Dosen</b>
                  @elseif( Auth::user()->role === 1 )
                    <b>Admin</b>
                  @endif
                  <span class="float-right text-sm text-primary"><i class="fa fa-pencil"> Edit</i></span> <!--wrench-->
                </h3>
                
                {{-- Test --}}
                  <p class="text-sm">{{ Auth::user()->nama ?? '' }}</p>
                  {{-- <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p> --}}
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="/logout" class="dropdown-item dropdown-footer"><b>Logout</b></a>
        </div>
      </li>

      {{-- <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li> --}}
    </ul>
  </nav>

  {{-- <script>
    $(function(){
       $('#jadwal').click(function() {
            $.ajax({
                url: 'dosen/jadwal/{status}',
                type: 'GET',
                status: { status: 1 },
                success: function(response)
                {
                  console.log(parsed_data.success);
                }
            });
       });
    });    
</script> --}}