<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{asset('assets/dist/img/Logo_ITATS.png')}}" alt="ITATS Logo" height="60px" class="ml-5">
      {{-- <span class="brand-text font-weight-light">.</span> --}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRkdYrHRn224Kyw57NTR8dKliuT6PD8-QDdaTmgwTLw4vKsBAa5" class="img-circle elevation-2 mr-2" alt="User Image">
        </div>
        <div class="info">
          {{-- @if ( Auth::user()->role === 3 )
            <a href="#" class="d-block">{{Auth::user()->nama}}</a>
          @elseif( Auth::user()->role === 2 )
            <a href="#" class="d-block">{{Auth::user()->nama}}</a>
          @elseif( Auth::user()->role === 1 )
            <a href="#" class="d-block">{{Auth::user()->nama}}</a>
          @endif --}}
          <a href="#" class="d-block">{{Auth::user()->nama}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

            <!-- DEFAULT -->

          <li class="nav-item">
          @if ( Auth::user()->role === 3 )
            <a href="/user/mahasiswa" class="nav-link {{ (request()->is('user/mahasiswa')) ? 'active' : '' }}">
          @elseif( Auth::user()->role === 2 )
            <a href="/user/dosen" class="nav-link {{ (request()->is('user/dosen')) ? 'active' : '' }} ">
          @elseif( Auth::user()->role === 1 )
            <a href="/user/admin" class="nav-link {{ (request()->is('user/admin')) ? 'active' : '' }}">
          @endif
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          
          @if(Auth::user()->role === 1)
            {{-- ADMIN --}}
            <li class="nav-item has-treeview {{ (request()->is('user/admin/manage*')) ? 'menu-open' : '' }}">
              <a href="#" class="nav-link {{ (request()->is('user/admin/manage*')) ? 'active' : '' }} ">
                <i class="nav-icon fa fa-table"></i>
                <p>
                  Manage
                  <i class="fa fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/user/admin/manage/dosen" class="nav-link {{ (request()->is('user/admin/manage/dosen')) ? 'active' : '' }}">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Dosen</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="/user/admin/pengajuan" class="nav-link {{ (request()->is('user/admin/pengajuan*')) ? 'active' : '' }}">
                <i class="nav-icon fa fa-file-text"></i>
                <p>
                  Pengajuan
                  {{-- <i class="right fa fa-angle-left"></i> --}}
                </p>
              </a>
            </li>
            <li class="nav-item has-treeview">
              <a href="/user/admin/pendaftaran" class="nav-link {{ (request()->is('user/admin/pendaftaran*')) ? 'active' : '' }}">
                <i class="nav-icon fa fa-file-zip-o"></i>
                <p>
                  Pendaftaran
                  {{-- <i class="right fa fa-angle-left"></i> --}}
                </p>
              </a>
            </li>
            <li class="nav-item has-treeview">
              <a href="/user/admin/penjadwalan" class="nav-link {{ (request()->is('user/admin/penjadwalan*')) ? 'active' : '' }}">
                <i class="nav-icon fa fa-list-ol"></i>
                <p>
                  Penjadwalan
                  {{-- <i class="right fa fa-angle-left"></i> --}}
                </p>
              </a>
            </li>
          @endif

          @if(Auth::user()->role === 2)
            {{-- DOSEN --}}
            <li class="nav-item has-treeview">
                <a href="/user/dosen/pengajuan" class="nav-link {{ (request()->is('user/dosen/pengajuan')) ? 'active' : '' }}">
                  <i class="nav-icon fa fa-file-text"></i>
                  <p>
                    Pengajuan
                  </p>
                </a>
            </li>
            <li class="nav-item has-treeview">
              <a href="/user/dosen/jadwal" class="nav-link {{ (request()->is('user/dosen/jadwal*')) ? 'active' : '' }}">
                <i class="nav-icon fa fa-list-ol"></i>
                <p>
                  Jadwal
                </p>
              </a>
            </li>
          @endif

          @if(Auth::user()->role === 3)
            {{-- MAHASISWA --}}
            <li class="nav-item has-treeview">
                <a href="/user/mahasiswa/pengajuan" class="nav-link {{ (request()->is('user/mahasiswa/pengajuan')) ? 'active' : '' }}">
                  <i class="nav-icon fa fa-file-text"></i>
                  <p>
                    Pengajuan Judul
                    {{-- <i class="right fa fa-angle-left"></i> --}}
                  </p>
                </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="/user/mahasiswa/pendaftaran" class="nav-link {{ (request()->is('user/mahasiswa/pendaftaran')) ? 'active' : '' }}">
                  <i class="nav-icon fa fa-edit"></i>
                  <p>
                    Pendaftaran
                    {{-- <i class="right fa fa-angle-left"></i> --}}
                  </p>
                </a>
            </li>
            <li class="nav-item has-treeview">
              <a href="/user/mahasiswa/jadwal" class="nav-link {{ (request()->is('user/mahasiswa/jadwal')) ? 'active' : '' }}">
                <i class="nav-icon fa fa-list-ol"></i>
                <p>
                  Jadwal
                </p>
              </a>
          </li>
          @endif

          
          {{-- <li class="nav-item has-treeview"> --}}
            {{-- <a href="/user/dosen/notification" class="nav-link"> --}}
            {{-- @if ( Auth::user()->role === 3 )
              <a href="/user/mahasiswa/notification" class="nav-link {{ (request()->is('user/mahasiswa/notification')) ? 'active' : '' }}">
            @elseif( Auth::user()->role === 2 )
              <a href="/user/dosen/notification" class="nav-link {{ (request()->is('user/dosen/notification')) ? 'active' : '' }}">
            @elseif( Auth::user()->role === 1 )
              <a href="/user/admin/notification" class="nav-link {{ (request()->is('user/admin/notification')) ? 'active' : '' }}">
            @endif
              <i class="nav-icon fa fa-bell"></i>
              <p>
                Notifikasi
              </p>
            </a>
          </li> --}}
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>