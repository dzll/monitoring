@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Profile</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              {{-- <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li> --}}
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-3">
  
              <!-- Profile Image -->
              <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                  <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle"
                         src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRkdYrHRn224Kyw57NTR8dKliuT6PD8-QDdaTmgwTLw4vKsBAa5"
                         alt="User profile picture">
                  </div>
                  <br>
                  <h3 class="profile-username text-center">{{ Auth::user()->nama ?? '' }}</h3>
                  @if ( Auth::user()->role === 3 )
                    <p class="text-center">( {{ Auth::user()->npm ?? '' }} )</p>
                    <p class="text-muted text-center">Mahasiswa</p>
                  @elseif( Auth::user()->role === 2 )
                    <p class="text-muted text-center">Dosen</p>
                  @elseif( Auth::user()->role === 1 )
                    <p class="text-muted text-center">Admin</p>
                  @endif
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>


            <div class="col-md-9">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Edit Profile</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                @if ( Auth::user()->role === 3 )
                  <form role="form" action="/user/mahasiswa/profile/edit/{{ Auth::user()->id }}" method="POST">
                @elseif( Auth::user()->role === 2 )
                  <form role="form" action="/user/dosen/profile/edit/{{ Auth::user()->id }}" method="POST">
                @elseif( Auth::user()->role === 1 )
                  <form role="form" action="/user/admin/profile/edit/{{ Auth::user()->id }}" method="POST">
                @endif
                    @csrf
                    <div class="row">
                      <div class="card-body col-6">
                        <div class="form-group">
                          <label for="username">Username</label>
                          <input type="username" class="form-control" id="username" name="username" placeholder="Masukkan Username" autocomplete="off" value="{{ $user->username }}">
                        </div>
                        <div class="form-group">
                          <label for="email">Email</label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan email" autocomplete="off" value="{{ $user->email }}">
                        </div>
                      </div>
  
                      <div class="card-body col-6">
                        <div class="form-group">
                          <label for="PasswordLama">Password Lama</label>
                          <input type="password" class="form-control" id="PasswordLama" name="password_lama" placeholder="Input Password">
                        </div>
                        <div class="form-group">
                          <label for="password">Password Baru</label>
                          <input type="password" class="form-control" id="password_baru" name="password_baru" placeholder="Masukkan Password Baru">
                        </div>
                        <div class="form-group">
                          <label for="password">Konfirmasi Password</label>
                          <input type="password" class="form-control" id="ulangi_password" name="ulangi_password" placeholder="Masukkan ulang password">
                        </div>
                      </div>
                    </div>
                    <!-- /.card-body -->
    
                    <div class="card-footer">
                    @if ( Auth::user()->role === 3 )
                      <a href="/user/mahasiswa" class="btn btn-default">
                    @elseif( Auth::user()->role === 2 )
                      <a href="/user/dosen" class="btn btn-default">
                    @elseif( Auth::user()->role === 1 )
                      <a href="/user/admin" class="btn btn-default">
                    @endif
                        Cancel
                      </a>
                      <button type="submit" class="btn btn-primary float-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> &nbsp;Save</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->

              </div>
            
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>


  </div>
  <!-- /.content-wrapper -->


@endsection