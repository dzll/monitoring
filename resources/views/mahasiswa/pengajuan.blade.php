@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pengajuan Judul</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Pengajuan</h3>

                <div class="card-tools">
                  @if ($cek_status == null)
                    <a href="" class="btn btn-sm btn-info float-right mt-1" type="button" data-toggle="modal" data-target="#modal-tambah-pengajuan"><i class="fa fa-plus"></i> &nbsp;Tambah</a>
                  @endif
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th style="width:5%">No</th>
                      <th style="width:35%">Judul</th>
                      <th style="width:15%">Dosen 1</th>
                      <th style="width:15%">Dosen 2</th>
                      <th style="width:10%">Status</th>
                      <th style="width:20%">Tanggal</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php
                        $x = 0;
                        $dosenarr = 0;
                        $arr = 0;
                        
                      @endphp
                      @foreach ($pengajuan as $data)
                        <tr>
                          
                          <td>{{ ++$x }}</td>
                          <td>{{ $data->judul_proposal ?? ''}}</td>
                          
                          {{-- <h1>{{ count($cek_dosen[1]) }}</h1> --}}
                          {{-- <h1>{{ $temp[0] }}</h1>
                          <br>   <br> --}}

                          @if (count($cek_dosen[$dosenarr])==2)
                            @foreach ($cek_dosen[$arr] as $item)
                              {{-- @if($data->id == $item[]->id_pengajuan) --}}
                                <td>{{$item->dosen->nama ?? ''}}</td>
                              {{-- @endif --}}
                                {{-- @php $arr++; @endphp --}}
                            @endforeach
                          @else
                            @foreach ($cek_dosen[$arr] as $item)
                              {{-- @if($data->id == $item[]->id_pengajuan) --}}
                                <td>{{$item->dosen->nama ?? ''}}</td>
                              {{-- @endif --}}
                            @endforeach
                            <td> - </td>
                          @endif
                          @php $dosenarr++; $arr++; @endphp
                          {{-- < $x = 0 ?>
                          < while($x < count($cek_dosen)) { ?>
                              @if($cek_dosen[$x]->count() == 2)
                                <td>uluk</td>
                              @endif
                              < $x++; ?>
                          < } ?> --}}
                          @if($data->status == 0)
                            <td><span class="badge badge-warning"><i class="fa fa-clock-o"></i> Menunggu</span></td>
                          @elseif($data->status == 1)
                            <td><span class="badge badge-success"><i class="fa fa-check"></i> Approve</span></td>
                          @elseif($data->status == 2)
                            <td><span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span></td>
                          @endif
                          <td>
                            <div class="sparkbar" data-color="#00a65a" data-height="20">{{ $data->created_at }}</div>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                {{-- <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a> --}}
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

          
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->

      <div class="modal fade" id="modal-tambah-pengajuan">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <form role="form" action="/user/mahasiswa/pengajuan" method="POST">
              @csrf
              <div class="modal-header">
                <h4 class="modal-title">Tambah Pengajuan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body"> 
                <div class="form-group">
                  <label for="exampleInputEmail1">Judul Proposal <span style="color:red;font-weight:bold;">*</span></label>
                  <textarea class="form-control" rows="3" name="judul_proposal" id="judul_proposal" placeholder="Masukkan judul" autocomplete="off" required ></textarea>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Dosen Pembimbing 1 <span style="color:red;font-weight:bold;">*</span></label>
                      <select class="form-control" name="id_dosen" required>
                        <option disabled selected>- Pilih dosen  -</option>
                        @foreach (App\Models\Dosen::orderBy('id', 'ASC')->get() as $data)
                          <option value="{{ $data->id }}">{{ $data->nama }} ( {{ $data->spesialis }} )</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Dosen Pembimbing 2 <span style="color:gray;font-style: italic;">(opsional)</span></label>
                      <select class="form-control" name="id_dosen_2">
                        <option disabled selected>- Pilih dosen  -</option>
                        @foreach (App\Models\Dosen::orderBy('id', 'ASC')->get() as $data)
                          <option value="{{ $data->id }}">{{ $data->nama }} ( {{ $data->spesialis }} )</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <input class="form-control" name="id_mahasiswa" id="id_mahasiswa" type="hidden" value="{{Auth::user()->id}}">
                </div>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-file"></i>&nbsp; Submit</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection