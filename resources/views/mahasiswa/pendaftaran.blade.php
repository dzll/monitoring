@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pendaftaran Proposal</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    @if ( $cek_status == null)


      <div class="lockscreen-wrapper">
        <div class="help-block text-center">
          <h5>Pengajuan judul belum di setujui dosen</h5>
        </div>
      </div>

    @else

    <section class="content">
        <div class="container-fluid">
          @php
              $ss = App\Models\PendaftaranProposal::where('id_pengajuan', $pengajuan->id)->first()
          @endphp
          {{-- <p>{{ $ss }}</p> --}}
          @if ($ss == false)
              {{-- <p>gak ono</p> --}}
              <div class="card col-md-12">
                <div class="card-header">
                    <h3 class="card-title">Form Pendaftaran Proposal</h3>
                </div>
              <!-- form start -->
                <form action="/user/mahasiswa/pendaftaran" role="form" method="POST" enctype="multipart/form-data">
                  @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="judul">Judul Proposal</label>
                            <h2 class="form-control">{{ $pengajuan->judul_proposal ?? '' }}</h2>
                            <input type="hidden" name="id_pengajuan" value="{{ $pengajuan->id ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="file_dhs">File DHS</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="file_dhs" name="file_dhs">
                                    <label class="custom-file-label" for="file_dhs">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_krs">File KRS</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="file_krs" name="file_krs">
                                    <label class="custom-file-label" for="file_krs">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file_laporan">File Laporan</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="file_laporan" name="file_laporan">
                                    <label class="custom-file-label" for="file_laporan">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <label for="file_laporan">Bukti Pembayaran</label>
                          <div class="input-group">
                              <div class="custom-file">
                                  <input type="file" class="custom-file-input" id="file_bukti_pembayaran" name="file_bukti_pembayaran">
                                  <label class="custom-file-label" for="file_bukti_pembayaran">Choose file</label>
                              </div>
                          </div>
                      </div>
                    </div>
                    <!-- /.card-body -->
  
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary float-right"><i class="fa fa-file"></i> &nbsp; Submit</button>
                    </div>
                </form>
              </div>
          @else
              {{-- <p>Ono</p> --}}
              <div class="lockscreen-wrapper">
                <div class="help-block text-center">
                  <h5>Anda sudah terdaftar</h5>
                </div>
              </div>
          @endif
          {{-- @php
            $pengajuan = App\Models\Pengajuan::where('id_mahasiswa', Auth::user()->id)->first();
            $i = 0;
          @endphp
            <div class="card col-md-8">
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th style="width:5%">No</th>
                      <th style="width:20%">Nama</th>
                      <th style="width:25%">Judul</th>
                      <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach (App\Models\PendaftaranProposal::where('id_pengajuan', $pengajuan->id)->get() as $item)
                          <tr>
                              <td>{{ ++$i }}</td>
                              <td>{{ $item->pengajuan->mahasiswa->nama ?? '' }}</td>
                              <td>{{ $item->pengajuan->judul_proposal ?? '' }}</td>
                              <td>
                                <div class="row">
                                    <button class="btn btn-info btn-sm">
                                        <i class="fa fa-eye" aria-hidden="true"> &nbsp;View</i>
                                    </button>
                                </div>
                            </td>
                          </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div> --}}
            <!-- general form elements -->
            
              <!-- /.card -->
        </div><!-- /.container-fluid -->
      </section>
      
          
    @endif

  </div>
  <!-- /.content-wrapper -->


@endsection

@push('js')
  <script src="{{asset('assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      bsCustomFileInput.init();
    });
    </script>    
@endpush