@extends('layout.app')

@push('css')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Jadwal</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
      <div class="container-fluid">
        @php   
          $jadwal_mahasiswa = DB::select("
            SELECT 
              mahasiswa.npm as npm, 
              mahasiswa.nama as nama, 
              notifikasi.id_dosen as id_dosen_pembimbing, 
              notifikasi.id as id_notifikasi,
              dosen.nama as nama_dosen, 
              pengajuan.judul_proposal,
              pendaftaran_proposal.id as id_pendaftaran,
              pendaftaran_proposal.tanggal as tanggal, 
              pendaftaran_proposal.waktu as waktu, 
              pendaftaran_proposal.tempat as tempat
              FROM pendaftaran_proposal, mahasiswa, notifikasi, pengajuan, penguji, dosen
              WHERE (penguji.id_pendaftaran = pendaftaran_proposal.id AND pendaftaran_proposal.id_pengajuan = pengajuan.id 
                  AND pengajuan.id = notifikasi.id_pengajuan AND notifikasi.id_dosen = dosen.id
                  AND pengajuan.id_mahasiswa = mahasiswa.id AND notifikasi.id_dosen = penguji.id_dosen
                  AND mahasiswa.id =".Auth::user()->id." ) ORDER BY id_notifikasi ASC LIMIT 1;
            ");
        @endphp
        @if ( count($jadwal_mahasiswa) < 1 )
            <div class="lockscreen-wrapper">
                <div class="help-block text-center">
                <h5>Anda belum terjadwal pada sidang proposal</h5>
                </div>
            </div>
        @endif
        @foreach ($jadwal_mahasiswa as $j)
        <div class="invoice p-3 mb-3">
          <h4>
            <i class="fa fa-list-ol"></i> &nbsp; Jadwal Sidang Proposal.
          </h4>
          <br>
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Nama
              <address>
                <strong>{{ $j->nama}}</strong>
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              NPM
              <address>
                <strong>{{ $j->npm}}</strong>
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              <b>Tanggal Sidang</b>
              <br>
              {{ date('d-M-Y', strtotime($j->tanggal ??'')) }}<br>
              {{ $j->waktu }}<br>
              {{ $j->tempat }}<br>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table table-striped">
                <thead>
                <tr>
                  <th width="67%">Judul Proposal</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>{{ $j->judul_proposal}}</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div><br>
          <div class="row">
            <div class="col-6">

              <div class="table-responsive">
                <table class="table">
                  <tr style="background-color:#d9ffdd;">
                    <th style="width:30%">Dosen Pembimbing</th>
                    <form role="form" action="/user/mahasiswa/revisi/print" method="POST" target="_BLANK">
                    @csrf
                      <td style="width:40%">
                          @php
                            $jadwalMhs = App\Models\PendaftaranProposal::find($j->id_pendaftaran);
                            $pembimbing_uji = App\Models\Penguji::where('id_pendaftaran', $j->id_pendaftaran)->orderBy('id', 'DESC')->first();
                          @endphp
                          @foreach ($jadwalMhs->pengajuan->notifikasi->sortBy('id') as $pembimbing)
                            {{ $pembimbing->dosen->nama ?? '' }} <br>
                          @endforeach
                          <input type="hidden" name="nama" value="{{$j->nama ??''}}">
                      </td>
                      <td style="width:30%">
                        <input type="hidden" name="npm" value="{{$j->npm ??''}}">
                        <input type="hidden" name="tanggal" value="{{$j->tanggal ??''}}">
                        <input type="hidden" name="penguji" value="{{$pembimbing_uji->dosen->nama ??''}}">
                        <input type="hidden" name="id_penguji" value="{{$pembimbing_uji->id ??''}}">
                        <button class="btn btn-sm btn-success float-right" type="submit"><i class="fa fa-download"></i> &nbsp;Download Revisi</button>
                        <button class="btn btn-sm btn-info float-right mr-2" type="button" onclick="detailRevisi('{{$pembimbing_uji->id}}','{{$pembimbing_uji->dosen->nama}}','{{ date('d-M-Y', strtotime($pembimbing_uji->tanggal_revisi ??'')) }}','{{ date('d-M-Y', strtotime($pembimbing_uji->deadline_revisi ??'')) }}')"><i class="fa fa-eye"></i></button> 
                      </td>
                    </form>
                  </tr>
                  @foreach (App\Models\Penguji::where('id_pendaftaran', $j->id_pendaftaran)->where('id_dosen','<>', $jadwalMhs->pengajuan->notifikasi->first()->dosen->id)->where('id_dosen','<>', $jadwalMhs->pengajuan->notifikasi->last()->dosen->id)->orderBy('id')->get() as $p)
                    @php
                        $penguji = App\Models\Penguji::where('id_pendaftaran', $j->id_pendaftaran)->where('id_dosen', $p->dosen->id)->first();
                    @endphp
                    <tr>
                      <th style="width:30%">Dosen Penguji</th>
                      <form role="form" action="/user/mahasiswa/revisi/print" method="POST" target="_BLANK">
                      @csrf
                        <td style="width:40%">
                          {{$p->dosen->nama ??''}} 
                        </td>
                        <td style="width:30%">
                          <input type="hidden" name="nama" value="{{$j->nama ??''}}">
                            <input type="hidden" name="npm" value="{{$j->npm ??''}}">
                            <input type="hidden" name="tanggal" value="{{$j->tanggal ??''}}">
                            <input type="hidden" name="penguji" value="{{ $p->dosen->nama ??''}}">
                            <input type="hidden" name="id_penguji" value="{{ $penguji->id ??''}}">
                            <button class="btn btn-sm btn-success float-right" type="submit"><i class="fa fa-download"></i> &nbsp;Download Revisi</button>
                            <button class="btn btn-sm btn-info float-right mr-2" type="button" onclick="detailRevisi('{{$penguji->id}}','{{$p->dosen->nama}}','{{ date('d-M-Y', strtotime($penguji->tanggal_revisi ??'')) }}','{{ date('d-M-Y', strtotime($penguji->deadline_revisi ??'')) }}')"><i class="fa fa-eye"></i></button>
                        </td>
                      </form>
                    </tr>
                  @endforeach
                  

                  
                  {{-- @php
                      $uji = App\Models\Penguji::where('id_pendaftaran', $j->id_pendaftaran)->orderBy('id','desc')->get();
                  @endphp
                  @foreach ($uji as $p)
                      <tr>
                        @php
                            $penguji = App\Models\Penguji::where('id_pendaftaran', $j->id_pendaftaran)->where('id_dosen', $p->dosen->id)->first();
                        @endphp
                        @if ( $p->id_dosen == $j->id_dosen_pembimbing )
                          <th style="width:40%">Dosen Pembimbing</th>
                        @else
                            <th>Dosen Penguji</th>
                        @endif
                        <form role="form" action="/user/mahasiswa/revisi/print" method="POST" target="_BLANK">
                        @csrf
                            <td>{{$p->dosen->nama ??''}}
                                <input type="hidden" name="nama" value="{{$j->nama ??''}}">
                                <input type="hidden" name="npm" value="{{$j->npm ??''}}">
                                <input type="hidden" name="tanggal" value="{{$j->tanggal ??''}}">
                                <input type="hidden" name="penguji" value="{{ $p->dosen->nama ??''}}">
                                <input type="hidden" name="id_penguji" value="{{ $penguji->id ??''}}">
                                <button class="btn btn-sm btn-success float-right" type="submit"><i class="fa fa-download"></i> &nbsp;Download Revisi</button>
                                <button class="btn btn-sm btn-info float-right mr-2" type="button" onclick="detailRevisi('{{$penguji->id}}','{{$p->dosen->nama}}','{{ date('d-M-Y', strtotime($penguji->tanggal_revisi ??'')) }}','{{ date('d-M-Y', strtotime($penguji->deadline_revisi ??'')) }}')"><i class="fa fa-eye"></i></button>
                            </td>
                        </form>
                      </tr>
                  @endforeach --}}


                </table>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <br><br>
        </div>
        @endforeach
        
      </div><!--/. container-fluid -->
        <div class="modal fade" id="modal-detail-revisi">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="judulModal">Detail Revisi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"> 
                    <table id="tableDetailRevisi" class="table"></table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@push('js')
<script>
    function detailRevisi (id, nama, tgl_rev, dead_rev) {

        // alert('horeee' + id);
        var SITEURL = "{{url('/')}}";
        $.ajaxSetup({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        
        $.ajax({
            url: SITEURL + "/user/mahasiswa/revisi/get_revisi",
            type: "POST",
            data: 'id=' + id,
            success:function(result){
                var data = '';
                data += '<tr>';
                data += '<th width="20%">Bab Revisi</th>';
                data += '<th width="60%">Isi Revisi</th>';
                data += '<th width="20%">Status</th>';
                data += '</tr>';
                $.each(result, function(i, item) {
                    data += '<tr>';
                    if( item.bab_revisi!= null ){
                        data += '<td>' + item.bab_revisi + '</td>';
                    } else {
                        data += '<td>-</td>';
                    }
                    data += '<td>' + item.isi_revisi + '</td>';
                    if(item.status_revisi == 1){
                        data += '<td><span class="badge badge-success"><i class="fa fa-check"></i>&nbsp; Selesai</span></td>';
                    }else if(item.status_revisi == 0){
                        data += '<td><span class="badge badge-warning"><i class="fa fa-clock-o"></i>&nbsp; Belum Selesai</span></td>';
                    }
                    data += '</tr>';
                });
                if(tgl_rev != null && dead_rev != null && result.length > 0){
                    data += '<tr style="background-color:#fffeb8;">';
                    data += '<th width="20%">Deadline</th>';
                    data += '<td width="80%">'+ tgl_rev + ' - ' + dead_rev +'</td>';
                    data += '<td width="0%"></td>';
                    data += '</tr>';
                }
                $('#judulModal').text('Revisi ' + nama);
                $('#tableDetailRevisi').empty().append(data);
                $('#modal-detail-revisi').modal('show');
            }
        });


    }
  </script>
@endpush