@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
          @php
              $pengajuan = App\Models\Notifikasi::where('id_dosen', Auth::id())->orderBy('id', 'desc')->get(); 
              $jumlah_bimbingan = DB::select("
                            SELECT 
                              mahasiswa.npm,
                              mahasiswa.nama,
                              pengajuan.judul_proposal,
                              pengajuan.status,
                              dosen.id as id_dosen
                              FROM mahasiswa, pengajuan, dosen, notifikasi
                              WHERE pengajuan.id_mahasiswa = mahasiswa.id AND notifikasi.id_pengajuan = pengajuan.id AND pengajuan.status = '1'
                                AND notifikasi.id_dosen = dosen.id AND dosen.id = ".Auth::user()->id."
                                ;
                            ");
              $jadwal = App\Models\Penguji::where('id_dosen', Auth::user()->id)->get();
              $pendaftaran = DB::select("
                            SELECT 
                              mahasiswa.npm,
                              mahasiswa.nama,
                              pengajuan.judul_proposal,
                              dosen.id as id_dosen,
                              pendaftaran_proposal.id as id_pendaftaran
                              FROM mahasiswa, pendaftaran_proposal, pengajuan, dosen, notifikasi
                              WHERE pendaftaran_proposal.id_pengajuan = pengajuan.id
                                AND pengajuan.id_mahasiswa = mahasiswa.id AND notifikasi.id_pengajuan = pengajuan.id
                                AND notifikasi.id_dosen = dosen.id AND dosen.id = ". Auth::user()->id ." ORDER BY id_pendaftaran DESC
                            ");
          @endphp
          
          <div class="row">
            <div class="col-md-4 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-file-text"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Bimbingan</span>
                  <span class="info-box-number">{{ count($jumlah_bimbingan) }}</span>
  
                  <div class="progress">
                    <div class="progress-bar" style="width: 0%"></div>
                  </div>
                  <span class="progress-description">
                    Jumlah Mahasiswa yang dibimbing 
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-edit"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Pendaftaran</span>
                  <span class="info-box-number">{{ count($pendaftaran) }}</span>
  
                  <div class="progress">
                    <div class="progress-bar" style="width: 0%"></div>
                  </div>
                  <span class="progress-description">
                    Jumlah mahasiswa bimbingan yang telah mendaftar
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4 col-sm-6 col-12">
              <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-list-ol"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Jadwal</span>
                  <span class="info-box-number">{{ count($jadwal) }}</span>
  
                  <div class="progress">
                    <div class="progress-bar" style="width: 0%"></div>
                  </div>
                  <span class="progress-description">
                    Jumlah Jadwal Sidang Proposal
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </div>
        
        <div class="row">
            <!-- Left col -->
            <div class="col-md-6">
              <!-- TABLE: LATEST ORDERS -->

              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Pengajuan</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="tablePengajuan" class="table table-striped">
                    <thead>
                    <tr>
                      <th style="width:5%">No</th>
                      <th style="width:10%">NPM</th>
                      <th style="width:20%">Nama</th>
                      <th style="width:25%">Judul</th>
                      <th style="width:10%">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $i=0
                    @endphp
                    @foreach ($pengajuan as $item) 
                      <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{$item->pengajuan->mahasiswa->npm ?? ''}}</td>
                          <td>{{$item->pengajuan->mahasiswa->nama ?? ''}}</td>
                          <td>{{$item->pengajuan->judul_proposal ?? ''}}</td>
                          @if ($item->judul_status == 0)
                              <td><span class="badge badge-warning"><i class="fa fa-clock-o"></i> Menunggu</span></td>
                          @elseif ($item->judul_status == 1)
                              <td><span class="badge badge-success"><i class="fa fa-check"></i> Approve</span></td>
                          @elseif ($item->judul_status == 2)
                              <td><span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span></td>
                          @endif
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Pendaftaran Proposal</h3>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped" id="tablePendaftaran">
                      <thead>
                      <tr>
                        <th style="width:5%">No</th>
                        <th style="width:10%">NPM</th>
                        <th style="width:20%">Nama</th>
                        <th style="width:35%">Judul</th>
                      </tr>
                      </thead>
                      @php
                        $i = 0;
                      @endphp
                      <tbody>
                        @foreach ($pendaftaran as $item)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $item->npm ?? '' }}</td>
                                <td>{{ $item->nama ?? '' }}</td>
                                <td>{{ $item->judul_proposal ?? '' }}</td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div> 
            </div>
          </div>
        </div>
      </section>

  </div>

@endsection

@push('js')

    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script>
        $(function () {
          $("#tablePengajuan").DataTable({
            "responsive": true,
            "autoWidth": false,
          });
          $("#tablePendaftaran").DataTable({
            "responsive": true,
            "autoWidth": false,
          });
        });
    </script>

@endpush