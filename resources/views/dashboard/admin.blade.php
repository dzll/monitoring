@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
          <!-- Info boxes -->
          <div class="row">
            <div class="col-12 col-sm-6 col-md-4">
              <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-user"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Dosen</span>
                  <span class="info-box-number">
                    {{App\Models\Dosen::all()->count()}}
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-4">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-users"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Mahasiswa</span>
                  <span class="info-box-number">{{App\Models\Mahasiswa::all()->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
  
            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>
  
            <div class="col-12 col-sm-6 col-md-4">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fa fa-file-zip-o"></i></span>
  
                <div class="info-box-content">
                  <span class="info-box-text">Pendaftaran Proposal</span>
                  <span class="info-box-number">{{App\Models\PendaftaranProposal::all()->count()}}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

          <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
              <!-- TABLE: LATEST ORDERS -->
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Pendaftaran Proposal</h3>
                  <div class="card-tools">
                      <a href="/user/admin/pendaftaran" class="btn btn-sm btn-info float-right mt-1 mb-1"><i class="fa fa-eye"></i> &nbsp; Detail</a>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table" id="tablePendaftaran">
                      <thead>
                      <tr>
                        <th style="width:5%">No</th>
                        <th style="width:10%">NPM</th>
                        <th style="width:20%">Nama</th>
                        <th style="width:35%">Judul</th>
                        <th style="width:15%">Tanggal</th>
                      </tr>
                      </thead>
                      @php
                        $i = 0;
                      @endphp
                      <tbody>
                        @foreach (App\Models\PendaftaranProposal::all() as $item)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $item->pengajuan->mahasiswa->npm ?? '' }}</td>
                                <td>{{ $item->pengajuan->mahasiswa->nama ?? '' }}</td>
                                <td>{{ $item->pengajuan->judul_proposal ?? '' }}</td>
                                <td>{{ date("d-M-Y H:i:s",strtotime($item->created_at ?? '')) }}</td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card-body -->
                <!-- /.card-footer -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Dosen</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table id="tableDosen" class="table">
                        <thead>
                        <tr>
                          <th width="20%">No</th>
                          <th>Nama</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i=0
                        @endphp
                        @foreach(App\Models\Dosen::all() as $data)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $data->nama }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  
            </div>
  
            
            <!-- /.col -->
          </div>
          
  


        </div><!--/. container-fluid -->
      </section>
      


    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@push('js')

    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script>
        $(function () {
          $("#tableDosen").DataTable({
            "responsive": true,
            "autoWidth": false,
          });
          $("#tablePendaftaran").DataTable({
            "responsive": true,
            "autoWidth": false,
          });
          // $('#example2').DataTable({
          //   "paging": true,
          //   "lengthChange": false,
          //   "searching": false,
          //   "ordering": true,
          //   "info": true,
          //   "autoWidth": false,
          //   "responsive": true,
          // });
        });
    </script>

@endpush