@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
      <div class="container-fluid">
        @php   
          $jadwal_mahasiswa = DB::select("
            SELECT 
              mahasiswa.npm as npm, 
              mahasiswa.nama as nama, 
              notifikasi.id_dosen as id_dosen_pembimbing, 
              notifikasi.id as id_notifikasi,
              dosen.nama as nama_dosen, 
              pengajuan.judul_proposal,
              pendaftaran_proposal.id as id_pendaftaran,
              pendaftaran_proposal.tanggal as tanggal, 
              pendaftaran_proposal.waktu as waktu, 
              pendaftaran_proposal.tempat as tempat
              FROM pendaftaran_proposal, mahasiswa, notifikasi, pengajuan, penguji, dosen
              WHERE (penguji.id_pendaftaran = pendaftaran_proposal.id AND pendaftaran_proposal.id_pengajuan = pengajuan.id 
                  AND pengajuan.id = notifikasi.id_pengajuan AND notifikasi.id_dosen = dosen.id
                  AND pengajuan.id_mahasiswa = mahasiswa.id AND notifikasi.id_dosen = penguji.id_dosen
                  AND mahasiswa.id =".Auth::user()->id." ) ORDER BY id_notifikasi ASC LIMIT 1;
            ");
        @endphp
        @foreach ($jadwal_mahasiswa as $j)
          <div class="invoice p-3 mb-3">
            <h4>
              <i class="fa fa-list-ol"></i> &nbsp; Jadwal Sidang Proposal.
            </h4>
            <br>
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                Nama
                <address>
                  <strong>{{ $j->nama}}</strong>
                </address>
              </div>
              <div class="col-sm-4 invoice-col">
                NPM
                <address>
                  <strong>{{ $j->npm}}</strong>
                </address>
              </div>
              <div class="col-sm-4 invoice-col">
                <b>Tanggal Sidang</b>
                <br>
                {{ date('d-M-Y', strtotime($j->tanggal ??'')) }}<br>
                {{ $j->waktu }}<br>
                {{ $j->tempat }}<br>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th width="67%">Judul Proposal</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{{ $j->judul_proposal}}</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div><br>
            <div class="row">
              <div class="col-6">
                <div class="table-responsive">
                  <table class="table">
                    @php
                      $jadwalMhs = App\Models\PendaftaranProposal::find($j->id_pendaftaran);
                    @endphp
                    <tr style="background-color:#d9ffdd;">
                      <th style="width:50%">Dosen Pembimbing</th>
                      <td> 
                        @foreach ($jadwalMhs->pengajuan->notifikasi->sortBy('id') as $pembimbing)
                          {{ $pembimbing->dosen->nama ?? '' }} <br>
                        @endforeach
                      </td>
                    </tr>
                    @foreach (App\Models\Penguji::where('id_pendaftaran', $j->id_pendaftaran)->where('id_dosen','<>', $jadwalMhs->pengajuan->notifikasi->first()->dosen->id)->where('id_dosen','<>', $jadwalMhs->pengajuan->notifikasi->last()->dosen->id)->orderBy('id')->get() as $p)
                      @php
                          $penguji = App\Models\Penguji::where('id_pendaftaran', $j->id_pendaftaran)->where('id_dosen', $p->dosen->id)->first();
                      @endphp
                      <tr>
                        <th>Dosen Penguji</th>
                        <td>
                          {{$p->dosen->nama ??''}}
                        </td>
                      </tr>
                    @endforeach
                  </table>
                </div>
              </div>
            </div>
            <br>
            <div class="row no-print">
              <div class="col-12">
                <a href="/user/mahasiswa/jadwal" class="btn btn-primary float-right" style="margin-right: 5px;">
                  <i class="fa fa-eye"></i>&nbsp; Detail
                </a>
              </div>
            </div>
          </div>
        @endforeach

        @foreach ((App\Models\Notifikasi::where('status', 2)->get()) as $item)
            @if ($item->pengajuan->mahasiswa->id == Auth::id() AND $item->status == 2)
                <div class="row">
                    <!-- Left col -->
                    <div class="col-md-12">
                      <!-- TABLE: LATEST ORDERS -->
                      <div class="card">
                        <div class="card-header border-transparent">
                          <h3 class="card-title">History Pengajuan</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                          <div class="table-responsive">
                            <table class="table m-0">
                              <thead>
                              <tr>
                                <th style="width:5%">No</th>
                                <th style="width:40%">Judul</th>
                                {{-- <th style="width:25%">Dosen</th> --}}
                                <th style="width:10%">Status</th>
                                <th style="width:20%">Tanggal</th>
                              </tr>
                              </thead>
                              <tbody>
                                @php
                                  $x = 0;
                                @endphp
                                @foreach (App\Models\Pengajuan::where('id_mahasiswa', Auth::user()->id)->orderBy('id', 'DESC')->get() as $data)
                                  <tr>
                                    <td>{{ ++$x }}</td>
                                    <td>{{ $data->judul_proposal ?? ''}}</td>
                                    {{-- <td>{{ $data->notifikasi->dosen->nama ?? ''}}</td> --}}
                                    @if($data->status == 0)
                                      <td><span class="badge badge-warning"><i class="fa fa-clock-o"></i> Menunggu</span></td>
                                    @elseif($data->status == 1)
                                      <td><span class="badge badge-success"><i class="fa fa-check"></i> ACC</span></td>
                                    @elseif($data->status == 2)
                                      <td><span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span></td>
                                    @endif
                                    <td>
                                      <div class="sparkbar" data-color="#00a65a" data-height="20">{{ $data->created_at }}</div>
                                    </td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                          <!-- /.table-responsive -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                        </div>
                        <!-- /.card-footer -->
                      </div>
                      <!-- /.card -->
                    </div>
                  </div>
            @endif
        @endforeach

        
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection