@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/bootstrap-clockpicker.min.css')}}">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Jadwal</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Jadwal Sidang Proposal</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  
                    <table class="table" id="dosen_jadwal">
                        <thead>
                        <tr>
                          <th style="width:5%">No</th>
                          <th style="width:10%">NPM</th>
                          <th style="width:25%">Nama</th>
                          <th style="width:15%">Pembimbing</th>
                          <th style="width:20%">Penguji</th>
                          <th style="width:15%">Jadwal</th>
                          <th style="width:10%">Aksi</th>
                        </tr>
                        </thead>
                        @php
                        $i = 0;
                        @endphp
                        <tbody>
                          @foreach (App\Models\PendaftaranProposal::orderBy('id', 'desc')->get() as $item)
                            @php
                                $penguji = App\Models\Penguji::where('id_pendaftaran', $item->id)->where('id_dosen', Auth::id())->get();
                            @endphp
                            @if (count($penguji) > 0)
                              @if ( $item->tanggal >=  date("Y-m-d"))
                                <tr style="background-color:#d9ffdd">
                              @else
                                <tr>
                              @endif
                                <td>{{ ++$i }}</td>
                                <td>{{ $item->pengajuan->mahasiswa->npm }}</td>
                                <td>{{ $item->pengajuan->mahasiswa->nama ?? '' }}</td>
                                <td>
                                  @foreach ($item->pengajuan->notifikasi->sortBy('id') as $item_pembimbing)
                                    {{ $item_pembimbing->dosen->nama ?? '' }} <br>
                                  @endforeach
                                </td>
                                <td>
                                  @foreach (App\Models\Penguji::where('id_pendaftaran', $item->id)->where('id_dosen','<>', $item->pengajuan->notifikasi->first()->dosen->id)->where('id_dosen','<>', $item->pengajuan->notifikasi->last()->dosen->id)->orderBy('id')->get() as $penguji)
                                    {{ $penguji->dosen->nama }} <br>
                                  @endforeach
                                </td>
                                <td>
                                    {{ date('d-M-Y', strtotime($item->tanggal ??'')) }} <br>
                                    {{ $item->waktu ?? '' }} <br> 
                                    {{ $item->tempat ?? '' }}
                                </td>
                                <td>
                                  <form action="/user/dosen/jadwal/detail" method="post">
                                    @csrf
                                    <input type="hidden" name="id_pendaftaran" value="{{$item->id}}">
                                    <button class="btn btn-info" title="Detail">
                                      <i class="fa fa-eye" aria-hidden="true"></i>
                                    </button>
                                  </form>
                                </td>
                              </tr>
                            @endif
                          @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@push('js')
    <!-- DataTables -->
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script>
        $(function () {
          $("#dosen_jadwal").DataTable({
            "responsive": true,
            "autoWidth": false,
          });
        });
    </script>

@endpush