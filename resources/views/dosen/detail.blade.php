@extends('layout.app')

@push('css')

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Jadwal</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        @php
          $penguji = App\Models\Penguji::where('id_pendaftaran', $detail->id)->where('id_dosen', Auth::id())->first();
        @endphp
        <div class="container-fluid">
          <div class="invoice p-3 mb-3">
            <h4>
              <i class="fa fa-list-ol"></i> &nbsp; Jadwal Sidang Proposal.
            </h4>
            <br>
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                Nama
                <address>
                  <strong>{{$detail->pengajuan->mahasiswa->nama ??''}}</strong>
                </address>
              </div>
              <div class="col-sm-4 invoice-col">
                NPM
                <address>
                  <strong>{{$detail->pengajuan->mahasiswa->npm ??''}}</strong>
                </address>
              </div>
              <div class="col-sm-4 invoice-col">
                <b>Tanggal Sidang</b>
                <br>
                {{ date('d-M-Y', strtotime($detail->tanggal ??'')) }}<br>
                {{ $detail->waktu ?? '' }}<br>
                {{ $detail->tempat ?? '' }}<br>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                  <tr>
                    <th width="80%">Judul Proposal</th>
                    <th width="10%">File</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{{$detail->pengajuan->judul_proposal ??''}}</td>
                    <td><a href="/user/dosen/jadwal/file_laporan/{{ $detail->id ?? '' }}/download" class="btn btn-success" title="Download Laporan"><i class="fa fa-download"></i></a></td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div><br>
            <div class="row">
              <div class="col-6">
  
                <div class="table-responsive">
                  <table class="table">
                    <tr style="background-color:#d9ffdd;">
                      <th style="width:50%">Dosen Pembimbing</th>
                      <td>
                        @foreach ($detail->pengajuan->notifikasi->sortBy('id') as $pembimbing)
                          {{ $pembimbing->dosen->nama ?? '' }} <br>
                        @endforeach
                      </td>
                    </tr>
                    @foreach (App\Models\Penguji::where('id_pendaftaran', $detail->id)->where('id_dosen','<>', $detail->pengajuan->notifikasi->first()->dosen->id)->where('id_dosen','<>', $detail->pengajuan->notifikasi->last()->dosen->id)->orderBy('id')->get() as $p)
                      <tr>
                        <th>Dosen Penguji</th>
                        <td>
                          {{$p->dosen->nama ??''}}
                        </td>
                      </tr>
                    @endforeach
                    @php
                      $rev = App\Models\Revisi::where('id_penguji', $penguji->id)->get();
                    @endphp
                    @if ( $penguji->tanggal_revisi != null AND $penguji->deadline_revisi != null AND count($rev) > 0 )
                      <tr style="background-color:#fffeb8;">
                        <th>Deadline</th>
                        <td>{{ date('d-M-Y', strtotime($penguji->tanggal_revisi ??'')) }} - {{ date('d-M-Y', strtotime($penguji->deadline_revisi ??'')) }}</td>
                      </tr>
                    @endif
                  </table>
                </div>
              </div>
            </div>
            <br><br>
              <div class="row no-print">
                <div class="col-12">
                    <form action="/user/dosen/jadwal/detail/revisi" method="post">
                      @csrf
                      <a href="/user/dosen/jadwal" class="btn btn-default"><i class="fa fa-undo"></i> &nbsp;Kembali</a>
                      @if ( $detail->tanggal <= date("Y-m-d") )
                        <input type="hidden" name="id_pendaftaran" value="{{$detail->id}}">
                        <button class="btn btn-info float-right">
                          <i class="fa fa-plus"> &nbsp;Buat Revisi</i>
                        </button>
                      @endif
                    </form>
                </div>
              </div>
          </div>
        </div>
      </section>
      
  </div>


@endsection