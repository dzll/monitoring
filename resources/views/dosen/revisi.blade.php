@extends('layout.app')

@push('css')

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.css')}}">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Revisi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        @php
          $penguji = App\Models\Penguji::where('id_pendaftaran', $detail->id)->where('id_dosen', Auth::id())->first();
        @endphp
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tambah Revisi</h3>
                    </div>
                    <form role="form" id="formRevisi">
                        @csrf
                        <div class="card-body pad">
                            <div class="form-group">
                                <label>Bab Revisi <span style="color:gray;font-weight:bold;">(opsional)</span></label>
                                <input class="form-control" name="bab_revisi" placeholder="Bab Revisi ..." type="text" autocomplete="off"/>
                            </div>
                            <div class="mb-3">
                                <label>Isi Revisi <span style="color:red;font-weight:bold;">*</span></label>
                                <textarea class="textarea" name="isi_revisi" placeholder="Place some text here"
                                        style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>
                            </div>
                            <input type="hidden" name="id_penguji" value="{{ $penguji->id }}">
                        </div>
                        <div class="card-footer">
                            <a href="/user/dosen/jadwal" class="btn btn-default"><i class="fa fa-undo"></i> &nbsp;Kembali</a>
                            <button class="btn btn-primary float-right" id="btn_save">
                                <i class="fa fa-plus"> &nbsp;Tambah</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div> 
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Revisi</h3>
                        <div class="card-tools">
                            <form role="form" action="/user/dosen/revisi/print" method="POST" target="_BLANK">
                                @csrf
                                @if ( $detail->tanggal <= date("Y-m-d") )
                                    <input type="hidden" name="nama" value="{{$detail->pengajuan->mahasiswa->nama ??''}}">
                                    <input type="hidden" name="npm" value="{{$detail->pengajuan->mahasiswa->npm ??''}}">
                                    <input type="hidden" name="tanggal" value="{{$detail->tanggal ??''}}">
                                    <input type="hidden" name="penguji" value="{{ Auth::user()->nama ??''}}">
                                    <input type="hidden" name="id_penguji" value="{{ $penguji->id ??''}}">
                                    <button class="btn btn-sm btn-success float-right" type="submit" data-toggle="modal" data-target="#modal-tambah-revisi"><i class="fa fa-download"></i> &nbsp;Download Revisi</button>
                                @endif
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="tableDosen" class="table">
                            <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="25%">Bab Revisi</th>
                                <th width="50%">Isi Revisi</th>
                                <th width="15%">Status</th>
                                <th width="5%">#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=0;
                            @endphp
                            @foreach( App\Models\Revisi::where('id_penguji', $penguji->id)->orderBy('id', 'asc')->get() as $data)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $data->bab_revisi ?? '-' }}</td>
                                    <td>{!! $data->isi_revisi ?? '' !!}</td>
                                    <td>
                                        @if ($data->status_revisi == 1)
                                        <span class="badge badge-success"><i class="fa fa-check"></i> Selesai</span>
                                        @else
                                        <input type="hidden" name="id_revisi" value="{{ $data->id }}">
                                        <button class="btn btn-sm btn-success" onclick="updateStatusRevisi({{ $data->id }})" type="submit"><i class="fa fa-check" title="Selesai"></i></button>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($data->status_revisi == 0)
                                        <input type="hidden" name="id_revisi" value="{{ $data->id }}">
                                        <button class="btn btn-sm btn-danger" onclick="hapusRevisi({{ $data->id }})" type="submit"><i class="fa fa-trash" title="Delete"></i></button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="callout">
                  <span>
                    {{-- <div class="input-group float-right col-md-5">
                      <input type="text" class="form-control" placeholder="Input Nilai (1 - 100)">
                      <div class="input-group-append">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i></button>
                      </div>
                    </div> --}}
                    @php
                        $nilai = App\Models\NilaiDosen::where('id_penguji', $penguji->id)->get();
                    @endphp
                    @if (count($nilai) > 0)
                      <button class="mt-2 md-2 ml-2 float-right btn btn-primary btn-sm" onclick="editNilai('{{ $penguji->id ??'' }}')" title="Edit"><i class="fa fa-pencil"></i></button>
                      <div class="modal fade" id="modal-edit-nilai-dosen">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <form action="" role="form" id="formSaveEditNilai">
                              @csrf
                              <div class="modal-header">
                                  <h4 class="modal-title">Edit Nilai</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body"> 
                                  <table id="tableEditNilaiDosen" class="table"></table>
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                <button type="button" class="btn btn-primary" id="btn_save_edit_nilai"><i class="fa fa-bookmark"></i>&nbsp; Simpan</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                      <button class="mt-2 md-2 float-right btn btn-info btn-sm" onclick="detailNilai('{{ $penguji->id ??'' }}')" title="Detail"><i class="fa fa-eye"></i></button>
                      <div class="modal fade" id="modal-detail-nilai-dosen">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title">Detail Nilai</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </button>
                              </div>
                              <div class="modal-body"> 
                                  <table id="tableNilaiDosen" class="table"></table>
                              </div>
                              <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                              </div>
                          </div>
                        </div>
                      </div>
                      <h3 class="m-2 mr-3 float-right"><strong>= &nbsp; <u>{{ $nilai_akhir ?? '0'}}</u></strong></h3>  
                    @else
                      <button class="mt-2 md-2 mr-2 float-right btn btn-primary btn-sm" type="submit" data-toggle="modal" data-target="#modal-tambah-nilai"><i class="fa fa-plus"></i>&nbsp; Tambah</button>
                      <div class="modal fade" id="modal-tambah-nilai">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            {{-- <form action="/user/dosen/revisi/nilai/tambah" role="form" method="POST"> --}}
                            <form action="" role="form" id="formTambahNilai">
                            @csrf
                              <div class="modal-header">
                                <h4 class="modal-title">Tambah Nilai</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <table class="table">
                                  <tr>
                                    <th>No</th>
                                    <th style="width:55%">Materi yang diuji</th>
                                    <th style="width:10%">Persentase</th>
                                    <th>Nilai <span style="font-family: consolas; font-size: 18px;color: red"><strong>*</strong></span></th>
                                  </tr>
                                  @php  $no = 0; @endphp
                                  @foreach ($item_nilai as $item)
                                    <tr>
                                      <td>{{ ++$no }}.</td>
                                      <td>{{ $item->nama }}</td>
                                      <td>{{ $item->persentase }}%</td>
                                      <td>
                                        <div class="form-group">
                                          <input type="hidden" name="id_item_nilai[]" value="{{ $item->id ??'' }}">
                                          <input type="hidden" name="id_penguji[]" value="{{ $penguji->id ??'' }}">
                                          <input type="number" class="form-control" placeholder="Masukkan Nilai" name="nilai[]" required autocomplete="off">
                                        </div>
                                      </td>
                                    </tr>
                                  @endforeach
                                </table>
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="btn_save_nilai"><i class="fa fa-bookmark"></i>&nbsp; Simpan</button>
                                {{-- <button type="submit" class="btn btn-primary"><i class="fa fa-bookmark"></i>&nbsp; Simpan</button> --}}
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    @endif
                    <h3 class="m-2"><i class="fa fa-check-square-o"></i><strong>&nbsp; Nilai</strong></h3>
                  </span>
                </div>
            </div> 
        </div>
      </section>
  </div>

@endsection

@push('js')

<!-- Summernote -->
<script src="{{asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote({
      height: 200,
      focus: true
    })
  })
</script>
    
<script>
  var SITEURL = "{{url('/')}}";
  $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $(function () {

    $('#btn_save').click(function(e){
      e.preventDefault();
      $(this).html('<i class="fa fa-spinner fa-spin"></i> &nbsp;Menambahkan...');

      $.ajax({
        data: $('#formRevisi').serialize(),
        url: SITEURL + "/user/dosen/revisi/tambah",
        type: "POST",
        dataType: 'json',
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            console.log('Error:', data);
            $('#btn_save').html('<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Tambah');
        }
      });

    });
  });

  function updateStatusRevisi(id_revisi){
    var updateMsg = confirm("Revisi Selesai?");
    if(updateMsg){
      $.ajax({
        data: 'id_revisi=' + id_revisi,
        url: SITEURL + "/user/dosen/revisi/status/update",
        type: "POST",
        dataType: 'json',
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            console.log('Error:', data);
        }
      });
    } 
  }

  function hapusRevisi(id_revisi){
    var deleteMsg = confirm("Yakin Hapus Revisi?");
    if(deleteMsg){
      $.ajax({
        data: 'id_revisi=' + id_revisi,
        url: SITEURL + "/user/dosen/revisi/hapus",
        type: "POST",
        dataType: 'json',
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            console.log('Error:', data);
        }
      });
    }
  }

  $(function () {

    $('#btn_save_nilai').click(function(e){
      e.preventDefault();
      $(this).html('<i class="fa fa-spinner fa-spin"></i> &nbsp;Menyimpan...');

      $.ajax({
        data: $('#formTambahNilai').serialize(),
        url: SITEURL + "/user/dosen/revisi/nilai/tambah",
        type: "POST",
        dataType: 'json',
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            console.log('Error:', data);
            $('#btn_save_nilai').html('<i class="fa fa-bookmark" aria-hidden="true"></i>&nbsp; Simpan');
        }
      });

    });
  });

  function detailNilai (id) {

    $.ajax({
        url: SITEURL + "/user/dosen/revisi/nilai/get_nilai",
        type: "POST",
        data: 'id=' + id,
        success:function(result){
            var data = '';
            var nilai_akhir = 0;
            data += '<tr>';
            data += '<th width="60%">Materi Yang diuji</th>';
            data += '<th width="20%">Persentase</th>';
            data += '<th width="20%">Nilai</th>';
            data += '</tr>';
            $.each(result, function(i, item) {
                nilai_akhir += parseFloat(item.hasil_nilai_persentase);
                data += '<tr>';
                data += '<td>' + item.nama + '</td>';
                data += '<td>' + item.persentase + '%</td>';
                data += '<td>' + item.nilai + '</td>';
                data += '</tr>';
            });
            if(result.length > 0){
                data += '<tr style="background-color:#d9ffdd; font-size:20px;">';
                data += '<td></td>';
                data += '<th>Nilai Akhir &nbsp; : </th>';
                data += '<td><strong><u>'+ nilai_akhir.toFixed(2) +'</u></strong></td>';
                data += '</tr>';
            }
            $('#tableNilaiDosen').empty().append(data);
            $('#modal-detail-nilai-dosen').modal('show');
        }
    });
  }

  function editNilai (id) {
    $.ajax({
        url: SITEURL + "/user/dosen/revisi/nilai/get_nilai",
        type: "POST",
        data: 'id=' + id,
        success:function(result){
            var data = '';
            var nilai_akhir = 0;
            data += '<tr>';
            data += '<th width="55%">Materi Yang diuji</th>';
            data += '<th width="15%">Persentase</th>';
            data += '<th width="10%">Nilai</th>';
            data += '<th width="20%">Edit Nilai</th>';
            data += '</tr>';
            $.each(result, function(i, item) {
                data += '<tr>';
                data += '<td>' + item.nama + '</td>';
                data += '<td>' + item.persentase + '%</td>';
                data += '<td>' + item.nilai + '</td>';
                data += '<input type="hidden" name="id_item_nilai[]" value="'+item.id_item_nilai+'">';
                data += '<input type="hidden" name="id_nilai_dosen[]" value="'+item.id_nilai_dosen+'">';
                data += '<td style="background-color:#d9ffdd;"><input type="number" class="form-control" placeholder="Input" name="nilai[]" required autocomplete="off"></td>';
                data += '</tr>';
            });
            $('#tableEditNilaiDosen').empty().append(data);
            $('#modal-edit-nilai-dosen').modal('show');
        }
    });
  }

  $(function () {

    $('#btn_save_edit_nilai').click(function(e){
      e.preventDefault();
      $(this).html('<i class="fa fa-spinner fa-spin"></i> &nbsp;Menyimpan...');

      $.ajax({
        data: $('#formSaveEditNilai').serialize(),
        url: SITEURL + "/user/dosen/revisi/nilai/edit",
        type: "POST",
        dataType: 'json',
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            console.log('Error:', data);
            $('#btn_save_nilai').html('<i class="fa fa-bookmark" aria-hidden="true"></i>&nbsp; Simpan');
        }
      });

    });
  });
</script>

@endpush