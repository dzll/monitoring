@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Riwayat Pengajuan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">History Pengajuan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th style="width:5%">No</th>
                      <th style="width:40%">Judul</th>
                      <th style="width:25%">Dosen</th>
                      <th style="width:10%">Status</th>
                      <th style="width:20%">Tanggal</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php
                        $x = 0;
                      @endphp
                      @foreach ($riwayat as $data)
                        <tr>
                          <td>{{ ++$x }}</td>
                          <td>{{ $data->judul_proposal ?? ''}}</td>
                          <td>{{ $data->notifikasi->dosen->nama ?? ''}}</td>
                          @if($data->status == 0)
                            <td><span class="badge badge-warning"><i class="fa fa-clock-o"></i> Menunggu</span></td>
                          @elseif($data->status == 1)
                            <td><span class="badge badge-success"><i class="fa fa-check"></i> ACC</span></td>
                          @elseif($data->status == 2)
                            <td><span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span></td>
                          @endif
                          <td>
                            <div class="sparkbar" data-color="#00a65a" data-height="20">{{ $data->created_at }}</div>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                {{-- <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a> --}}
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

          
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection