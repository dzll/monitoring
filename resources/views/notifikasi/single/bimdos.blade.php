@extends('layout.app')

@push('css')

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endpush

@push('js')

  <!-- PAGE PLUGINS -->
  <!-- SparkLine -->
  <script src="{{asset('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
  <!-- jVectorMap -->
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <!-- SlimScroll 1.3.0 -->
  <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
  <!-- ChartJS 1.0.2 -->
  <script src="{{asset('assets/plugins/chartjs-old/Chart.min.js')}}"></script>

  <!-- PAGE SCRIPTS -->
  <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script>

@endpush

@section('content') 
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Notifikasi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Notifikasi</h3>

                <div class="card-tools">
                  {{-- @if ($cek_status == null)
                    <a href="" class="btn btn-sm btn-info float-right mt-1" type="button" data-toggle="modal" data-target="#modal-tambah-pengajuan"><i class="fa fa-plus"></i> &nbsp;Tambah</a>
                  @endif --}}
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th style="width:5%">No</th>
                      <th style="width:10%">NPM</th>
                      <th style="width:20%">Nama</th>
                      <th style="width:25%">Judul</th>
                      <th style="width:10%">Status</th>
                      <th style="width:10%">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($req as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->pengajuan->mahasiswa->npm ?? ''}}</td>
                                <td>{{$item->pengajuan->mahasiswa->nama ?? ''}}</td>
                                <td>{{$item->pengajuan->judul_proposal ?? ''}}</td>
                                @if ($item->judul_status == 0)
                                    <td><span class="badge badge-warning"><i class="fa fa-clock-o"></i> Menunggu</span></td>
                                    <td>
                                      <div class="row">
                                        <form action="/user/dosen/notification/acc" method="POST">
                                          @csrf
                                          <input type="hidden" name="id_pengajuan" value="{{$item->pengajuan->id}}">
                                          <input type="hidden" name="nama_mahasiswa" value="{{$item->pengajuan->mahasiswa->nama ?? ''}}">
                                          <button class="btn btn-success btn-sm" type="submit">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                          </button>
                                        </form>
                                        &nbsp; &nbsp;
                                        <form action="/user/dosen/notification/tolak" method="POST">
                                          @csrf
                                          <input type="hidden" name="id_pengajuan" value="{{$item->pengajuan->id}}">
                                          <input type="hidden" name="nama_mahasiswa" value="{{$item->pengajuan->mahasiswa->nama ?? ''}}">
                                          <button class="btn btn-danger btn-sm" type="submit">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                          </button>
                                        </form>
                                      </div>


                                        {{-- <a href="/user/dosen/notification/tolak/{{$item->pengajuan->id}}" type="button" class="btn btn-block btn-danger">Tolak</a>
                                        <a href="/user/dosen/notification/acc/{{$item->pengajuan->id}}" type="button" class="btn btn-block btn-success">ACC</a> --}}
                                    </td>
                                @elseif ($item->judul_status == 1)
                                    <td><span class="badge badge-success"><i class="fa fa-check"></i> Acc</span></td>
                                @elseif ($item->judul_status == 2)
                                    <td><span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span></td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
                {{-- <a href="/user/dosen/notification/tolak" type="button" class="btn btn-block btn-danger">Tolak</a> --}}
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                {{-- <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a> --}}
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

          
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->

      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection