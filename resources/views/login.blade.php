<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Akademik | Monitoring Proposal</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link href="{{asset('assets/dist/css/bootstrap.min.css')}}" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/css/main.css')}}">


</head>
<body class="hold-transition">

  <div class="limiter">
    <div class="container-login100"> <!--style="background-image: url('http://smpn50sby.sch.id/wp-content/uploads/2017/07/WhatsApp-Image-2018-12-19-at-14.56.35-1100x400.jpeg'); "> -->
      <div class="wrap-login100 p-l-50 p-r-50 p-t-62 p-b-33">
        <img src="https://itats.ac.id/wp-content/uploads/2018/11/logo-baru-retina.jpg" alt="ITATS Logo" class="mx-auto d-block" style="max-height:75px; width: auto;">
        <div class="card mt-5">
          <div class="card-body ml-5 mr-5 mt-2 mb-2">
            {{-- <h2 class="mt-4 txt3" align="center"><b>LOGIN</b></h2> --}}
            {{-- <hr> --}}
            @if(session('message'))
            <div class="alert alert-danger alert-dismissible show fade">
              <div class="alert-body">
                <button class="close" data-dismiss="alert">
                  <span>x</span>
                </button>
                {{session('message')}}
              </div>
            </div>
            @endif
          
            <form action="{{route('login')}}" method="post" class="login100-form validate-form flex-sb flex-w">
              @csrf

              <div class="p-t-31 p-b-9">
                <span class="txt1">
                  Username
                </span>
              </div>
              <div class="wrap-input100 validate-input" data-validate = "Username is required">
                <input class="input100" type="text" name="username" value="{{ session('username') }}" autocomplete="off">
                <span class="focus-input100"></span>
              </div>
              
              <div class="p-t-13 p-b-9">
                <span class="txt1">
                  Password
                </span>
    
                {{-- <a href="#" class="txt2 bo1 m-l-5">
                  Forgot?
                </a> --}}
              </div>
              <div class="wrap-input100 validate-input" data-validate = "Password is required">
                <input class="input100" type="password" name="password" >
                <span class="focus-input100"></span>
              </div>
    
              <div class="container-login100-form-btn m-t-17 mt-4">
                <button type="submit" class="login100-form-btn">
                  <b>LOGIN</b>
                </button>
              </div>
                <div class="w-full text-center mt-5">
                    <a href="https://itats.ac.id/" target="_blank" class="txt1" style="text-decoration:none">
                        Institut Teknologi Adhi Tama Surabaya <br>
                        Copyright &copy; 2020
                    </a>
                    <div class="row w-full text-center mb-2 mt-2" style="margin-left: 25%; margin-right: 25%">
                        <div class="facebook text-center mr-2">
                            <a href="//facebook.com/kampusitats" target="_blank" class="btn btn-xs btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
                        </div>
                        <div class="twitter text-center mr-2">
                            <a href="//twitter.com/kampusitats" target="_blank" class="btn btn-xs btn-circle btn-twitter"><i class="fa fa-twitter"></i></a> 
                        </div>
                        <div class="youtube text-center mr-2">
                            <a href="//youtube.com/channel/UCcTh5Zf3QsFbpcajPd2Fj-Q/featured" target="_blank" class="btn btn-xs btn-circle btn-youtube"><i class="fa fa-youtube"></i></a> 
                        </div>
                        <div class="instagram text-center mr-2">
                            <a href="//instagram.com/kampusitats" target="_blank" class="btn btn-xs btn-circle btn-instagram"><i class="fa fa-instagram"></i></a> 
                        </div>
                        <div class="linkedin text-center mr-2">
                            <a href="//linkedin.com/school/institut-teknologi-adhi-tama-surabaya" target="_blank" class="btn btn-xs btn-circle btn-linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
