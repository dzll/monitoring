<?php

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });
Route::group(['prefix' => '/user'], function () {

    Route::middleware('auth:admin')->group(function () {

        Route::get('/admin', function () {
            return view('dashboard.admin');
        });

        Route::get('/admin/manage/dosen', 'App\Http\Controllers\AdminController@manageDosen');

        Route::get('/admin/pengajuan', 'App\Http\Controllers\AdminController@pengajuan');
        Route::post('/admin/pengajuan/acc', 'App\Http\Controllers\AdminController@pengajuanAcc');
        Route::post('/admin/pengajuan/tolak', 'App\Http\Controllers\AdminController@pengajuanTolak');

        Route::get('/admin/pendaftaran/{id}/detail', 'App\Http\Controllers\AdminController@detailPendaftaran');
        Route::get('/admin/pendaftaran', 'App\Http\Controllers\AdminController@pendaftaran');

        Route::get('/admin/pendaftaran/{id}/file_dhs/download', 'App\Http\Controllers\AdminController@fileDhsDownload');
        Route::get('/admin/pendaftaran/{id}/file_krs/download', 'App\Http\Controllers\AdminController@fileKrsDownload');
        Route::get('/admin/pendaftaran/{id}/file_laporan/download', 'App\Http\Controllers\AdminController@fileLaporanDownload');
        Route::get('/admin/pendaftaran/{id}/file_bukti_pembayaran/download', 'App\Http\Controllers\AdminController@fileBuktiPembayaranDownload');

        Route::post('/admin/manage/dosen/store', 'App\Http\Controllers\AdminController@manageDosenStore');
        Route::post('/admin/manage/dosen/edit/{id}', 'App\Http\Controllers\AdminController@manageDosenUpdate');

        Route::get('/admin/penjadwalan', 'App\Http\Controllers\PengujiController@penjadwalan');
        Route::post('/admin/penjadwalan/get_dosen', 'App\Http\Controllers\PengujiController@getDosen');
        Route::post('/admin/penjadwalan/tambah', 'App\Http\Controllers\PengujiController@tambahPenjadwalan');
        Route::post('/admin/penjadwalan/store', 'App\Http\Controllers\PengujiController@store');
        Route::post('/admin/penjadwalan/full_print', 'App\Http\Controllers\PengujiController@fullPrint');
        Route::post('/admin/penjadwalan/print', 'App\Http\Controllers\PengujiController@print');
        Route::POST('/admin/file/berita_acara', 'App\Http\Controllers\PengujiController@printBeritaAcara');

        Route::get('/admin/profile/{id}', 'App\Http\Controllers\AdminController@profile');
        Route::post('/admin/profile/edit/{id}', 'App\Http\Controllers\AdminController@profileUpdate');
    });

    Route::middleware('auth:dosen')->group(function () {

        Route::get('/dosen', function () {
            return view('dashboard.dosen');
        });
        Route::get('/dosen/newnotification', 'App\Http\Controllers\DosenController@singlenotif');
        Route::get('/dosen/notification', 'App\Http\Controllers\DosenController@notification');
        Route::post('/dosen/notification/tolak', 'App\Http\Controllers\DosenController@statusNo');
        Route::post('/dosen/notification/acc', 'App\Http\Controllers\DosenController@statusYes');

        Route::get('/dosen/pengajuan', 'App\Http\Controllers\DosenController@pengajuan');

        Route::get('/dosen/jadwal', 'App\Http\Controllers\DosenController@jadwal');
        // Route::get('/dosen/jadwal/{status}', 'TestController@updateNotif');
        Route::post('/dosen/jadwal/detail', 'App\Http\Controllers\DosenController@detailJadwal');

        Route::get('/dosen/jadwal/file_laporan/{id}/download', 'App\Http\Controllers\DosenController@fileLaporanDownload');
        Route::post('/dosen/jadwal/detail/revisi', 'App\Http\Controllers\DosenController@revisi');
        Route::post('/dosen/revisi/tambah', 'App\Http\Controllers\DosenController@tambahRevisi');
        Route::post('/dosen/revisi/status/update', 'App\Http\Controllers\DosenController@statusRevisiUpdate');
        Route::post('/dosen/revisi/hapus', 'App\Http\Controllers\DosenController@hapusRevisi');
        Route::post('/dosen/revisi/print', 'App\Http\Controllers\DosenController@revisiPrint');
        Route::post('/dosen/revisi/nilai/tambah', 'App\Http\Controllers\DosenController@tambahNilai');
        Route::post('/dosen/revisi/nilai/get_nilai', 'App\Http\Controllers\DosenController@getNilai');
        Route::post('/dosen/revisi/nilai/edit', 'App\Http\Controllers\DosenController@editNilai');

        Route::get('/dosen/profile/{id}', 'App\Http\Controllers\DosenController@profile');
        Route::post('/dosen/profile/edit/{id}', 'App\Http\Controllers\DosenController@profileUpdate');
    });

    Route::middleware('auth:mahasiswa')->group(function () {

        Route::get('/mahasiswa', function () {
            return view('dashboard.mahasiswa');
        });
        Route::post('/mahasiswa/historyPengajuan', 'App\Http\Controllers\PengajuanController@history');
        // Route::get('/mahasiswa/historyPengajuan/{id}', function (Request $request)
        // {
        //     if(! $request->has)
        // }
        Route::get('/mahasiswa/pengajuan', 'App\Http\Controllers\PengajuanController@view');
        Route::post('/mahasiswa/pengajuan', 'App\Http\Controllers\PengajuanController@store');

        Route::get('/mahasiswa/pendaftaran', 'App\Http\Controllers\PendaftaranProposalController@view');
        Route::post('/mahasiswa/pendaftaran', 'App\Http\Controllers\PendaftaranProposalController@store');

        Route::get('/mahasiswa/jadwal', 'App\Http\Controllers\MahasiswaController@jadwal');
        Route::post('/mahasiswa/revisi/print', 'App\Http\Controllers\MahasiswaController@revisiPrint');
        Route::post('/mahasiswa/revisi/get_revisi', 'App\Http\Controllers\MahasiswaController@getRevisi');
        Route::get('/mahasiswa/profile/{id}', 'App\Http\Controllers\MahasiswaController@profile');
        Route::post('/mahasiswa/profile/edit/{id}', 'App\Http\Controllers\MahasiswaController@profileUpdate');
    });
});

Route::get('/', function () {
    return view('login');
})->middleware('guest')->name('login');;
Route::get('/login', function () {
    return view('login');
})->middleware('guest')->name('login');
Route::post('/login', 'App\Http\Controllers\AuthController@login');
// Route::post('/login', ['as' => 'login', 'uses' => 'App\Http\Controllers\AuthController@login']);
Route::get('/logout', 'App\Http\Controllers\AuthController@logout');
